import { Creators as actions} from 'Ducks/carteira'
import store from 'Store';

const loggoutSystem = () => {
    const state = store.getState();

    if(state && state.websocket && state.websocket.socket){
        state.websocket.socket.disconnect();
    }   
    store.dispatch(actions.adicionarCorAtivo([]));     
    sessionStorage.removeItem('tokenHash');
    window.location.href = "/";
    
}

export default loggoutSystem;