import React from 'react';
import { Bar } from 'react-chartjs-2';
import {isMobile} from 'react-device-detect';

import StyledChartBar from 'Pages/charts/StyledChartBar';

export default class GraficoBar extends React.Component {

  state={};

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  onOkFiltro (param){ 
    
  }

  componentDidUpdate(nextProps){        
  }

  iniciarVariaveisChart(){
    const { data = [], title = "" } = this.props;
    
    const listAtivos = [];
    const listDividendYield = [];
    const listDividendYieldOnCost = [];

    for (let i = 0; i < data.length; i++ ) {
      const tradeVez = data[i];
      const ativo = tradeVez.ticker.ativo;
      if(ativo && ativo.dividendYield > 0){
        listAtivos.push(tradeVez.ticker.descricao);
        listDividendYield.push(ativo.dividendYield.toFixed(2));
        listDividendYieldOnCost.push(tradeVez.dividendYieldOnCost.toFixed(2));
      }
    }

    if(listAtivos && listAtivos.length > 1){
      listAtivos.push("Média");      
      const mediaDividendYield = listDividendYield.reduce((accumulator, currentValue) => accumulator + currentValue);
      const mediaDividendYieldOnCost = listDividendYieldOnCost.reduce((accumulator, currentValue) => accumulator + currentValue);
      listDividendYield.push((mediaDividendYield / listAtivos.length).toFixed(2));
      listDividendYieldOnCost.push((mediaDividendYieldOnCost / listAtivos.length).toFixed(2));
    }

    this.setState({
      listAtivos,
      listDividendYield,
      listDividendYieldOnCost
    })
  }

  handleData = ( listAtivos, listDividendYield, listDividendYieldOnCost ) => {  
    const datasets = [
      {
        label: "Dividend yield",
        type: "bar",
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(94, 169, 255)',
        borderColor: 'rgb(109, 156, 65)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(80, 144, 217)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: listDividendYield
      },
      {
        label: "Dividend yield on cost",
        type: "bar",
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(2, 88, 209)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(3, 70, 166)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: listDividendYieldOnCost
      },
    ];

   
    return {
      labels: listAtivos,
      datasets
    };
  }

 

  render() {     
    const { 
      listAtivos = [],
      listDividendYield = [],
      listDividendYieldOnCost = []
    } = this.state;

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      tooltips: {
        mode: 'index',
        callbacks: {
          label: function(tooltipItem, dataSource) {
              const dataset = dataSource.datasets[tooltipItem.datasetIndex];
              return parseFloat(dataset.data[tooltipItem.index]).toFixed(2) + "%";   
          }
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          },
          maxBarThickness: 50,
        }],
        yAxes: [{
          ticks: {
            min: 0,
          },
          gridLines: {
            color: "black",
            borderDash: [2, 5],
          },
          scaleLabel: {
            display: true,
            fontColor: "green"
          }
        }]
      }
    }

    const {  title = "", renderModeCarossel } = this.props;

    const data = this.handleData(listAtivos, listDividendYield, listDividendYieldOnCost);
  
    return (
      <div className="chart-bar">
        <StyledChartBar {...this.props}>
          <Bar
            data={data} 
            height={500}
            width={700}
            options={options} 
          />
        </StyledChartBar>   
      </div>
    );
  }
}