import React from 'react';
import { Bar } from 'react-chartjs-2';
import {isMobile} from 'react-device-detect';
import moment from 'moment';

import { converterDinheiroParaFloat } from 'Util/Formatacao'
import ModalBarCustoValorAtivo from 'Pages/main/ModalBarCustoValorAtivo'
import StyledChartBar from 'Pages/charts/StyledChartBar';

const options = {
  responsive: true,
  maintainAspectRatio: isMobile ? false : true,
  tooltips: {
    callbacks: {
        label: function(tooltipItem, data) {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const currentValue = dataset.data[tooltipItem.index]; 
            const valorConvertido = parseFloat(currentValue).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
            return `${dataset.label}: ${valorConvertido}`;
        },
    }
  },
  scales: {
    xAxes: [{
      maxBarThickness: 50,
      stacked: true,
      gridLines: {
        display: false,
      }
    }],
    yAxes: [{
      stacked: true,
      ticks: {
        min: 0,
      },
      gridLines: {
        color: "black",
        borderDash: [2, 5],
      },
      scaleLabel: {
        display: true,
        labelString: "R$",
        fontColor: "green"
      }
    }]
  }
}

export default class GraficoBarDividendosRecebidos extends React.Component {

  state={};

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  componentDidUpdate(nextProps){
    if(nextProps !== this.props ){
        if(nextProps.renderModalPrecoCusto != this.props.renderModalPrecoCusto){
          this.setState({renderModalPrecoCusto: true});
        }
    }         
}

  iniciarVariaveisChart(){
    const { data = [], title = "" } = this.props;
    
    const labels = [];
    const dividendos = [];
    
    for (let i = 0; i < data.length; i++ ) {
      const ativoVez = data[i];
      let valorDividendos = 0;

      ativoVez.listaDeDividendos.forEach(element => {
        const now = moment(new Date()); // Data de hoje
        const past = moment(element.dataCom); // Outra data no passado
        const duration = moment.duration(now.diff(past));
        const days = duration.asDays();

        if(days > 1){
          valorDividendos += element.valorProvento
        }        
      });

      if(valorDividendos > 0){
        labels.push(ativoVez.ticker.descricao);
        dividendos.push(valorDividendos);
      }
    }

    this.setState({
      labels, 
      dividendos 
    })
  }


  handleData = (labels = [], dividendos = [] ) => {  
    const datasets = [
      {
        label: "Dividendos",
        type: "bar",
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(104, 193, 197)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(76, 166, 131)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: dividendos
      }
    ]
    return {
      labels,
      datasets
    };
  }

  
  handleCancel(){
    this.iniciarVariaveisChart();
    this.setState({ renderModalPrecoCusto: false, checkAtive: false})
    this.props.resetCarrosel()
  }

  render() {     
    const { 
      renderModalPrecoCusto = false,
      labels = [], 
      dividendos = []
    } = this.state;

    const {  title = "", renderModeCarossel } = this.props;

    const data = this.handleData(labels, dividendos);

    return (
      <div className="chart-bar">
        {!isMobile && (
          <ModalBarCustoValorAtivo
            visible={renderModalPrecoCusto}
            handleCancel={() => this.handleCancel()}
            data={data}
            onChangeCheckBox={this.onChangeCheckBox}
            options={options}
          />
        )}
        <StyledChartBar {...this.props}>
          <Bar
            data={data} 
            height={500}
            width={700}
            options={options} 
          />
        </StyledChartBar>         
      </div>
    );
  }
}