
import styled from 'styled-components';

const StyledChartPie = styled.div`
    @media (max-width: 2560px) {
        width: 650px;
        height: 500px;
        top: 65%;
        left: 50%;
    }

    @media (max-width: 768px) {
        width: 430px;
        height: 430px;
        top: 55%;
        left: 50%;
    }

    max-width: calc(100% - 20px);
    margin: 0;
    position: absolute;
    margin-right: -50%;
    transform: translate(-50%, -50%) }
`;

export default StyledChartPie;
