import React from 'react';
import { Bar } from 'react-chartjs-2';
import {isMobile} from 'react-device-detect';

import ModalBarCustoValorAtivo from 'Pages/main/ModalBarCustoValorAtivo'
import StyledChartBar from 'Pages/charts/StyledChartBar';

export default class GraficoBarDividendosAReceber extends React.Component {

  state={};

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  componentDidUpdate(nextProps){
    if(nextProps !== this.props ){
        if(nextProps.renderModalPrecoCusto != this.props.renderModalPrecoCusto){
          this.setState({renderModalPrecoCusto: true});
        }
    }         
}

  iniciarVariaveisChart(){
    const { data = [], title = "" } = this.props;

    const labels = [];
    const dividendos = [];

    for (let i = 0; i < data.length; i++ ) {
      labels.push(data[i].ativo.nomePregao);
      dividendos.push(data[i].valorProvento);
    }

    this.setState({
      labels, 
      dividendos 
    })
  }


  handleData = (labels = [], dividendos = [] ) => {  
    const datasets = [
      {
        label: "Dividendos",
        type: "bar",
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(104, 193, 197)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(76, 166, 131)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: dividendos
      }
    ]
    return {
      labels,
      datasets
    };
  }

  
  handleCancel(){
    this.iniciarVariaveisChart();
    this.setState({ renderModalPrecoCusto: false, checkAtive: false})
    this.props.resetCarrosel()
  }

  render() {     
    const { 
      renderModalPrecoCusto = false,
      labels = [], 
      dividendos = []
    } = this.state;

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      tooltips: {
        callbacks: {
            label: (tooltipItem, data) => {
                const dataset = data.datasets[tooltipItem.datasetIndex];
                const currentValue = dataset.data[tooltipItem.index]; 
                const valorConvertido = parseFloat(currentValue).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                return `${dataset.label}: ${valorConvertido}`;
            },
        }
      },
      scales: {
        xAxes: [{
          maxBarThickness: 50,
          stacked: true,
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          stacked: true,
          ticks: {
            min: 0,
          },
          gridLines: {
            color: "black",
            borderDash: [2, 5],
          },
          scaleLabel: {
            display: true,
            labelString: "R$",
            fontColor: "green"
          }
        }]
      }
    }

    const data = this.handleData(labels, dividendos);

    return (
      <div className="chart-bar">
        {!isMobile && (
          <ModalBarCustoValorAtivo
            visible={renderModalPrecoCusto}
            handleCancel={() => this.handleCancel()}
            data={data}
            onChangeCheckBox={this.onChangeCheckBox}
            options={options}
          />
        )}
        <StyledChartBar {...this.props}>
          <Bar
            data={data} 
            height={500}
            width={700}
            options={options} 
          />
        </StyledChartBar>         
      </div>
    );
  }
}