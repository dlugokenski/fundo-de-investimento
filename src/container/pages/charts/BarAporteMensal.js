import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Select, Button, Tooltip, Popover } from 'antd';
import {isMobile} from 'react-device-detect';

import { converterDinheiroParaFloat } from 'Util/Formatacao'
import { meses, trimestre, semestre} from 'Util/Periodo'
import { hexToRGBA } from 'Util/ColorUtil'
import FilterDataGrafico from 'Componente/FilterDataGrafico'
import StyledChartBar from 'Pages/charts/StyledChartBar';
import { coresTiposAtivos } from 'Util/Cores'

const { Option } = Select;

export default class GraficoBar extends React.Component {

  state={ 
    valueSelect : 1, 
    filtros: {
      filtrar: false,
      values: {}
  }
  };

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  iniciarVariaveisChart(){    
    const { data = [] } = this.props;
    const saldoAcao = [];
    const saldoTesouro = [];
    const saldoFII = [];
    const saldoGeral = [];
    const labelGrafico = [];

    const tamanhoArray = data.length;

    // const menorData = `${data[0].mesCopetencia}/${data[0].anoCopetencia}`
    // const maiorData = `${data[tamanhoArray- 1].mesCopetencia}/${data[tamanhoArray- 1].anoCopetencia}`

    // let menorValor = converterDinheiroParaFloat(data[0].listaDeTrade[0].valorTotalOperacao) || 0;
    // let maiorValor = 0;

    // for(let i = 0; i < tamanhoArray; i++){
    //   const itemDaVez = data.aportesMensal[i];
      
    //   const calculoTrade = this.calculoTipoDeTrade(itemDaVez);
    //   const valorEmAcao = calculoTrade.valorEmAcao;
    //   const valorTesouro = calculoTrade.valorTesouro;

    //   const element = {
    //     valorEmAcao,
    //     valorTesouro,
    //     mesCopetencia: itemDaVez.mesCopetencia,
    //     anoCopetencia: itemDaVez.anoCopetencia
    //   }

    //   if(this.checkFiltros(element)){       
    //     labelGrafico.push(`${meses[itemDaVez.mesCopetencia]}/${itemDaVez.anoCopetencia}`);
    //     saldoGeral.push(converterDinheiroParaFloat(itemDaVez.valorTotal)); 
    //     saldoAcao.push(valorEmAcao);
    //     saldoTesouro.push(valorTesouro);
    //   } 

    //   const calculoLimitesValor = this.calcularMenorValor(element, menorValor, maiorValor);
    //   menorValor = calculoLimitesValor.menorValor;
    //   maiorValor = calculoLimitesValor.maiorValor;

    // }

    // menorValor = menorValor.toFixed(2);
    // maiorValor = maiorValor.toFixed(2);

    for(let i = 0; i < tamanhoArray; i++){
      const itemDaVez = data[i];
      labelGrafico.push(itemDaVez.competencia);
      saldoGeral.push(itemDaVez.saldoTotal); 
      saldoAcao.push(itemDaVez.saldoAcao);
      saldoTesouro.push(itemDaVez.saldoTesouro);
      saldoFII.push(itemDaVez.saldoFundoImobiliario)
    }

    this.setState({ 
      saldoAcao, 
      saldoTesouro, 
      saldoGeral, 
      saldoFII,
      labelGrafico, 
      labelHover: 'mensal', 
      dataOriginal: data, 
      // menorData, maiorData, 
      // menorValor, maiorValor 
    });
  }

  // montarSalvarObejetoDataTrabalhada(data = [], labelHover = ""){
  //   const saldoGeral = [];
  //   const labelGrafico = [];
  //   const saldoAcao = [];
  //   const saldoTesouro = [];
  //   let menorValor = data[0].valorTotal || 0;
  //   let maiorValor = 0;
    
  //   data.forEach(element => {      

  //     if(this.checkFiltros(element)){
  //       saldoGeral.push(element.valorTotal); 
  //       labelGrafico.push(element.periodoReferencia);
  //       saldoAcao.push(element.valorEmAcao);
  //       saldoTesouro.push(element.valorTesouro);
  //     }

  //     const calculoLimitesValor = this.calcularMenorValor(element, menorValor, maiorValor);
  //     menorValor = calculoLimitesValor.menorValor;
  //     maiorValor = calculoLimitesValor.maiorValor;

  //   });
  
  //   menorValor = menorValor.toFixed(2);
  //   maiorValor = maiorValor.toFixed(2);

  //   this.setState({ labelGrafico, saldoGeral, saldoAcao, saldoTesouro, labelHover, menorValor, maiorValor });
  // }

  // calcularMenorValor(element, menorValor, maiorValor){
  //   menorValor = parseFloat(menorValor);
  //   maiorValor = parseFloat(maiorValor);

  //   if(element.valorEmAcao !== 0 && element.valorEmAcao < menorValor){
  //     menorValor = element.valorEmAcao;
  //   } else if(element.valorTesouro !== 0 && element.valorTesouro < menorValor){
  //     menorValor = element.valorTesouro;
  //   } 

  //   if(element.valorEmAcao !== 0 && element.valorEmAcao > maiorValor){
  //     maiorValor = element.valorEmAcao;
  //   } else if(element.valorTesouro !== 0 && element.valorTesouro > maiorValor){
  //     maiorValor = element.valorTesouro;
  //   } 

  //   menorValor = menorValor === 0 ? menorValor : parseFloat(menorValor.toFixed(2)); 
  //   maiorValor = maiorValor === 0 ? maiorValor : parseFloat(maiorValor.toFixed(2)); 

  //   return {menorValor, maiorValor}
  // }

  // handleChangeSelect(value) {
  //   const { dataOriginal = [] } = this.state;  

  //   if(value === 1){
  //     this.iniciarVariaveisChart();
  //   } else if(value === 2) {
  //     this.montarSalvarObejetoDataTrabalhada(this.getNewDataSelect(dataOriginal, trimestre, "trimestre", value), "trimestral");
  //   } else if (value === 3){
  //     this.montarSalvarObejetoDataTrabalhada(this.getNewDataSelect(dataOriginal, semestre, "semestre", value), "semestral");
  //   } else if (value === 4) {
  //     this.montarSalvarObejetoDataTrabalhada(this.getNewDataSelect(dataOriginal, semestre, "", value), "anual");
  //   }

  //   this.setState({valueSelect: value});
  // }

  handleOpitionSelect(value){
    this.setState({  
      filtros: {
        filtrar: false,
        values: {} 
      }
    },
    //() => { this.handleChangeSelect(value); }
    );
  }
 

  // checkFiltros(element){
  //   const { filtros } = this.state;

  //   let menorValorFiltro;
  //   let maiorValorFiltro;
  //   let maiorDataFiltro;
  //   let menorDataFiltro;

  //   if(filtros.filtrar){
  //     menorValorFiltro =  filtros.values.valorMinimo ? filtros.values.valorMinimo : undefined;
  //     maiorValorFiltro = filtros.values.valorMaximo ? filtros.values.valorMaximo : undefined;
  //     maiorDataFiltro = filtros.values.dataFinal ? { mes: parseInt(filtros.values.dataFinal.mes), ano: parseInt(filtros.values.dataFinal.ano) } : undefined;
  //     menorDataFiltro = filtros.values.dataInicial ? {  mes: parseInt(filtros.values.dataInicial.mes), ano: parseInt(filtros.values.dataInicial.ano) } : undefined;
  //   }

  //   let checkFilter = true;

  //   if(maiorDataFiltro){
  //     if(element.anoCopetencia > maiorDataFiltro.ano){
  //       checkFilter = false;
  //     } else if ((element.anoCopetencia === maiorDataFiltro.ano) && (element.mesCopetencia > maiorDataFiltro.mes)){
  //       checkFilter = false;
  //     } 
  //   }

  //   if(menorDataFiltro){
  //     if(element.anoCopetencia < menorDataFiltro.ano){
  //       checkFilter = false;
  //     } else if ((element.anoCopetencia === menorDataFiltro.ano) && (element.mesCopetencia < menorDataFiltro.mes)){
  //       checkFilter = false;
  //     } 
  //   }
    
  //   if(menorValorFiltro){
  //     if(element.valorEmAcao !== 0) {
  //       if(element.valorEmAcao <= menorValorFiltro){
  //         checkFilter = false; 
  //       } 
  //     }      
  //     if(element.valorTesouro !== 0) {
  //       if(element.valorTesouro < menorValorFiltro){
  //         checkFilter = false; 
  //       } 
  //     }
  //   }

  //   if(maiorValorFiltro){
  //     if(element.valorEmAcao !== 0) {
  //       if(element.valorEmAcao > maiorValorFiltro){
  //         checkFilter = false; 
  //       } 
  //     }      
  //     if(element.valorTesouro !== 0) {
  //       if(element.valorTesouro > maiorValorFiltro){
  //         checkFilter = false; 
  //       } 
  //     }
  //   }
  
  //   return checkFilter;
  // }

  // getNewDataSelect(data = [], opcaoSelect, separador, value){
  //   const ajustarData = [];

  //   for(let i = 0; i < data.aportesMensal.length; i++){
  //     const elementoVez = data.aportesMensal[i];

  //     const periodoReferencia = value !== 4 ? 
  //       `${opcaoSelect[elementoVez.mesCopetencia]}° ${separador} de ${elementoVez.anoCopetencia}` 
  //       : elementoVez.anoCopetencia; 
        
  //     const objetoVez = ajustarData.find(element => element.periodoReferencia === periodoReferencia);
  //     const valorTotal = converterDinheiroParaFloat(elementoVez.valorTotal);
  //     const calculoTrade = this.calculoTipoDeTrade(elementoVez);
  //     const valorEmAcao = calculoTrade.valorEmAcao;
  //     const valorTesouro = calculoTrade.valorTesouro;
  //     const anoCopetencia = elementoVez.anoCopetencia;
  //     const mesCopetencia = elementoVez.mesCopetencia;

  //     if(!objetoVez){  
  //       ajustarData.push({ periodoReferencia, valorTotal, valorEmAcao, valorTesouro, anoCopetencia, mesCopetencia });
  //     } else {
  //       const index = ajustarData.indexOf(ajustarData.find(result => result.periodoReferencia === periodoReferencia));
  //       ajustarData[index].valorEmAcao += valorEmAcao;
  //       ajustarData[index].valorTesouro += valorTesouro;
  //       ajustarData[index].valorTotal += valorTotal;
  //     }
  //   }
  //   return ajustarData;
  // }

  // calculoTipoDeTrade(data){
  //   let valorEmAcao = 0;
  //   let valorTesouro = 0;

  //   for(let i = 0; i < data.listaDeTrade.length; i++){
  //     const element = data.listaDeTrade[i];
  //     if(element.tipo === "acao"){
  //       if(element.compraVenda === "C"){
  //         valorEmAcao = valorEmAcao + converterDinheiroParaFloat(element.valorTotalOperacao);
  //       } else {
  //         valorEmAcao = valorEmAcao - converterDinheiroParaFloat(element.valorTotalOperacao);
  //       }
        
  //     } else {
  //       if(element.compraVenda === "C"){
  //         valorTesouro = valorTesouro + converterDinheiroParaFloat(element.valorTotalOperacao);
  //       } else {
  //         valorTesouro = valorTesouro - converterDinheiroParaFloat(element.valorTotalOperacao);
  //       }
  //     }
  //   }

  //   return { valorEmAcao, valorTesouro }
  // }
  conteudoInformacao (){
    return(
    <React.Fragment>
      Os valores dos aportes mostrados neste grafico são resultantes<br />
      das operações realizadas dentro dos periodos especificados. <br />
      O calculo leva em consideração o total compras menos o total de vendas. <br/>
      <center> 
        <Button 
          type="primary" 
          onClick={() => this.setState({ visiblePoupover: false})}
          style={{marginTop: "20px"}}
        >
          Ok entendi
        </Button>
      </center>
    </React.Fragment>
    )      
  };

  verificarSeTemSaldoAcaoOuTesouroOuFII(){
    const { saldoAcao = [], saldoTesouro = [], saldoFII = [] } = this.state;
    let contemAcao = false;
    let contemTesouro = false;
    let contemFundoImobiliario = false;

    saldoAcao.forEach(elementAcao => {
      if(elementAcao !== 0){
        contemAcao = true;
      }
    });

    saldoFII.forEach(elementFII => {
      if(elementFII !== 0){
        contemFundoImobiliario = true;
      }
    });

    saldoTesouro.forEach(element => {
      if(element !== 0){
        contemTesouro = true;
      }
    });

    if(contemAcao && contemTesouro || contemAcao && contemFundoImobiliario || contemTesouro && contemFundoImobiliario){
      return true;
    } else{
      return false;
    }
  }
  montarDataGrafico(){
    const { labelHover = "", saldoAcao = [], saldoTesouro = [], saldoGeral = [], labelGrafico = [], saldoFII = [] } = this.state;
    
    let dataGrafico = {};

    if(saldoAcao.length > 0 || saldoTesouro.length > 0 || saldoGeral.length > 0 || saldoFII.length > 0){

      dataGrafico= {
        labels: labelGrafico,
        datasets: []
      }
     
      if( this.verificarSeTemSaldoAcaoOuTesouroOuFII() ){ 
        dataGrafico.datasets.push(
          {
            label: `Aporte ${labelHover} geral`,
            type: "line",
            borderColor: "rgba(94, 169, 255)",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: saldoGeral
          }    
        )
      }
          
      for(let b = 0; b < saldoTesouro.length; b++){
        if(saldoTesouro[b] !== 0){

          dataGrafico.datasets.push(
            {
              label: `Aporte ${labelHover} tesouro`,
              type: "bar",
              barPercentage: '1',
              categoryPercentage: '1',
              backgroundColor: coresTiposAtivos['Tesouro Direto'].cor,
              borderColor: coresTiposAtivos['Tesouro Direto'].cor,
              borderWidth: '1',
              borderSkipped: 'bottom',
              hoverBackgroundColor: coresTiposAtivos['Tesouro Direto'].cor,
              hoverBorderWidth: '1',
              data: saldoTesouro
            }
          )
          break;
        }     
      }
  
      for(let c = 0; c < saldoAcao.length; c++){
        if(saldoAcao[c] !== 0){
          dataGrafico.datasets.push(
            {
              label: `Aporte ${labelHover} ação`,
              type: "bar",
              barPercentage: '1',
              categoryPercentage: '1',
              backgroundColor: coresTiposAtivos['Ações'].cor,
              borderColor: coresTiposAtivos['Ações'].cor,
              borderWidth: '1',
              borderSkipped: 'bottom',
              hoverBackgroundColor: coresTiposAtivos['Ações'].cor,
              hoverBorderWidth: '1.7',
              data: saldoAcao
            }
          )
          break;
        }
      }   

      for(let c = 0; c < saldoFII.length; c++){
        if(saldoFII[c] !== 0){
          dataGrafico.datasets.push(
            {
              label: `Aporte ${labelHover} fundo imobiliário`,
              type: "bar",
              barPercentage: '1',
              categoryPercentage: '1',
              backgroundColor: coresTiposAtivos['FII'].cor,
              borderColor: coresTiposAtivos['FII'].cor,
              borderWidth: '1',
              borderSkipped: 'bottom',
              hoverBackgroundColor: coresTiposAtivos['FII'].cor,
              hoverBorderWidth: '1.7',
              data: saldoFII
            }
          )
          break;
        }
      }   
      
    }    
    
    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
                const dataset = data.datasets[tooltipItem.datasetIndex];
                const currentValue = dataset.data[tooltipItem.index]; 
                const valorConvertido = currentValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                return `${dataset.label}: ${valorConvertido}`;
            },
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          }, 
          maxBarThickness: 50,
        }],
        yAxes: [{
          gridLines: {
            color: "black",
            borderDash: [2, 5],
          },
          scaleLabel: {
            display: true,
            labelString: "R$",
            fontColor: "green"
          }
        }]
      }
    }

    return { dataGrafico, options };
  }

  onOkFiltro (param){ 
    const { valueSelect = 1 } = this.state;   
    this.setState({ filtros: param.filtros },
     () => {
      if(param.filtros.filtrar){  
    //    this.handleChangeSelect(valueSelect);
      }
     });    
  }

  render() {     
    const { 
      visiblePoupover = false, 
      menorValor = 0,
      maiorValor = 0,
      menorData,
      maiorData
    } = this.state;

    const { renderModeCarossel } = this.props;

    const dataMontada = this.montarDataGrafico();
    const options = dataMontada.options;
    const data = dataMontada.dataGrafico;
   
    return (
      <div className="chart-bar">
        {renderModeCarossel ? (
          <React.Fragment>
            <div>
              <div style={{ position: 'relative', float: 'left' }}>
              {'Forma de visualização'} <br />
              <Select defaultValue={1}  onChange={element => this.handleOpitionSelect(element)} style={{marginTop: '10px'}}>
                <Option value={1}>Mensalmente</Option>
                <Option value={2}>Trimestral</Option>
                <Option value={3}>Semestral</Option>
                <Option value={4}>Anualizado</Option>
              </Select>
              </div>
              <Tooltip placement="left" title="Alerta"  style={{ position: 'relative', float: 'right' }}>
                <Popover 
                  placement="left"
                  content={this.conteudoInformacao()}         
                  trigger="click"
                  visible={visiblePoupover}
                  onVisibleChange={() => this.setState({ visiblePoupover: true })}
                >
                  <Button 
                    style={{ float: "right", position: 'relative'}}
                    type="primary" 
                    icon="alert"
                    shape="circle" 
                  />
                </Popover>
              </Tooltip>
              <div style={{ position: 'relative', float: 'right', marginRight: '10px'}}>
                <FilterDataGrafico
                  menorValor={menorValor} 
                  maiorValor = {maiorValor} 
                  menorData={menorData} 
                  maiorData={maiorData} 
                  onOk={filtros => this.onOkFiltro(filtros)}
                />
              </div>
            </div>
            <Bar
              data={data || {}} 
              height={500}
              width={700}
              options={options} 
            />
          </React.Fragment>
        ) : (
          <StyledChartBar>
            <Bar
              data={data || {}} 
              height={500}
              width={700}
              options={options} 
            />
          </StyledChartBar>
        )}
      </div>
    );
  }
}