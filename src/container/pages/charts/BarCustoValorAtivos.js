import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Button } from 'antd';
import { isMobile } from 'react-device-detect';
import { connect } from 'react-redux';

import ModalBarCustoValorAtivo from 'Pages/main/ModalBarCustoValorAtivo';
import Title from 'Componente/Title';
import FilterDataGrafico from 'Componente/FilterDataGrafico';
import StyledChartBar from 'Pages/charts/StyledChartBar';
import { TIPO_ATIVOS } from 'Util/Constantes';

class GraficoBar extends React.Component {
  state = {};

  componentDidMount() {
    this.iniciarVariaveisChart();
  }

  onOkFiltro(param) {}

  componentDidUpdate(nextProps) {
    if (nextProps !== this.props) {
      if (nextProps.renderModalPrecoCusto != this.props.renderModalPrecoCusto) {
        this.setState({ renderModalPrecoCusto: true });
      }
    }
  }

  iniciarVariaveisChart() {
    const { data = [], title = '' } = this.props;

    const ativos = [];
    const custos = [];
    const valor = [];
    const dividendos = [];
    const valorInvestidoReal = [];
    let dividendosTotalPagos = 0;

    for (let i = 0; i < data.listaTrade.length; i++) {
      const ativoVez = data.listaTrade[i];
      let dividendosPagos = 0;
      ativoVez.listaDeDividendos.forEach((elementDividendos) => {
        dividendosPagos += elementDividendos.valorProvento;
      });
      dividendosTotalPagos += dividendosPagos;

      if (ativoVez.ticker.ativo.tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO) {
        let minDescricao = ativoVez.ticker.descricao;
        minDescricao = minDescricao.replace('Tesouro', 'T.');
        minDescricao = minDescricao.replace('tesouro', 'T.');
        minDescricao = minDescricao.replace('IPCA', 'I.');
        minDescricao = minDescricao.replace('com Juros Semestrais', '');
        ativos.push(minDescricao);
      } else {
        ativos.push(ativoVez.ticker.descricao);
      }

      valor.push(ativoVez.valorAtualInvestido);
      custos.push(ativoVez.valorInvestido);
      dividendos.push(dividendosPagos);
      if (data.listaTrade.length > 1) {
        valorInvestidoReal.push(0);
      }
    }

    if (data.listaTrade.length > 1) {
      ativos.push('Soma total');
    }

    valor.push(data.valorAtualInvestido);
    custos.push(data.valorTotalInvestido);
    valorInvestidoReal.push(data.valorJusto);
    dividendos.push(dividendosTotalPagos);

    this.setState({
      ativosState: ativos,
      custosState: custos,
      valorState: valor,
      dividendosTotalPagosState: dividendos,
      valorInvestidoRealState: valorInvestidoReal,
      ativosProps: ativos,
      custosProps: custos,
      valorProps: valor,
      valorInvestidoRealProps: valorInvestidoReal,
      dividendosTotalPagosProps: dividendos,
    });
  }

  // iniciarVariaveisChart(){
  //   const { data = [], title = "" } = this.props;

  //   let menorValor = data.length > 0 ? data[0].custo : 0;
  //   let maiorValor = 0;
  //   const ativos = [];
  //   const custos = [];
  //   let valor = [];
  //   let somaDosCustos = 0;
  //   let somaValor = 0;

  //   for (let i = 0; i < data.length; i++ ) {
  //     const custosFormatado = converterDinheiroParaFloat(data[i].custo);
  //     const valoreFormatado = converterDinheiroParaFloat(data[i].valorTotalOperacao);
  //     ativos.push(data[i].codigo);
  //     custos.push(custosFormatado)
  //     valor.push(valoreFormatado)
  //     somaDosCustos = parseFloat(somaDosCustos) + parseFloat(custosFormatado);
  //     somaValor = parseFloat(somaValor) + parseFloat(valoreFormatado);

  //     if(somaDosCustos > maiorValor){
  //       maiorValor = somaDosCustos;
  //     } else if(somaValor > maiorValor){
  //       maiorValor = somaValor;
  //     }

  //     if(somaDosCustos < menorValor){
  //       menorValor = somaDosCustos;
  //     } else if(somaValor < menorValor){
  //       menorValor = somaValor;
  //     }

  //   }

  //   if(data.length > 0){
  //     ativos.push('Soma total');
  //     custos.push(somaDosCustos);
  //     valor.push(somaValor);
  //   }

  //   this.setState({
  //     ativosState: ativos,
  //     custosState: custos,
  //     valorState: valor,
  //     ativosProps: ativos,
  //     custosProps: custos,
  //     valorProps: valor,
  //     menorValor,
  //     maiorValor,
  //     title
  //   });
  // }

  handleData = (
    ativos = [],
    custos = [],
    valor = [],
    valorInvestidoRealProps = [],
    dividendosTotalPagosState = []
  ) => {
    const datasets = [];

    if (
      custos[custos.length - 1] &&
      valorInvestidoRealProps[custos.length - 1]
    ) {
      if (
        custos[custos.length - 1].toFixed(0) !==
        valorInvestidoRealProps[custos.length - 1].toFixed(0)
      ) {
        datasets.push({
          stack: 'Stack 0',
          label: 'Custo de aquisição (valor verdadeiro investido)',
          type: 'bar',
          barPercentage: '1',
          categoryPercentage: '1',
          backgroundColor: 'rgb(94, 169, 255)',
          borderColor: 'rgb(109, 156, 65)',
          borderWidth: '1',
          borderSkipped: 'bottom',
          hoverBackgroundColor: 'rgb(80, 144, 217)',
          hoverBorderColor: 'rgb(29, 41, 57)',
          hoverBorderWidth: '1.7',
          data: valorInvestidoRealProps,
        });
      }
    }

    datasets.push(
      {
        stack: 'Stack 1',
        label: 'Custo de aquisição (Carteira atual)',
        type: 'bar',
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(2, 88, 209)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(3, 70, 166)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: custos,
      },
      {
        stack: 'Stack 2',
        label: 'Valor atual',
        type: 'bar',
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(255, 123, 8)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(224, 108, 7)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: valor,
      },
      {
        stack: 'Stack 2',
        label: 'Dividendos pagos',
        type: 'bar',
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(104, 193, 197)',
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(76, 166, 131)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: dividendosTotalPagosState,
      }
    );
    return {
      labels: ativos,
      datasets,
    };
  };

  onChangeCheckBox = (e) => {
    const { ativosState = [], custosState = [], valorState = [] } = this.state;

    let checkAtive = false;
    if (e.target.checked) {
      checkAtive = true;

      ativosState.pop();
      custosState.pop();
      valorState.pop();

      this.setState({ ativosState, custosState, valorState });
    } else {
      this.iniciarVariaveisChart();
      checkAtive = false;
    }
    this.setState({ checkAtive });
  };

  handleCancel() {
    this.iniciarVariaveisChart();
    this.setState({ renderModalPrecoCusto: false, checkAtive: false });
    this.props.resetCarrosel();
  }

  renderOptions = () => {
    const { exponentialGraph } = this.props;
    return {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      tooltips: {
        mode: 'index',
        filter: function(tooltipItem, data) {
          if (tooltipItem.index === data.labels.length - 1) {
            return true;
          } else {
            if (data.datasets.length > 3) {
              return tooltipItem.datasetIndex !== 0;
            } else {
              return true;
            }
          }
        },
        callbacks: {
          label: function(tooltipItem, data) {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const currentValue = dataset.data[tooltipItem.index];
            const valorConvertido = parseFloat(
              currentValue
            ).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
            return `${dataset.label}: ${valorConvertido}`;
          },
        },
      },
      scales: {
        xAxes: [
          {
            stacked: true,
            gridLines: {
              display: false,
            },
            maxBarThickness: 50,
          },
        ],
        yAxes: [
          {
            type: exponentialGraph ? 'logarithmic' : 'linear',
            position: 'left',
            stacked: true,
            ticks: {
              min: 0,
              callback: function(value, index, values) {
                if (exponentialGraph) {
                  if (value === 1000000) return '1M';
                  if (value === 500000) return '500k';
                  if (value === 300000) return '300k';
                  if (value === 200000) return '200k';
                  if (value === 100000) return '100k';
                  if (value === 75000) return '75k';
                  if (value === 50000) return '50k';
                  if (value === 25000) return '25k';
                  if (value === 10000) return '10k';
                  if (value === 5000) return '5k';
                  if (value === 2500) return '2,5k';
                  if (value === 1000) return '1k';
                  if (value === 500) return '500';
                  if (value === 100) return '100';
                  if (value === 1) return '1';
                } else {
                  return Number(value.toString());
                }
              },
            },
            gridLines: {
              color: 'black',
              borderDash: [2, 5],
            },
            scaleLabel: {
              display: true,
              labelString: 'R$',
              fontColor: 'green',
            },
          },
        ],
      },
    };
  };

  render() {
    const {
      renderModalPrecoCusto = false,
      checkAtive = false,
      ativosState = [],
      custosState = [],
      valorState = [],
      ativosProps = [],
      custosProps = [],
      valorProps = [],
      valorInvestidoRealState = [],
      valorInvestidoRealProps = [],
      dividendosTotalPagosState = [],
      menorValor = 0,
      maiorValor = 0,
    } = this.state;

    const { title = '', renderModeCarossel } = this.props;

    const data = this.handleData(
      ativosState,
      custosState,
      valorState,
      valorInvestidoRealState,
      dividendosTotalPagosState
    );
    const dataModal = this.handleData(
      ativosProps,
      custosProps,
      valorProps,
      valorInvestidoRealProps,
      dividendosTotalPagosState
    );

    return (
      <div className="chart-bar">
        {!isMobile && (
          <React.Fragment>
            <ModalBarCustoValorAtivo
              visible={renderModalPrecoCusto}
              handleCancel={() => this.handleCancel()}
              data={dataModal}
              onChangeCheckBox={this.onChangeCheckBox}
              options={this.renderOptions()}
              checkAtive={checkAtive}
            />
            {renderModeCarossel && (
              <div style={{ marginTop: '10px', marginBottom: '10px' }}>
                <Title title={title} />
                <div style={{ marginTop: '-25px' }}>
                  <Button
                    type="primary"
                    icon="zoom-in"
                    onClick={() =>
                      this.setState({ renderModalPrecoCusto: true })
                    }
                    style={{
                      float: 'right',
                      marginRight: '10px',
                      position: 'relative',
                    }}
                    shape="circle"
                  />
                  <div
                    style={{
                      position: 'relative',
                      float: 'right',
                      marginRight: '10px',
                    }}
                  >
                    <FilterDataGrafico
                      menorValor={menorValor}
                      maiorValor={maiorValor}
                      onOk={(filtros) => this.onOkFiltro(filtros)}
                    />
                  </div>
                </div>
              </div>
            )}
          </React.Fragment>
        )}
        <StyledChartBar {...this.props}>
          <Bar
            data={data}
            height={500}
            width={700}
            options={this.renderOptions()}
          />
        </StyledChartBar>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  exponentialGraph: state.configuracaoAplication.exponentialGraph === 'true',
});

export default connect(mapStateToProps, null)(GraficoBar);
