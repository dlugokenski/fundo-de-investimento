import React from 'react';
import { Bar } from 'react-chartjs-2';
import {isMobile} from 'react-device-detect';
import moment from 'moment';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as actions} from 'Ducks/carteira'

import StyledChartBar from 'Pages/charts/StyledChartBar';

const options = {
  responsive: true,
  maintainAspectRatio: isMobile ? false : true,
  onClick: (evt, element, teste) => {
    if(element.length > 0){
      console.log('element[0]._index', element[0]._index)
    }
  },
  legend: {
    display: false
  },
  tooltips: {
    mode: 'index',
    filter: function (tooltipItem, data) {
      if(tooltipItem.value !== "0"){
        return true     
      }      
      return false;
    },
    callbacks: {
        label: function(tooltipItem, data) {
            const dataset = data.datasets[tooltipItem.datasetIndex];
            const currentValue = dataset.data[tooltipItem.index]; 
            const valorConvertido = parseFloat(currentValue).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
            return `${dataset.label}: ${valorConvertido}`;
        },
    }
  },
  scales: {
    xAxes: [{
      gridLines: {
        display: false,
      },
      maxBarThickness: 50,
    }],
    yAxes: [{
      ticks: {
        min: 0,
      },
      gridLines: {
        color: "black",
        borderDash: [2, 5],
      },
      scaleLabel: {
        display: true,
        fontColor: "green"
      }
    }]
  }
}

class GraficoBarEvolucaoDividendosAtivos extends React.Component {

  state={

  };

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  onOkFiltro (param){ 
    
  }

  iniciarVariaveisChart(){
    const { data = [], title = "" } = this.props;

    const dataOrdenada = data.sort((a, b) => {
      return new Date(a.periodo) < new Date(b.periodo) ? -1 : 0
    });

    const dataReorganizada = this.agruparDataPorPeriodoEativo(dataOrdenada);
    this.mountedData(dataReorganizada.list, dataReorganizada.empresas);
  }

  mountedData = ( data = [], empresas = [] ) => {
    const { coresAtivo } = this.props;

    const labels = data.map(element => moment(new Date(element.periodo)).format('MM/YYYY'));
    let dataEmpresaInitial = empresas.map(element => {
      const color = coresAtivo.find(elementColor => elementColor.ativo === element.id);
      
      return {
        ativo: element,
        color: color.corRgb,
        values: data.map(() => 0)
      }
    })

    data.forEach((elementData, index) => {
      elementData.listDividendos.forEach(elementDividendos => {
        const indexElement = dataEmpresaInitial.indexOf(dataEmpresaInitial.find(elementAtivo => elementAtivo.ativo.id === elementDividendos.ativo.id));
        dataEmpresaInitial[indexElement].values[index] += elementDividendos.valor
      })
    })

    const datasets = dataEmpresaInitial.map(elementDatasets =>({
        stack: 'Stack 0',
        label: elementDatasets.ativo.nomePregao,
        type: "bar",
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: elementDatasets.color,
        borderColor: 'rgb(29, 41, 57)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: elementDatasets.color,
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: elementDatasets.values
      }
    ));

    const dataReturn = {
      labels,
      datasets
    }
    
    this.setState({dataChart: dataReturn});
  }

  agruparDataPorPeriodoEativo(list){
    const newList = [];
    const empresas = [];

    list.forEach(element => {
      const newElementData = {
        ...element,
        listDividendos: []        
      }

      element.listDividendos.forEach(elementDividendo => {     
        const indexEmpresa = empresas.indexOf(empresas.find(elementEmpresa => elementEmpresa.id === elementDividendo.ativo.id));
        if(indexEmpresa === -1){
          empresas.push({
            ...elementDividendo.ativo,
            ticker: elementDividendo.ticker.descricao
          });
        }   
        if(newElementData.listDividendos.length === 0){
          newElementData.listDividendos.push({
            ativo: elementDividendo.ativo,
            valor: elementDividendo.valorProvento,
            periodo: element.periodo
          })
        } else {
          const index = newElementData.listDividendos.indexOf(
                          newElementData.listDividendos.find(
                            elementAtivo => elementAtivo.ativo.id === elementDividendo.ativo.id && elementAtivo.periodo === element.periodo
                          )
                        );
          if(index != -1){
            newElementData.listDividendos[index].valor = newElementData.listDividendos[index].valor + elementDividendo.valorProvento;
          } else {
            newElementData.listDividendos.push({
              ativo: elementDividendo.ativo,
              valor: elementDividendo.valorProvento,
              periodo: element.periodo
            })
          }
        }
      });
      newList.push(newElementData);
    });

    this.setState({empresas: empresas});
    return {
      list: newList,
      empresas: empresas
    }; 
  }

  render() {     
    const { dataChart = {labels : [], datasets: [] } } = this.state;
    const {  title = "", renderModeCarossel } = this.props;
  
    return (
      <div className="chart-bar">
        <StyledChartBar {...this.props}>
          <Bar
            data={dataChart} 
            height={500}
            width={700}
            options={options} 
          />
        </StyledChartBar>   
      </div>
    );
  }
}

const mapStateToProps = state => ({  
  coresAtivo: state.carteira.coresAtivo
})

const mapDispatchToPros = dispatch => bindActionCreators(actions, dispatch)
export default connect(mapStateToProps, mapDispatchToPros)(GraficoBarEvolucaoDividendosAtivos)
