import React from 'react';
import { Pie } from 'react-chartjs-2';
import { Button } from 'antd';
import { isMobile } from 'react-device-detect';

import { connect } from 'react-redux';

import Title from 'Componente/Title';
import ModalEditePie from './ModalEditePie';
import StyledChartPie from 'Pages/charts/StyledChartPie';
import { TIPO_ATIVOS } from 'Util/Constantes';

class GraficoPie extends React.Component {
  state = {};

  componentDidMount() {
    this.iniciarVariaveisChart();
  }

  componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.iniciarVariaveisChart();
    }
  }

  iniciarVariaveisChart() {
    const { data = {}, title = '', coresAtivo = [] } = this.props;

    const ativos = [];
    const valores = [];
    const coresCharts = [];
    const ativosProps = [];

    for (let i = 0; i < data.listaTrade.length; i++) {
      const dadosVez = data.listaTrade[i];
      const participacao =
        (dadosVez.valorAtualInvestido * 100) / data.valorAtualInvestido;
      const cor = coresAtivo.find(
        (element) => element.ativo === dadosVez.ticker.ativo.id
      );
      ativosProps.push(dadosVez);
      ativos.push(this.pegarNomeAtivo(dadosVez.ticker));
      valores.push(participacao);
      coresCharts.push(cor.corRgb);
    }
    this.setState({ ativos, valores, coresCharts, title, ativosProps });
  }

  pegarNomeAtivo(ticker) {
    const tipo = ticker.ativo.tipo.descricao;
    if (tipo === TIPO_ATIVOS.ACOES) {
      return ticker.ativo.nomePregao;
    } else if (tipo === TIPO_ATIVOS.TESOURO_DIRETO) {
      return ticker.descricao;
    } else if (tipo === TIPO_ATIVOS.FII) {
      return ticker.descricao;
    }
  }

  mudarCorDoGrafico = (alteracoes) => {
    const { coresCharts } = this.state;

    alteracoes.forEach((item) => {
      coresCharts[item.ativo] = item.cor;
    });
    this.setState({ coresCharts });
  };

  render() {
    const {
      ativos = [],
      valores = [],
      coresCharts = [],
      renderModalEditePie = false,
      title = '',
      ativosProps = [],
    } = this.state;
    const { renderModeCarossel } = this.props;
    const dataChart = {
      labels: ativos,
      datasets: [
        {
          data: valores,
          backgroundColor: coresCharts,
          hoverBackgroundColor: coresCharts,
        },
      ],
    };

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      legend: {
        onClick: null,
      },
      elements: {
        arc: {
          borderWidth: 0,
        },
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem, dataLabel) {
            const dataset = dataLabel.datasets[tooltipItem.datasetIndex];
            const total = dataset.data.reduce(function(
              previousValue,
              currentValue,
              currentIndex,
              array
            ) {
              return previousValue + currentValue;
            });
            const currentValue = dataset.data[tooltipItem.index];
            const percentage = parseFloat(
              ((currentValue / total) * 100).toFixed(2)
            );
            const label = dataLabel.labels[tooltipItem.index];

            const ativo = ativosProps.find((element) => {
              const ativoTicker = element.ticker.ativo;
              if (
                ativoTicker.tipo.descricao === TIPO_ATIVOS.ACOES &&
                ativoTicker.nomePregao === label
              ) {
                return true;
              } else if (
                ativoTicker.tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO &&
                element.ticker.descricao === label
              ) {
                return true;
              } else if (
                ativoTicker.tipo.descricao === TIPO_ATIVOS.FII &&
                element.ticker.descricao === label
              ) {
                return true;
              } else {
                return false;
              }
            });
            let valor = '';
            if (ativo && ativo.valorAtualInvestido) {
              valor = ativo.valorAtualInvestido.toLocaleString('pt-BR', {
                style: 'currency',
                currency: 'BRL',
              });
            }
            return `${label}: ${percentage}% | ${valor}`;
          },
        },
      },
    };

    return (
      <React.Fragment>
        <ModalEditePie
          onChangeColor={this.mudarCorDoGrafico}
          onChangeAgruparAtivos={this.agruparAtivos}
          visible={renderModalEditePie}
          handleCancel={() => this.setState({ renderModalEditePie: false })}
          data={dataChart}
        />
        {renderModeCarossel ? (
          <React.Fragment>
            <Title title={title} />
            <Button
              type="primary"
              icon="edit"
              onClick={() => this.setState({ renderModalEditePie: true })}
              style={{
                float: 'right',
                marginTop: '-22px',
                position: 'relative',
                marginBotton: '20px',
              }}
              shape="circle"
            />
            <StyledChartPie>
              <Pie
                redraw={true}
                data={dataChart}
                height={300}
                width={500}
                options={options}
              />
            </StyledChartPie>
          </React.Fragment>
        ) : (
          <StyledChartPie>
            <Pie
              redraw={true}
              data={dataChart}
              height={300}
              width={500}
              options={options}
            />
          </StyledChartPie>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  coresAtivo: state.carteira.coresAtivo,
});

export default connect(mapStateToProps, null)(GraficoPie);
