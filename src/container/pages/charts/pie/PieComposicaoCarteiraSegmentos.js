import React from "react";
import { Pie } from "react-chartjs-2";
import { isMobile } from "react-device-detect";

import { defaultCores } from "Util/Cores";
import StyledChartPie from "Pages/charts/StyledChartPie";

export default class PieComposicaoCarteira extends React.Component {
  state = {};

  componentDidMount() {
    this.iniciarVariaveisChart();
  }

  componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.iniciarVariaveisChart();
    }
  }

  iniciarVariaveisChart() {
    const { data = {}, title = "" } = this.props;

    const segmentos = [];
    const valores = [];
    const coresCharts = [];
    const segmentosSeparados = [];
    const ativosComSegmento = [];

    data.listaTrade.forEach((element) => {
      if (
        element &&
        element.ticker &&
        element.ticker.ativo &&
        element.ticker.ativo.segmento &&
        element.ticker.ativo.segmento.id
      ) {
        ativosComSegmento.push(element);
      }
    });

    for (let i = 0; i < ativosComSegmento.length; i++) {
      const dadosVez = ativosComSegmento[i];
      let segmentoJaContabilizado = false;
      let valorInvestido = 0;

      segmentosSeparados.forEach((element) => {
        if (dadosVez.ticker.ativo.segmento.id === element.segmento.id) {
          segmentoJaContabilizado = true;
        }
      });

      if (!segmentoJaContabilizado) {
        data.listaTrade.forEach((element) => {
          if (
            dadosVez.ticker.ativo.segmento.id ===
            element.ticker.ativo.segmento.id
          ) {
            valorInvestido += element.valorAtualInvestido;
          }
        });
        segmentosSeparados.push({
          segmento: dadosVez.ticker.ativo.segmento,
          valor: valorInvestido,
        });
      }
    }

    segmentosSeparados.forEach((element, index) => {
      segmentos.push(element.segmento.descricao);
      valores.push(element.valor);
      coresCharts.push(defaultCores[index]);
    });

    this.setState({ segmentos, valores, coresCharts });
  }

  render() {
    const { segmentos = [], valores = [], coresCharts = [] } = this.state;

    const dataChart = {
      labels: segmentos,
      datasets: [
        {
          data: valores,
          backgroundColor: coresCharts,
          hoverBackgroundColor: coresCharts,
        },
      ],
    };

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      legend: {
        onClick: null,
      },
      elements: {
        arc: {
          borderWidth: 0,
        },
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem, dataLabel) {
            const dataset = dataLabel.datasets[tooltipItem.datasetIndex];
            const total = dataset.data.reduce(function(
              previousValue,
              currentValue,
              currentIndex,
              array
            ) {
              return previousValue + currentValue;
            });
            const currentValue = dataset.data[tooltipItem.index];
            const percentage = parseFloat(
              ((currentValue / total) * 100).toFixed(2)
            );
            const label = dataLabel.labels[tooltipItem.index];

            return `${label}: ${percentage}%`;
          },
        },
      },
    };

    return (
      <StyledChartPie>
        <Pie
          redraw={true}
          data={dataChart}
          height={300}
          width={500}
          options={options}
        />
      </StyledChartPie>
    );
  }
}
