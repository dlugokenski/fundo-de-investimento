import React from 'react';
import { Modal, Button, Col, Row, Tabs, Input, Checkbox, Card, Tooltip, notification, Collapse, Icon, Tag, Popconfirm  } from 'antd';
import { Pie } from 'react-chartjs-2';

import LegendItem from 'Componente/LegendItem';
import { randomHex } from 'Util/ColorUtil'
const { TabPane } = Tabs;
const Panel = Collapse.Panel;
export default class ModalEditePie extends React.Component {
    
    state = {};

    componentDidMount() {
        this.inciarDadosPagina();
    }

    componentDidUpdate(nextProps){
        if(nextProps !== this.props){
            this.inciarDadosPagina();
        }
    }

    inciarDadosPagina(){
        const { data = {} } = this.props;
        this.setState({ data, ativosCarteira: data.labels });
    }

    mudarCorDoGrafico = (cor, ativo) => {
        const { data } = this.state;
        let { alteracoes = [] } = this.state;
        data.datasets[0].backgroundColor[ativo] = cor.hex;

        const alteracao = {cor: cor.hex, ativo};

        if(alteracoes.length === 0){
            alteracoes.push(alteracao);
        } else {
            let elementoEncontrado = false;
            for(let i =0; i < alteracoes.length; i++) {
                if(alteracoes[i].ativo === ativo){
                    elementoEncontrado = true;
                    alteracoes[i] = alteracao;
                }
            }
            if(!elementoEncontrado){
                alteracoes.push(alteracao);
            }
        }
        this.setState({ alterarCor: alteracoes });
    }

    mudarCorDoGraficoGrupo = (grupo) => {
    }

    onOk(){
        const { alterarCor =    [], ativosSelecionados = [] } = this.state;
        if(alterarCor.length > 0){
            this.props.onChangeColor(alterarCor);
        }
        
        if(ativosSelecionados.length > 0){
            this.props.onChangeAgruparAtivos(ativosSelecionados);        
        }            
        this.setState({ alterarCor: undefined });
    }

    handleCheckAtivos(ativoSelecionado){
        const { ativosSelecionados = [] } = this.state;
        const valor = ativoSelecionado.target.value;
        const index = ativosSelecionados.indexOf(valor);
        if(ativoSelecionado.target.checked){
            if(index === -1 ) {
                ativosSelecionados.push(valor);
            } else{
                ativosSelecionados[index] = valor;
            }
        } else{
            ativosSelecionados.splice(index,1);
        }
       
        this.setState({ativosSelecionados});
    }

    openNotification(type, title, description) {   
        notification[type || 'info']({
          message: title || 'Aviso',
          description: description,
        });
      }

      validarNomeGrupoEAtivos(){
        const { nomeDoGrupo = "",  ativosSelecionados = [], dataAgrupar = {}, data = {} } = this.state;
        
        if(dataAgrupar.labels) {
            if(dataAgrupar.labels.indexOf(nomeDoGrupo.toUpperCase()) !== -1) {
                this.openNotification('error',"Erro", "Nome do ativo ou grupo já existente!");
                return false;
            }
        } else if (data.labels) {
            if(data.labels.indexOf(nomeDoGrupo.toUpperCase()) !== -1) {
                this.openNotification('error',"Erro", "Nome do ativo já existente!");
                return false;
            }
        }

        if( nomeDoGrupo !== "" ){
            return true;
        } else if (ativosSelecionados.length < 2) {
            this.openNotification('error',"Erro", "E exigido no minimo dois ativos para formar um grupo!");
            return false;
        } else {
            this.openNotification('error',"Erro", "Nome do grupo e um campo obrigatório!");
            return false;
        }
    }

    handleOptions = (dataSource) => {
        return(
            {
                responsive: true,
                legend: {
                        onClick: null
                    },
                tooltips: {
                    callbacks: {
                        label: function(tooltipItem, dataSource) {
                            const dataset = dataSource.datasets[tooltipItem.datasetIndex];
                            const total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                return previousValue + currentValue;
                            });
                            const currentValue = dataset.data[tooltipItem.index];
                            const percentage = parseFloat(((currentValue/total) * 100).toFixed(2));         
                            return percentage + "%";
                        }
                    }
                }            
            }
        )  
    }    

    getAtivosDisponiveisParaAgrupar(){
        const { ativosCarteira = [], ativosSelecionados = [] } = this.state;
        
        const sobraDosAtivos = [];
        for(let i = 0; i < ativosCarteira.length; i++){
            let ativoEncontrado = false;
            for(let a = 0; a < ativosSelecionados.length; a++){
                if(ativosCarteira[i] === ativosSelecionados[a]){
                    ativoEncontrado = true;
                }    
            }
            if(!ativoEncontrado){
                sobraDosAtivos.push(ativosCarteira[i]);
            }
        }
        return sobraDosAtivos;
    }

    onOkAgrupar(){
        
        const { data = {}, nomeDoGrupo = "", listaDeGrupos = [], dataAgrupar = {} } = this.state;
        const sobraDosAtivos = this.getAtivosDisponiveisParaAgrupar();
        const listAtivos = [];
        const ativos = [];
        const coresCharts = [];
        const valores = [];
        const ativosData = data.labels || [];
        let somaValoresGroup = 0;

        if( this.validarNomeGrupoEAtivos() ){
            for(let i = 0; i < ativosData.length; i++ ){ 
                const ativoVez = ativosData[i];
                if(sobraDosAtivos.indexOf(ativoVez) !== -1){
                    const cor = data.datasets[0].backgroundColor[i];
                    const valor = data.datasets[0].data[i];
                    ativos.push(ativoVez);
                    coresCharts.push(cor);
                    valores.push(valor);
                } 
                else {
                    const objetoVez = listaDeGrupos.find(element => element.listAtivos.find(result => result === ativoVez)); 
                    if(!objetoVez){
                        listAtivos.push(ativoVez);                    
                        somaValoresGroup = somaValoresGroup + data.datasets[0].data[i];  
                    } else {
                        if(ativos.indexOf(objetoVez.nomeDoGrupo.toUpperCase()) === -1){
                            const indexGroup = dataAgrupar.labels.indexOf(objetoVez.nomeDoGrupo.toUpperCase());
                            ativos.push(dataAgrupar.labels[indexGroup]);
                            coresCharts.push( dataAgrupar.datasets[0].backgroundColor[indexGroup]);
                            valores.push(dataAgrupar.datasets[0].data[indexGroup]);
                        }
                   
                    }                            
                }            
            }
          
            ativos.push(nomeDoGrupo.toUpperCase());
            coresCharts.push( randomHex());
            valores.push(somaValoresGroup);

            const dataAgruparNew = {
                labels: ativos,
                datasets: [{
                  data: valores,
                  backgroundColor: coresCharts,
                  hoverBackgroundColor: coresCharts
                }]
            };
        
            this.setState({
                nomeDoGrupo: undefined,
                dataAgrupar: dataAgruparNew, 
                ativosCarteira: sobraDosAtivos, 
                ativosSelecionados: [],  
                listaDeGrupos: [...listaDeGrupos, { nomeDoGrupo, listAtivos }],
                visibleListGrupo: true 
            });
        }
        
    }

    handleRemoveGroup(grupo){
        const { data = {}, listaDeGrupos = [], ativosCarteira = [], dataAgrupar = {} } = this.state;

        const newAtivosCarteira = [];
        const indexGroup = dataAgrupar.labels.indexOf(grupo.nomeDoGrupo.toUpperCase());
        dataAgrupar.labels.splice(indexGroup,1);
        dataAgrupar.datasets[0].data.splice(indexGroup,1);
        dataAgrupar.datasets[0].backgroundColor.splice(indexGroup,1);
        dataAgrupar.datasets[0].hoverBackgroundColor.splice(indexGroup,1);

        grupo.listAtivos.forEach(element => {
            newAtivosCarteira.push(element);
            const index = data.labels.indexOf(element);

            const valor = data.datasets[0].data[index];
            const cor = data.datasets[0].backgroundColor[index];

            dataAgrupar.labels.push(element);
            dataAgrupar.datasets[0].data.push(valor);
            dataAgrupar.datasets[0].backgroundColor.push(cor);
          //  dataAgrupar.datasets[0].hoverBackgroundColor.push(cor);
        })

        listaDeGrupos.splice(listaDeGrupos.indexOf(grupo),1); 
        
        this.setState({
            ativosCarteira: [...ativosCarteira, ...newAtivosCarteira], 
            listaDeGrupos,
            dataAgrupar,
            visibleListGrupo: false
        }); 
    }
    
    estruturaAgruparAtivos = () => {
        const { visibleListGrupo, listaDeGrupos = [] } = this.state;

        return(
            <Card 
                size="small" 
                title="Grupo de ativos" 
                extra={
                    <Tooltip placement="topLeft" title="Novo grupo">
                        {!visibleListGrupo && listaDeGrupos.length !== 0 && (
                            <Button 
                                disabled={listaDeGrupos.length === 0}
                                type="primary" 
                                shape="circle" 
                                icon="left" 
                                onClick = { () => this.setState({ visibleListGrupo: !visibleListGrupo })}
                            />
                        )}
                        {visibleListGrupo && (
                            <Button 
                                type="primary" 
                                shape="circle" 
                                icon="plus" 
                                onClick = { () => this.setState({ visibleListGrupo: !visibleListGrupo })}
                            />
                        )}
                    </Tooltip>
                    } 
            >
                {visibleListGrupo && listaDeGrupos.length > 0 ? (
                    this.listarGrupos()
                ) : (
                    this.agruparAtivos()
                )
                }
            </Card>
        )
    }

    listarGrupos = () => {
        const { listaDeGrupos = [] } = this.state;
        return(
            <Collapse>
                {listaDeGrupos.map((result, index) => (
                    <Panel 
                        header={result.nomeDoGrupo.toUpperCase()} 
                        extra={[
                            <Tooltip placement="left" title="Alterar a cor do grupo">
                                <Icon 
                                    style={{marginRight: "15px"}}
                                    type="picture" 
                                    onClick = {() => this.mudarCorDoGraficoGrupo(result)}
                                />
                            </Tooltip>,
                            <Popconfirm placement="left" title="Realmente deseja excluir este grupo?" onConfirm={() => this.handleRemoveGroup(result)} okText="Yes" cancelText="No">
                                 <Tooltip placement="left"  title="Excluir grupo">
                                    <Icon 
                                        type="delete" 
                                    />
                                </Tooltip>                               
                            </Popconfirm>
                        ]} 
                        key={index}
                    >
                        <Row gutter={16}  key= {`${index}${index}`}>
                            {result.listAtivos.map(ativos => (
                                <Col md={8}  key= {`${index}${ativos}${index}`}>
                                    <Tag style={{marginTop: "5px"}} key= {`${index}${ativos}`} color="geekblue">{ativos}</Tag>                    
                                </Col>                       
                            ))}                          
                        </Row>
                    </Panel>           
                ))}               
            </Collapse>
        )
    }

    agruparAtivos = () => {        
        const { ativosCarteira = [], ativosSelecionados = [], nomeDoGrupo = "" } = this.state;

        return(
            <React.Fragment>
                <Input 
                    placeholder={"Digite o nome do grupo..."}
                    value={nomeDoGrupo}
                    onChange={event => this.setState({ nomeDoGrupo: event.target.value})}
                />
                <Row style={{ marginTop: '10px' }}>
                    {ativosCarteira.map((result, index) => (
                        <Col md={12}  key= {`${index}${index}`}>    
                            <Checkbox 
                                checked={ ativosSelecionados.indexOf(result) !== -1 }
                                key= {index} 
                                value={result} 
                                onChange={element => this.handleCheckAtivos(element)}
                            >
                                {result}
                            </Checkbox>    
                        </Col>                    
                    ))}                    
                </Row>
                <Row>
                    <center>
                        <Button
                            disabled={ativosSelecionados.length === 0}
                            key="submit"
                            type="primary"
                            onClick={() => this.onOkAgrupar()}
                            style={{marginTop: '20px'}}
                            >
                            {'Agrupar'}
                        </Button>
                    </center>
                </Row>
            </React.Fragment>
        )        
    }

    alterColor = () => {
        const { data = {} } = this.state;
        const coresCharts =  data.datasets ? data.datasets[0].backgroundColor : [];
        return(            
            coresCharts.map(( result, index) => (
                <Row gutter={16}  key= {`${index}${index}`}>
                    <Col md={5} key= {index}>
                        <LegendItem color={result} index={ index } onChangeColor={this.mudarCorDoGrafico} />                          
                    </Col>
                    <Col md={19} key= {data.labels[index] } style={{ marginTop: '5px' }}>    
                        <b>{data.labels[index]}</b>                       
                    </Col>
                </Row>
                )
            )                
        )        
    }

    handleGrafico = (data, key) => {
        return(
            <Pie
                key={key}
                data={data} 
                height={300}
                width={500}
                options={this.handleOptions(data)} 
            />
        )
    } 
   

    render(){ 
        const { alterarCor = false, data = {}, activeTab = "1", dataAgrupar = {} } = this.state;  

        return(
            <Modal
                width={700}
                style={{ top: '20px' }}
                visible={this.props.visible}
                onCancel={() => this.props.handleCancel()}
                title = {activeTab === "1" ? "Editar grafico carteira atual" : "Agrupar ativos da carteira atual"}
                footer={[
                    <Button key="back" onClick={() => this.props.handleCancel()}>
                    {'Fechar'}
                    </Button>,
                     <Button
                        disabled={!alterarCor}
                        key="submit"
                        type="primary"
                        onClick={() => this.onOk()}
                        >
                        {'Aplicar'}
                    </Button>,
                ]}
            >
                <Row gutter={16}>
                    <Col md={10} sm={24}>
                        <Tabs
                            type="card"
                            defaultActiveKey="1"
                            onChange={element => this.setState({ activeTab: element})}
                        >
                            <TabPane tab="Alterar cores" key="1">
                                {this.alterColor()}
                            </TabPane>
                            <TabPane tab="Agrupar ativos" key="2">
                                {this.estruturaAgruparAtivos()}
                            </TabPane>
                        </Tabs>
                    </Col>   
                    <Col md={14} sm={24}> 
                        { activeTab === "1" ? this.handleGrafico(data,"Avulso") : dataAgrupar.datasets ? this.handleGrafico(dataAgrupar,"Group") : this.handleGrafico(data,"Avulso") }  
                    </Col>                
                </Row>
            </Modal>
        )
    }
}