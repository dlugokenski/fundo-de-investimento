import React from 'react';
import { Pie } from 'react-chartjs-2';
import { Button } from 'antd';
import { isMobile } from 'react-device-detect';

import Title from 'Componente/Title';
import ModalEditePie from './ModalEditePie';
import { coresTiposAtivos } from 'Util/Cores';
import StyledChartPie from 'Pages/charts/StyledChartPie';

export default class PieComposicaoCarteira extends React.Component {
  state = {};

  componentDidMount() {
    this.iniciarVariaveisChart();
  }

  componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.iniciarVariaveisChart();
    }
  }

  iniciarVariaveisChart() {
    const { data = {}, title = '' } = this.props;

    const ativos = [];
    const valores = [];
    const coresCharts = [];
    const ativosProps = [];

    for (let i = 0; i < data.composicaoAtivos.length; i++) {
      const dadosVez = data.composicaoAtivos[i];
      const participacao =
        (dadosVez.valorAtualInvestido * 100) / data.valorAtualInvestido;

      ativosProps.push(dadosVez);
      ativos.push(dadosVez.tipoOperacao.descricao);
      valores.push(participacao);
      coresCharts.push(coresTiposAtivos[dadosVez.tipoOperacao.descricao].cor);
    }

    this.setState({ ativos, valores, coresCharts, title, ativosProps });
  }

  mudarCorDoGrafico = (alteracoes) => {
    const { coresCharts } = this.state;

    alteracoes.forEach((item) => {
      coresCharts[item.ativo] = item.cor;
    });
    this.setState({ coresCharts });
  };

  agruparAtivos = (grupoAtivos) => {};

  render() {
    const {
      ativos = [],
      valores = [],
      coresCharts = [],
      renderModalEditePie = false,
      title = '',
      ativosProps = [],
    } = this.state;
    const { renderModeCarossel } = this.props;

    const dataChart = {
      labels: ativos,
      datasets: [
        {
          data: valores,
          backgroundColor: coresCharts,
          hoverBackgroundColor: coresCharts,
        },
      ],
    };

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      legend: {
        onClick: null,
      },
      elements: {
        arc: {
          borderWidth: 0,
        },
      },
      tooltips: {
        callbacks: {
          label: function(tooltipItem, dataLabel) {
            const dataset = dataLabel.datasets[tooltipItem.datasetIndex];
            const total = dataset.data.reduce(function(
              previousValue,
              currentValue,
              currentIndex,
              array
            ) {
              return previousValue + currentValue;
            });
            const currentValue = dataset.data[tooltipItem.index];
            const percentage = parseFloat(
              ((currentValue / total) * 100).toFixed(2)
            );
            const label = dataLabel.labels[tooltipItem.index];

            const ativo = ativosProps.find(
              (element) => element.tipoOperacao.descricao === label
            );
            return `${label}: ${percentage}% | ${ativo.valorAtualInvestido.toLocaleString(
              'pt-BR',
              { style: 'currency', currency: 'BRL' }
            )}`;
          },
        },
      },
    };

    return (
      <React.Fragment>
        <ModalEditePie
          onChangeColor={this.mudarCorDoGrafico}
          onChangeAgruparAtivos={this.agruparAtivos}
          visible={renderModalEditePie}
          handleCancel={() => this.setState({ renderModalEditePie: false })}
          data={dataChart}
        />
        {renderModeCarossel ? (
          <React.Fragment>
            <Title title={title} />
            <Button
              type="primary"
              icon="edit"
              onClick={() => this.setState({ renderModalEditePie: true })}
              style={{
                float: 'right',
                marginTop: '-22px',
                position: 'relative',
                marginBotton: '20px',
              }}
              shape="circle"
            />
            <StyledChartPie>
              <Pie
                redraw={true}
                data={dataChart}
                height={300}
                width={500}
                options={options}
              />
            </StyledChartPie>
          </React.Fragment>
        ) : (
          <StyledChartPie>
            <Pie
              redraw={true}
              data={dataChart}
              height={300}
              width={500}
              options={options}
            />
          </StyledChartPie>
        )}
      </React.Fragment>
    );
  }
}
