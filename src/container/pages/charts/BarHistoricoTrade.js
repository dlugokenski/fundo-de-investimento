import React from 'react';
import { Bar } from 'react-chartjs-2';
import {isMobile} from 'react-device-detect';

import StyledChartBar from 'Pages/charts/StyledChartBar';

export default class GraficoBar extends React.Component {

  state={};

  componentDidMount(){
    this.iniciarVariaveisChart();
  }

  iniciarVariaveisChart(){
    const { data = [] } = this.props;
    const ativos = [];
    const resultadoTrade = [];
    let dinheioAcumulado = 0;
    const dinheiroAcumuladoArray = [];

    for (let i = 0; i < data.length; i++ ) {     
      if(!data[i].aberto){
        ativos.push(data[i].ticker.descricao);
        dinheioAcumulado = parseFloat(dinheioAcumulado) + parseFloat(data[i].resultadoFinanceiro);
        dinheiroAcumuladoArray.push(dinheioAcumulado);
        resultadoTrade.push(data[i].resultadoFinanceiro);
      }
    }
    
    this.setState({ativos, resultadoTrade, dinheiroAcumuladoArray });
  }

  render() {     
    const { ativos = [], resultadoTrade = [], dinheiroAcumuladoArray = [] } = this.state;
    const { renderModeCarossel } = this.props;

    const options = {
      responsive: true,
      maintainAspectRatio: isMobile ? false : true,
      tooltips: {
        callbacks: {
            label: function(tooltipItem, data) {
                const dataset = data.datasets[tooltipItem.datasetIndex];
                const currentValue = dataset.data[tooltipItem.index]; 
                const valorConvertido = currentValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                return `${dataset.label}: ${valorConvertido}`;
            },
        }
      },
      scales: {
        xAxes: [{
          maxBarThickness: 50,
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          gridLines: {
            color: "black",
            borderDash: [2, 5],
          },
          scaleLabel: {
            display: true,
            labelString: "R$",
            fontColor: "green"
          }
        }]
      }
    }

    const data = {
      labels: ativos,
      datasets: [
        {
          label: "Patrimônio acumulado",
          type: "line",
          borderColor: "rgba(94, 169, 255)",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: dinheiroAcumuladoArray
        }, 
        {
          label: "Resultado trade",
          type: "bar",
          barPercentage: '1',
          categoryPercentage: '1',
          backgroundColor: 'rgb(255, 123, 8)',
          borderColor: 'rgb(29, 41, 57)',
          borderWidth: '1',
          borderSkipped: 'bottom',
          hoverBackgroundColor: 'rgb(224, 108, 7)',
          hoverBorderColor: 'rgb(29, 41, 57)',
          hoverBorderWidth: '3',
          data: resultadoTrade
        }
      ]
  };
    
    return (
      <div className="chart-bar">
        <StyledChartBar {...this.props}>
          <Bar
            data={data} 
            height={500}
            width={700}
            options={options} 
          />
        </StyledChartBar>
      </div>
    );
  }
}