import React from "react";
import { notification, Modal } from "antd";

import Drawer from "Componente/Drawer";
import CadastrarAtivo from "Pages/ativo/cadastrar";
import { seachAtivo, apagarAtivo } from "Provider/Ativo";

import { connect } from "react-redux";

const { confirm } = Modal;

class AtivoComponente extends React.Component {
  state = {
    visible: true,
    close: false,
    pageAtiva: "cadastrar",
    ativo: {},
  };

  componentDidUpdate(newProps) {
    if (newProps.reset != this.props.reset) {
      this.setState({ visible: true });
    }
  }

  getTitlePage() {
    const { pageAtiva } = this.state;

    if (pageAtiva === "cadastrar") {
      return "Cadastrar ativo";
    } else if (pageAtiva === "editar") {
      return "Editar ativo";
    }
  }

  buscarAtivo = async (value) => {
    this.setState({ loading: true });
    const data = await seachAtivo(value);
    if (data) {
      if (data.id) {
        this.setState({ ativo: data });
        return true;
      } else if (data.message) {
        this.openNotification("error", "Alerta", data.message);
        this.setState({ ativo: {} });
        return false;
      }
    } else {
      this.openNotification(
        "error",
        "Alerta",
        "Ocorreu um erro ao pesquisar o ativo!"
      );
      this.setState({ ativo: {} });
      return false;
    }
  };

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  showConfirm = (removerAtivo) => {
    const { ativo } = this.state;
    confirm({
      title: `Realmente deseja apagar o ativo ${ativo.nome}?`,
      onOk() {
        return removerAtivo();
      },
      onCancel() {},
    });
  };

  removerAtivo = () => {
    const { ativo } = this.state;
    apagarAtivo(ativo);
    this.setState({ ativo: {} });
    this.openNotification("success", "Sucesso", `Ativo apagado com sucesso.`);
  };

  setarAtivo = (ativo) => {
    this.setState({ ativo });
  };

  retirarAtivo = () => {
    this.setState({ ativo: {} });
  };

  render() {
    const { visible, close, pageAtiva, ativo = {} } = this.state;

    const buttons = [
      {
        icon: "plus",
        label: "Cadastrar",
        onClick: () => this.setState({ pageAtiva: "cadastrar", ativo: {} }),
        disabled: pageAtiva === "cadastrar" ? true : false,
      },
      {
        icon: "search",
        label: "Pesquisar",
        onClick: () => this.setState({ pageAtiva: "editar", ativo: {} }),
        disabled: pageAtiva === "cadastrar" ? false : true,
      },
      {
        icon: "delete",
        label: "Deletar",
        onClick: () => this.showConfirm(this.removerAtivo),
        disabled: ativo && Object.keys(ativo).length !== 0 ? false : true,
      },
    ];

    return (
      <React.Fragment>
        <Drawer
          buttons={buttons}
          title={this.getTitlePage()}
          visible={visible}
          width={350}
          close={close}
          onVisible={(o) => this.setState({ visible: o })}
          onClose={(o) => this.setState({ close: o })}
        >
          {(pageAtiva === "cadastrar" || pageAtiva === "editar") && (
            <CadastrarAtivo
              pageAtiva={pageAtiva}
              buscarAtivo={(event) => this.buscarAtivo(event)}
              ativo={ativo}
              setarAtivo={this.setarAtivo}
              retirarAtivo={this.retirarAtivo}
            />
          )}
        </Drawer>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
});

export default connect(mapStateToProps, null)(AtivoComponente);
