import React from "react";
import { Form, Input, Switch, Button, Tooltip, Icon, Spin } from "antd";
import styled from "styled-components";

import { TIPO_ATIVOS } from "Util/Constantes";

const FormItem = Form.Item;

const StyledButtons = styled.div`
  display: flex;
  justify-content: flex-end;
  flex-direction: row;
  margin-bottom: 5px;
  margin-top: 5px;

  button {
    margin-right: 5px;
  }
`;

const StyledContent = styled.div`
  border: 1px solid #e3e3e3;
  padding: 5px;
  border-radius: 5px;
`;

const StyledSwitch = styled(Switch)`
  min-width: 70px !important;
  background-color: ${(props) =>
    props.checked ? "#099e32" : "#f02b2b"} !important;
`;

const StyledFormItem = styled(FormItem)`
  margin-left: 8px;

  .ant-form-item-control-wrapper {
    width: 100%;
    margin-top: -8px;
    margin-right: 5px;
  }

  .ant-form-item-label {
    display: contents;
  }
`;

const StyledHeader = styled.div`
  text-align: center;
  border-bottom: 1px solid #e3e3e3;
  margin-left: -5px;
  margin-right: -5px;
  color: rgba(0, 0, 0, 0.85);
  margin-top: -5px;
  margin-bottom: 5px;
  background-color: #f0f2f5;
  border-radius: 5px 5px 0px 0px;
`;

const StyledIcon = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  margin-right: 5px;
  margin-top: -5px;
  font-size: 10px;
`;

const StyleFormInputs = styled.div`
  display: flex;
  margin-bottom: ${(props) =>
    props.error_descricao === "sim" ? "0px" : "-20px"};
`;

class CadastroTickerComponente extends React.Component {
  state = {
    listar: false,
    edicao: false,
    loadingDiv: false,
  };

  componentDidMount() {
    const { tickerToEdit, error_descricao } = this.props;
    this.setState({
      switchChecked:
        tickerToEdit && tickerToEdit.ticker_ativo != null
          ? tickerToEdit.ticker_ativo
          : true,
      error_descricao,
    });
  }

  render() {
    const {
      form: { getFieldDecorator },
      tickerToEdit,
      tipoAtivoSelected,
    } = this.props;
    const {
      edicao,
      switchChecked = true,
      loadingDiv,
      error_descricao,
    } = this.state;
    const message = "Campo obrigatorio";

    return (
      <Spin spinning={loadingDiv}>
        <StyledContent>
          <StyledIcon>
            <Tooltip placement="top" title="Fechar">
              <Icon
                type="close"
                fill="default"
                onClick={() => this.props.onCancelCadastroTicker()}
              />
            </Tooltip>
          </StyledIcon>
          <StyledHeader>
            {edicao ? "Edição de ticker" : "Novo ticker"}
          </StyledHeader>
          <StyleFormInputs error_descricao={error_descricao}>
            <StyledFormItem label="Status:">
              {
                (getFieldDecorator("status_ticker", {
                  initialValue: switchChecked,
                }),
                (
                  <StyledSwitch
                    checked={switchChecked}
                    checkedChildren="Ativo"
                    unCheckedChildren="Inativo"
                    onChange={(e) => this.setState({ switchChecked: e })}
                  />
                ))
              }
            </StyledFormItem>
            <StyledFormItem label="Descrição:">
              {getFieldDecorator("descricao_ticker", {
                initialValue: tickerToEdit ? tickerToEdit.descricao : "",
                rules: [
                  {
                    required: true,
                    message: "Campo obrigatorio",
                    validator: (rule, value, callback) => {
                      let valueTrim = value.trim();
                      if (
                        valueTrim.length >= 5 &&
                        this.props.validadeTickerDuplicado(valueTrim) >= 1
                      ) {
                        rule.message = "Ticker duplicado";
                        this.setState({ error_descricao: "sim" });
                        callback("Ticker duplicado");
                      } else if (valueTrim.length == 0) {
                        this.setState({ error_descricao: "sim" });
                        rule.message = message;
                        callback(message);
                      } else {
                        this.setState({ error_descricao: "nao" });
                        rule.message = message;
                        callback(undefined);
                      }
                    },
                  },
                ],
                normalize: (value) => {
                  if (value) {
                    if (
                      tipoAtivoSelected === TIPO_ATIVOS.ACOES ||
                      tipoAtivoSelected === TIPO_ATIVOS.FII
                    ) {
                      return value
                        .toString()
                        .replace(/ /g, "")
                        .toUpperCase();
                    } else {
                      return value;
                    }
                  } else {
                    return "";
                  }
                },
              })(<Input />)}
            </StyledFormItem>
          </StyleFormInputs>
          <StyledButtons>
            <Button
              size={"small"}
              type="default"
              onClick={() => this.props.onCancelCadastroTicker()}
            >
              {"Cancelar"}
            </Button>
            <Button
              size={"small"}
              type="primary"
              onClick={() => this.props.onOkCadastroTicker()}
            >
              {"Confirmar"}
            </Button>
          </StyledButtons>
        </StyledContent>
      </Spin>
    );
  }
}

export default CadastroTickerComponente;
