import React from "react";
import {
  Form,
  Input,
  Select,
  Button,
  notification,
  AutoComplete,
  Icon,
  Spin,
  Tag,
  Tooltip,
} from "antd";
import styled from "styled-components";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as actions } from "Ducks/carteira";

import { seachAtivo } from "Provider/Ativo";
import SelectCountry from "Componente/SelectCountry";
import {
  buscarTiposAtivos,
  buscarSegmentosAtivos,
  cadastrarAtivoAcao,
  cadastrarAtivoFundoImobiliario,
  cadastrarTesouroDireto,
  getAtivoApi,
  consultarAllCountryWithListing,
} from "Provider/Ativo";
import { InputNumberForm } from "Componente/InputNumber";
import CadastroTickerComponente from "Pages/ativo/CadastroTicker";
import { TIPO_ATIVOS } from "Util/Constantes";

const FormItem = Form.Item;
const Option = Select.Option;
const { Search } = Input;
const OptionAutoComplete = AutoComplete.Option;

const StyledFormItem = styled(FormItem)`
  .ant-form-item-children {
    display: block;
  }
`;

const StyledFormItemError = styled.div`
  color: #f52934;
`;

const StyledConteudoTicker = styled.div`
  border: 1px solid ${(props) => (props.hasError ? "#f75961" : "#d9d9d9")};
  border-radius: 5px;
  display: flex;
  flex-wrap: wrap;
`;

const StyleTag = styled(Tag)`
  color: white;
`;

class CadastrarAtivoComponente extends React.Component {
  state = {
    tipoAtivo: [],
    optionsSegmento: [],
    visibleInputSegmento: false,
    cadastrarTicker: false,
    listTicker: [],
    validingForm: false,
    listRemoveTicker: [],
    tickerToEdit: undefined,
    error_duplicado: "",
  };

  componentDidMount() {
    this.buscarTiposAtivos();
    this.buscarSegmentosAtivosMethod();
    this.setSelectedAtivo();
    this.consultarAllCountryWithListing();
  }

  componentDidUpdate(nextProps) {
    if (nextProps !== this.props) {
      if (nextProps.pageAtiva != this.props.pageAtiva) {
        this.resetForm();
      }
      if (nextProps.ativo != this.props.ativo) {
        if (this.props.ativo && this.props.ativo.id) {
          this.setValuesForm();
        } else {
          this.resetForm();
        }
      }
    }
  }

  resetForm() {
    const {
      form: { resetFields },
    } = this.props;
    resetFields();
    this.setSelectedAtivo();
    this.setState({
      ativoEdit: {},
      cadastrarTicker: false,
      listTicker: [],
      listTickerDataBase: [],
      validingForm: false,
      listRemoveTicker: [],
      tickerToEdit: undefined,
      error_duplicado: "",
    });
  }

  async setValuesForm() {
    const {
      form: { setFieldsValue },
      ativo = {},
    } = this.props;

    const listTickerDataBase = this.ordernarTickers(
      ativo.listaDeTicker.map((e) => ({
        id: e.id,
        descricao: e.descricao,
        ticker_ativo: e.ticker_ativo,
      }))
    );

    this.setState({
      listTickerDataBase,
      listTicker: listTickerDataBase,
    });

    setFieldsValue({
      empresa: this.getNomeAtivo(ativo),
      tipo: ativo.tipo.descricao,
      ativo: ativo.ativo ? 1 : 0,
      nome_pregao: ativo.nomePregao,
      cnpj: ativo.cnpj,
    });

    await this.montedOptionsSegmento(ativo.tipo.descricao);
    setFieldsValue({
      segmento: ativo.segmento && ativo.segmento.id,
    });
  }

  setDefaultValues() {
    const {
      form: { setFieldsValue },
    } = this.props;
    setFieldsValue({
      empresa: undefined,
      tipo: undefined,
      ativo: undefined,
      segmento: undefined,
      nome_pregao: undefined,
      cnpj: undefined,
    });
  }

  async buscarTiposAtivos() {
    const tipoAtivo = await buscarTiposAtivos();
    const {
      form: { setFieldsValue },
    } = this.props;
    this.setState({ tipoAtivo });
    setFieldsValue({
      tipo: TIPO_ATIVOS.ACOES,
    });
  }

  setSelectedAtivo() {
    const {
      form: { setFieldsValue },
    } = this.props;
    setFieldsValue({
      ativo: 1,
    });
  }

  async consultarAllCountryWithListing() {
    const countryWithListing = await consultarAllCountryWithListing();
    this.setState({ countryWithListing });
  }

  async buscarSegmentosAtivosMethod() {
    const segmentos = await buscarSegmentosAtivos();
    this.setState({ segmentos });
    this.montedOptionsSegmento();
  }

  montedOptionsTipo = () => {
    const { tipoAtivo } = this.state;

    if (tipoAtivo) {
      return tipoAtivo.map((element, index) => (
        <Option key={index} value={element.descricao}>
          {element.descricao}
        </Option>
      ));
    }
  };

  montedOptionsSegmento = (ativo) => {
    const { segmentos } = this.state;
    const {
      form: { getFieldsValue },
    } = this.props;
    let listReturn = [];
    const tipoAtivo = ativo ? ativo : getFieldsValue().tipo;

    if (segmentos) {
      if (tipoAtivo) {
        segmentos.forEach((element, index) => {
          if (element.tipoAtivo && element.tipoAtivo.descricao === tipoAtivo) {
            listReturn.push(
              <Option key={index} value={element.id}>
                {element.descricao}
              </Option>
            );
          }
        });
      } else {
        listReturn = segmentos.map((element, index) => (
          <Option key={index} value={element.id}>
            {element.descricao}
          </Option>
        ));
      }
      this.setState({
        optionsSegmento: listReturn,
        visibleInputSegmento: listReturn.length > 0,
      });
    }
  };

  changeTipoAtivo(tipoAtivo) {
    const {
      form: { setFieldsValue },
    } = this.props;
    this.montedOptionsSegmento(tipoAtivo);
    this.setState({
      tipoAtivoSelected: tipoAtivo,
    });

    setFieldsValue({
      segmento: undefined,
    });
  }

  async getAtivoApi() {
    const {
      form: { getFieldValue, setFieldsValue },
    } = this.props;
    const tipoAtivo = getFieldValue("tipo");
    let nomeAtivo = getFieldValue("ticker");
    if (
      (tipoAtivo === TIPO_ATIVOS.ACOES || tipoAtivo === TIPO_ATIVOS.FII) &&
      nomeAtivo
    ) {
      this.setState({
        loadingDiv: true,
      });
      nomeAtivo = nomeAtivo.replace(/[0-9]/g, "");
      const ativo = await getAtivoApi(tipoAtivo, nomeAtivo);
      if (ativo && ativo.nomePregao) {
        setFieldsValue({
          cnpj: ativo.cnpj,
          nome_pregao: ativo.nomePregao,
          empresa: ativo.nome,
        });
      }
      this.setState({
        loadingDiv: false,
      });
    }
  }

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  ordernarTickers = (list) => {
    return list.sort((a, b) => {
      if (a.descricao.length > b.descricao.length) {
        return 0;
      }
      return a.descricao < b.descricao ? -1 : 0;
    });
  };

  onOkCadastroTicker = () => {
    const {
      form: { validateFields },
    } = this.props;
    const {
      listTicker,
      listRemoveTicker,
      tickerToEdit,
      listTickerDataBase = [],
    } = this.state;

    validateFields(
      ["status_ticker", "descricao_ticker"],
      async (err, values) => {
        if (!err) {
          const validateTicker = this.validadeTickerDuplicado(
            values.descricao_ticker
          );
          if (validateTicker == -1 || validateTicker == 0) {
            const checkTickerInListDelete = listRemoveTicker.indexOf(
              listRemoveTicker.find(
                (e) => e.descricao == values.descricao_ticker
              )
            );
            if (checkTickerInListDelete != -1) {
              listRemoveTicker.splice(checkTickerInListDelete, 1);
            }

            if (tickerToEdit) {
              listTicker.splice(
                listTicker.indexOf(
                  listTicker.find((e) => e.descricao == values.descricao_ticker)
                ),
                1
              );
            }

            const checkIdinListTicker = listTickerDataBase.find(
              (e) => e.descricao == values.descricao_ticker
            );

            let id = 0;
            if (tickerToEdit && tickerToEdit.id) {
              id = tickerToEdit.id;
            } else if (checkIdinListTicker) {
              id = checkIdinListTicker.id;
            }

            this.setState({
              listTicker: this.ordernarTickers([
                ...listTicker,
                {
                  id,
                  descricao: values.descricao_ticker,
                  ticker_ativo: values.status_ticker,
                },
              ]),
              cadastrarTicker: false,
              error_descricao: "nao",
              error_duplicado: false,
            });
          }
        } else {
          this.setState({
            error_descricao: "sim",
            error_duplicado: false,
          });
        }
      }
    );
  };

  validadeTickerDuplicado = (descricao) => {
    const { listTicker, tickerToEdit } = this.state;

    if (tickerToEdit) {
      this.setState({
        error_descricao: "nao",
        error_duplicado: false,
      });
      return -1;
    } else {
      let checkTickerExistente = listTicker.filter(
        (e) => e.descricao == descricao
      );
      if (checkTickerExistente.length == 1) {
        this.setState({
          error_descricao: "sim",
          error_duplicado: true,
        });
      } else {
        this.setState({
          error_descricao: "nao",
          error_duplicado: false,
        });
      }
      return checkTickerExistente.length;
    }
  };

  handleSubmit = () => {
    const {
      form: { validateFields },
      pageAtiva,
      ativo = {},
    } = this.props;
    const { listTicker, listRemoveTicker, tipoAtivo } = this.state;

    this.setState({
      loading: true,
      cadastrarTicker: false,
      validingForm: true,
    });
    validateFields(async (err, values) => {
      if (!err || listTicker.length != 0) {
        const nome = values.empresa;
        const tipo = tipoAtivo.find((e) => values.tipo === e.descricao);
        let ativoFomr = values.ativo;
        const segmento = values.segmento;
        const razaoSocial = values.razao_social;
        const codigoCvm = values.codigo_cvm;
        const nomePregao = values.nome_pregao;
        const cnpj = values.cnpj;
        const country = values.country;

        let ativoData = {
          id: ativo && ativo.id ? ativo.id : 0,
          listaDeTicker: listTicker,
          ativo: ativoFomr,
          tipo,
          segmento: {
            id: segmento,
          },
          countryWithListing: {
            id: country,
          },
        };

        let data = {
          ativo: ativoData,
          tickerRemove: listRemoveTicker,
        };

        try {
          if (tipo.descricao === TIPO_ATIVOS.ACOES) {
            data.ativo.nome = nome;
            data.ativo.razaoSocial = razaoSocial ? razaoSocial.trim() : "";
            data.ativo.codigoCvm = Number(codigoCvm);
            data.ativo.nomePregao = nomePregao ? nomePregao.trim() : "";
            data.ativo.cnpj = cnpj;
            data = await cadastrarAtivoAcao(data);
          } else if (tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO) {
            data = await cadastrarTesouroDireto(data);
          } else if (tipo.descricao === TIPO_ATIVOS.FII) {
            data.ativo.nome = nome;
            data.ativo.nomePregao = nomePregao ? nomePregao.trim() : "";
            data.ativo.cnpj = cnpj;
            data = await cadastrarAtivoFundoImobiliario(data);
          }

          if (data) {
            if (data.id) {
              this.openNotification(
                "success",
                "Sucesso",
                pageAtiva === "editar"
                  ? "Ativo editado com sucesso."
                  : `Ativo cadastrado com sucesso.`
              );
              this.resetForm();
              this.buscarTiposAtivos();
            }
          }
        } catch (error) {
          this.openNotification(
            "error",
            "Erro",
            error.message
              ? error.message
              : "Ocorreu um erro ao cadastrar o ativo!"
          );
        }
      } else {
        this.openNotification(
          "warning",
          "Alerta",
          "Existe erros no formulário!"
        );
      }
    });
    this.setState({ loading: false });
  };

  async buscarAtivo(value) {
    this.setDefaultValues();
    this.props.retirarAtivo();
    this.setState({ loading: true, listAtivosAutoComplete: [] });
    const listAtivos = await seachAtivo(value);
    this.setState({ loading: false, listAtivosAutoComplete: listAtivos });
  }

  checkLabelTipoAtivo() {
    const {
      form: { getFieldValue },
    } = this.props;
    let label = "";
    const idTipoAtivo = getFieldValue("tipo");
    if (idTipoAtivo === TIPO_ATIVOS.ACOES) {
      label = "Nome empresa";
    } else if (idTipoAtivo === TIPO_ATIVOS.TESOURO_DIRETO) {
      label = "Nome Titulo";
    } else if (idTipoAtivo === TIPO_ATIVOS.FII) {
      label = "Nome do fundo";
    }
    return label;
  }

  getNomeAtivo(object) {
    if (object && object.tipo) {
      if (object.tipo.descricao === TIPO_ATIVOS.ACOES) {
        return object.nomePregao;
      } else if (object.tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO) {
        return object.ticker;
      } else if (object.tipo.descricao === TIPO_ATIVOS.FII) {
        return object.nome;
      }
    } else {
      return "";
    }
  }

  selectAtivo = (value) => {
    const { listAtivosAutoComplete = [] } = this.state;
    const object = listAtivosAutoComplete.find(
      (element) => element.id == value
    );
    this.setState({ tipoAtivoSelected: object.tipo.descricao });
    this.props.setarAtivo(object);
  };

  renderTextOptionAtivos(element) {
    if (
      element.tipo.descricao === TIPO_ATIVOS.ACOES ||
      element.tipo.descricao === TIPO_ATIVOS.FII
    ) {
      return element.nomePregao;
    } else if (element.tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO) {
      return element.listaDeTicker[0].descricao;
    }
  }

  renderOptionAtivos = () => {
    const { listAtivosAutoComplete = [] } = this.state;
    return listAtivosAutoComplete.map((element, index) => (
      <OptionAutoComplete key={element.id}>
        {this.renderTextOptionAtivos(element)}
      </OptionAutoComplete>
    ));
  };

  removeTicker = (index) => {
    const { listTicker, listRemoveTicker } = this.state;

    const ticker = listTicker[index];
    if (ticker.id != 0) {
      listRemoveTicker.push(ticker);
    }

    listTicker.splice(index, 1);
    this.setState({ listTicker });
  };

  renderTickers = () => {
    const { listTicker, validingForm } = this.state;
    const error = [];
    const result = listTicker.map((tag, index) => (
      <StyleTag
        onClick={() =>
          this.setState({ tickerToEdit: tag, cadastrarTicker: true })
        }
        key={index}
        style={{
          background: tag.ticker_ativo ? "#099e32" : "#f02b2b",
          color: "white",
          margin: "2px",
        }}
      >
        {tag.descricao}
        <Tooltip key={`b${index}`} placement="top" title="Remover ticker">
          <Icon
            key={`a${index}`}
            type="close"
            onClick={(e) => {
              e.stopPropagation();
              this.removeTicker(index);
            }}
            style={{ color: "white" }}
          />
        </Tooltip>
      </StyleTag>
    ));

    result.push(
      <Tag
        key={listTicker.length}
        onClick={() =>
          this.setState({ cadastrarTicker: true, tickerToEdit: undefined })
        }
        style={{ background: "#f5f5f5", borderStyle: "dashed", margin: "2px" }}
      >
        <Icon type="plus" /> {"New"}
      </Tag>
    );

    if (validingForm && listTicker.length == 0) {
      error.push(
        <StyledFormItemError key={1} className="ant-form-explain">
          Campo obrigatorio
        </StyledFormItemError>
      );
    }
    return (
      <div>
        <StyledConteudoTicker hasError={error.length === 1}>
          {result}
        </StyledConteudoTicker>
        {error}
      </div>
    );
  };

  checkDisabledForm() {
    const { pageAtiva, ativo = {} } = this.props;
    return pageAtiva === "editar" && Object.keys(ativo).length === 0;
  }

  render() {
    const {
      loading = false,
      loadingDiv = false,
      optionsSegmento = [],
      visibleInputSegmento,
      tipoAtivoSelected = TIPO_ATIVOS.ACOES,
      error_descricao = "nao",
      error_duplicado = false,
      cadastrarTicker,
      tickerToEdit,
      countryWithListing = [],
    } = this.state;

    const {
      form: { getFieldValue, getFieldDecorator },
      ativo = {},
      pageAtiva,
    } = this.props;
    const message = "Campo obrigatorio";

    return (
      <React.Fragment>
        <Form>
          <div className="content-form">
            <Spin spinning={loadingDiv}>
              {pageAtiva === "editar" && (
                <AutoComplete
                  style={{ width: "100%" }}
                  dataSource={this.renderOptionAtivos()}
                  onSelect={(value) => this.selectAtivo(value)}
                  onSearch={(value) => this.buscarAtivo(value)}
                >
                  <Search
                    placeholder="Nome empresa/ticker"
                    enterButton
                    onSearch={(value) => this.buscarAtivo(value)}
                  />
                </AutoComplete>
              )}
              <FormItem label="Tipo operação:">
                {getFieldDecorator("tipo", {
                  rules: [{ required: true, message }],
                })(
                  <Select
                    disabled={this.checkDisabledForm()}
                    placeholder="Selecione o tipo de operação"
                    defaultActiveFirstOption={true}
                    onChange={(e) => this.changeTipoAtivo(e)}
                  >
                    {this.montedOptionsTipo()}
                  </Select>
                )}
              </FormItem>
              {tipoAtivoSelected !== TIPO_ATIVOS.TESOURO_DIRETO && (
                <FormItem label={this.checkLabelTipoAtivo() || "Nome"}>
                  {getFieldDecorator("empresa", {
                    rules: [{ required: true, message }],
                  })(
                    <Input
                      disabled={this.checkDisabledForm()}
                      autoComplete="off"
                      suffix={
                        pageAtiva === "cadastrar" &&
                        getFieldValue("tipo") === TIPO_ATIVOS.ACOES && (
                          <Button
                            disabled={
                              (getFieldValue("tipo") != 1 ||
                                getFieldValue("tipo") != 3) &&
                              !getFieldValue("ticker")
                            }
                            onClick={() => this.getAtivoApi()}
                            style={{ marginRight: -12, marginLeft: "17px" }}
                            type="primary"
                          >
                            <Icon type="search" />
                          </Button>
                        )
                      }
                    />
                  )}
                </FormItem>
              )}
              <StyledFormItem label="Ticker:">
                {cadastrarTicker ? (
                  <React.Fragment>
                    {getFieldDecorator("ticker", {
                      rules: [
                        {
                          required: true,
                          message,
                        },
                      ],
                    })(
                      <CadastroTickerComponente
                        tipoAtivoSelected={tipoAtivoSelected}
                        tickerToEdit={tickerToEdit}
                        onCancelCadastroTicker={() =>
                          this.setState({
                            cadastrarTicker: false,
                            error_descricao: "nao",
                            error_duplicado: false,
                          })
                        }
                        error_duplicado={error_duplicado}
                        error_descricao={error_descricao}
                        onOkCadastroTicker={this.onOkCadastroTicker}
                        form={this.props.form}
                        validadeTickerDuplicado={this.validadeTickerDuplicado}
                      />
                    )}
                  </React.Fragment>
                ) : (
                  this.renderTickers()
                )}
              </StyledFormItem>
              {visibleInputSegmento && (
                <FormItem label="Segmento:">
                  {getFieldDecorator("segmento", {
                    rules: [{ required: true, message }],
                  })(
                    <Select
                      disabled={this.checkDisabledForm()}
                      placeholder="Selecione o segmento"
                      defaultActiveFirstOption={true}
                    >
                      {optionsSegmento}
                    </Select>
                  )}
                </FormItem>
              )}
              <FormItem label="Ativo:">
                {getFieldDecorator("ativo")(
                  <Select placeholder="Ativo" defaultActiveFirstOption={true}>
                    <Option key={1} value={1}>
                      Sim
                    </Option>
                    <Option key={2} value={0}>
                      Não
                    </Option>
                  </Select>
                )}
              </FormItem>
              <FormItem label="País onde esta listado:">
                <SelectCountry
                  message={message}
                  form={this.props.form}
                  ativo={ativo}
                  itens={countryWithListing.map((e) => ({
                    id: e.id,
                    description: e.country.nameCountry,
                    isoCountry: e.country.isoCountry,
                  }))}
                />
              </FormItem>
              {tipoAtivoSelected === TIPO_ATIVOS.ACOES && (
                <React.Fragment>
                  <FormItem label="Razão social:">
                    {getFieldDecorator("razao_social")(
                      <Input
                        disabled={this.checkDisabledForm()}
                        autoComplete="off"
                      />
                    )}
                  </FormItem>
                  <FormItem label="Codigo cvm:">
                    {getFieldDecorator("codigo_cvm", {
                      normalize: (value) => {
                        if (value) {
                          return value.toString().replace(/ /g, "");
                        } else {
                          return "";
                        }
                      },
                    })(
                      <InputNumberForm
                        ref={(input) => input}
                        autoComplete="off"
                        disabled={this.checkDisabledForm()}
                      />
                    )}
                  </FormItem>
                </React.Fragment>
              )}
              {(tipoAtivoSelected === TIPO_ATIVOS.FII ||
                tipoAtivoSelected === TIPO_ATIVOS.ACOES) && (
                <React.Fragment>
                  <FormItem label="CNPJ:">
                    {getFieldDecorator("cnpj", {
                      rules: [{ required: true, message }],
                    })(<Input autoComplete="off" />)}
                  </FormItem>
                  <FormItem label="Nome pregão:">
                    {getFieldDecorator("nome_pregao", {
                      rules: [{ required: true, message }],
                    })(<Input autoComplete="off" />)}
                  </FormItem>
                </React.Fragment>
              )}
            </Spin>
          </div>
          <FormItem>
            <Button
              className="button-submit"
              type="primary"
              loading={loading}
              htmlType="submit"
              onClick={() => this.handleSubmit()}
              disabled={this.checkDisabledForm()}
            >
              {ativo && Object.keys(ativo).length === 0
                ? "Cadastrar"
                : "Editar"}
            </Button>
          </FormItem>
        </Form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  carteira: state.carteira,
});
const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(Form.create()(CadastrarAtivoComponente));
