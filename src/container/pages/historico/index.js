import React from 'react';
import { Layout, Tabs} from 'antd';
import UpdateOrdens from 'Pages/historico/UpdateOrdens'

const {  Content } = Layout;
const { TabPane } = Tabs;

export default class Trade extends React.Component {
    
  state = {};
  render(){   

    return(
      <Content>
        <Tabs
            type="card"
            // onTabClick={e => this.setState({ activeTab: e })}
            // activeKey={activeTab}
            defaultActiveKey="1"
        >
          <TabPane tab="Historico" key="1">
            <UpdateOrdens />
          </TabPane>
          <TabPane tab="Updade de ordens" key="2">
            <UpdateOrdens />
          </TabPane>
        </Tabs>
      </Content>
    )
  }
}


  