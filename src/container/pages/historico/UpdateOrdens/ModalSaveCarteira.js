import React from 'react';
import { Modal, Button, Form, Input, Row, Col } from 'antd';

import { cadastrarNovaCarteira } from 'Provider/Carteira'

const FormItem = Form.Item;

class ModalSaveCarteira extends React.Component {

  salvarInformacoes = async (name) =>{
    const { usuario, carteira } = this.props;

    let newData = Object.assign({}, carteira);

    const listOperacao = newData.carteira.listaDeOperacao.map(e => {
      return {
        ...e,
        ativo: {
          id: e.ativo.id
        },
        ticker: {
          descricao: e.ticker.descricao
        }
      }
    })

    newData = {
      id: carteira.carteira.id ? carteira.carteira.id : 0,
      listaDeOperacao: listOperacao,
      usuario,
      name,
      favorita: false
    }

    await cadastrarNovaCarteira(newData);
    this.props.onOk()
  }
  
  handleSubmit = () => {
    const { validateFields } = this.props.form;
    validateFields(async (err, values) => {
      if (!err) {
        this.salvarInformacoes(values.name)
      }
    });
    this.setState({loading: false});
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <React.Fragment>
        <Modal
            style={{ top: '20px' }}
            visible={this.props.visible}
            onCancel={() => this.props.handleCancel()}
            title="Salvar informações"
            footer={[
                <Button key="back" onClick={() => this.props.handleCancel()}>
                {'Fechar'}
                </Button>,
                <Button
                    loading={this.props.loadingSalvandoInformacao}
                    key="submit"
                    type="primary"
                    onClick={() => this.handleSubmit()}
                    >
                    {this.props.loadingSalvandoInformacao ? 'Salvando os dados' : 'Confirmar'} 
                </Button>
            ]}
        > 
          <React.Fragment>
          <Form>
            <Row gutter={16}>
              <Col md={24} sm={24}>
                <FormItem label="Nome da carteira">
                    {getFieldDecorator('name', {
                    rules: [{ required: true, message: 'Campo obrigatorio' }],
                    })(<Input />)}
                </FormItem>
              </Col>
            </Row>
          </Form>
          <Row>
          {'Antes de confirmar o salvamento, verifique se as informações processadas corresponden com os dados planilha!'} 
          </Row>
            
          </React.Fragment>
          
        </Modal>
      </React.Fragment>
    )
  }
}
export default Form.create()(ModalSaveCarteira);
