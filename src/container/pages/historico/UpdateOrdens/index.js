import React from "react";
import {
  Button,
  Modal,
  Row,
  Upload,
  Tooltip,
  notification,
  Icon,
  Input,
} from "antd";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as actions } from "Ducks/carteira";

import { efetuarProcessamentoDeDados } from "Provider/Util";
import {
  atualizarEventos,
  atualizarDividendos,
  atualizarNoticias,
  atualizarCotacaoComGoogle,
  atualizarDividendosAtivo,
} from "Provider/Ativo";
import ModalSaveCarteira from "./ModalSaveCarteira";
import MainComponent from "Pages/main";
import initCarteiraController from "Pages/carteiraAtual/CarteiraAtualController";

// Render the nav arrows
function CustomArrow(props) {
  const { className, style, onClick, dir } = props;
  return (
    <button
      type="button"
      className={className}
      style={{ ...style }}
      onClick={onClick}
    >
      <Icon type={dir} />
    </button>
  );
}

// Default settings for Slick Slider
const settings = {
  dots: true,
  arrows: true,
  autoplay: true,
  centerMode: true,
  centerPadding: "0px",
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow: <CustomArrow dir="right" />,
  prevArrow: <CustomArrow dir="left" />,
};

class UpdateOrdens extends React.Component {
  state = {
    dadosProcessados: false,
    disabledUpdateCotacaoAcao: false,
    disabledUpdateNoticias: false,
    disabledUpdateDividendos: false,
  };

  atualizarNoticiasMethod() {
    atualizarNoticias();
    this.setState({
      disabledUpdateNoticias: true,
    });
  }

  atualizarCotacaoAcaoGoogleMethod() {
    atualizarCotacaoComGoogle();
    this.setState({
      disabledUpdateCotacaoAcao: true,
    });
  }

  atualizarEventosMethod() {
    atualizarEventos();
  }

  atualizarDividendosMethod() {
    atualizarDividendos();
    this.setState({
      disabledUpdateDividendos: true,
    });
  }

  disabledUpdateDividendos;

  checarArquivo = (file) => {
    var extArquivo = file.name.split(".").pop();
    if (extArquivo !== "xlsx") {
      this.openNotification(
        "error",
        "Erro",
        "Extensão do arquivo não permitido!"
      );
      return false;
    }
    return true;
  };

  info() {
    const { dataUltimAtualizacaoCotacao = "" } = this.state;
    Modal.info({
      title: "Alerta",
      content: `Ultima atualização das cotações foi em ${dataUltimAtualizacaoCotacao} PM`,
    });
  }

  onHandleUpload = async (file) => {
    const { idUsuario } = this.props;

    this.setState({
      loadingProcessandoDados: true,
      loadingTable: true,
      listaDeOperacao: undefined,
    });

    if (await this.checarArquivo(file)) {
      const formData = new FormData();
      formData.append("uploadedFile", file);
      try {
        const data = await efetuarProcessamentoDeDados(formData);
        initCarteiraController(data);
        this.setState({ dadosProcessados: true });
        this.openNotification(
          "success",
          "Sucesso",
          "Processamento da planilha realizado com sucesso!"
        );
      } catch (erro) {
        this.openNotification(
          "error",
          "Erro",
          erro.message
            ? erro.message
            : "Ocorreu um erro ao processar a planilha!"
        );
        this.setState({ data: undefined });
      }
    }
    this.setState({ loadingProcessandoDados: false, loadingTable: false });
    return true;
  };

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  salvarInformacoes = async () => {
    this.openNotification("success", "Sucesso", "Dados salvo com sucesso!");
    this.setState({ modalSalvar: false, loadingSalvandoInformacao: false });
  };

  renderModalSalvarInformacoes() {
    const {
      modalSalvar = false,
      loadingSalvandoInformacao = false,
    } = this.state;
    const { carteira } = this.props;
    if (modalSalvar) {
      return (
        <ModalSaveCarteira
          {...this.props}
          {...this.state}
          carteira={carteira}
          visible={modalSalvar}
          loadingSalvandoInformacao={loadingSalvandoInformacao}
          handleCancel={() => this.setState({ modalSalvar: false })}
          onOk={this.salvarInformacoes}
        />
      );
    }
    return null;
  }

  render() {
    const {
      loadingProcessandoDados = false,
      dadosProcessados = false,
      disabledUpdateNoticias = false,
      disabledUpdateCotacaoAcao = false,
      disabledUpdateDividendos = false,
      ativoDividendoAcao = undefined,
      ativoDividendoFii = undefined,
    } = this.state;

    const iconButton = loadingProcessandoDados ? "loading" : "upload";

    return (
      <React.Fragment>
        {this.renderModalSalvarInformacoes()}

        <Button onClick={() => this.atualizarEventosMethod()}>
          {" "}
          atualizar eventos{" "}
        </Button>
        <Button
          onClick={() => this.atualizarDividendosMethod()}
          disabled={disabledUpdateDividendos}
        >
          {" "}
          atualizar dividendos{" "}
        </Button>
        <Button
          onClick={() => this.atualizarNoticiasMethod()}
          disabled={disabledUpdateNoticias}
        >
          {" "}
          atualizar noticias{" "}
        </Button>
        <Button
          onClick={() => this.atualizarCotacaoAcaoGoogleMethod()}
          disabled={disabledUpdateCotacaoAcao}
        >
          {" "}
          atualizar cotacao google{" "}
        </Button>
        <br />
        <Input
          onChange={(e) =>
            this.setState({ ativoDividendoAcao: e.target.value })
          }
        />
        <Button onClick={() => atualizarDividendosAtivo(ativoDividendoAcao)}>
          {" "}
          atualizar dividendo do ativo acao{" "}
        </Button>

        <br />
        <Input
          onChange={(e) => this.setState({ ativoDividendoFii: e.target.value })}
        />
        <Button onClick={() => atualizarDividendosAtivo(ativoDividendoFii)}>
          {" "}
          atualizar dividendo do ativo fii{" "}
        </Button>

        <Row>
          <Upload
            customRequest={() => false}
            beforeUpload={this.onHandleUpload}
            showUploadList={false}
          >
            <Tooltip placement="bottom" title="Apenas arquivo excel (xlsx)">
              <Button
                type="primary"
                icon={iconButton}
                loading={loadingProcessandoDados}
              >
                {loadingProcessandoDados
                  ? "Processando os dados"
                  : "Fazer updade de ordens"}
              </Button>
            </Tooltip>
          </Upload>
          <Button type="primary" icon="download" style={{ marginLeft: "3px" }}>
            Planilha padrão
          </Button>
          <Button type="primary" style={{ marginLeft: "3px" }}>
            Ajuda
          </Button>
        </Row>
        {dadosProcessados && (
          <React.Fragment>
            <MainComponent />
            <center>
              <Button
                type="primary"
                icon="save"
                style={{ marginTop: "20px" }}
                onClick={() => this.setState({ modalSalvar: true })}
              >
                {"Salvar informações"}
              </Button>
            </center>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);
const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  carteira: state.carteira,
});

export default connect(mapStateToProps, mapDispatchToPros)(UpdateOrdens);
