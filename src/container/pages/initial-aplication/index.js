import React from 'react';
import styled, { keyframes } from 'styled-components';
import { Row, Icon } from 'antd';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import SpinCuston from 'Componente/Spin';
import {
  findAllCompanies,
  findAllFII,
  atualizarCotacaoComGoogle,
  consultarSeAplicacaoJaIniciou
} from "Provider/Ativo";

// Animação de piscar
const blink = keyframes`
  0% { opacity: 1; }
  50% { opacity: 0.5; }
  100% { opacity: 1; }
`;

// Animação de carregamento estilo Windows
const spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const Title = styled.div`
  text-align: left;
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 20px;
  color: #333;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  height: 100%;
  text-align: left;
  margin-left: 20px; /* Margem para a esquerda */
  font-size: 16px;
  font-weight: 500; /* Peso intermediário entre normal e bold */
`;

const ContentSteps = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 15px; /* Espaçamento entre os passos */
  margin-left: 10px;
`;

const ContentStep = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px; /* Espaçamento entre o spinner e o texto */
  font-size: 18px;
  color: #555;

  &.running {
    animation: ${blink} 2s infinite;
  }

  &.finished {
    color: green;
  }

  &.stop {
    color: #ccc;
  }
`;

const Spinner = styled.div`
  width: 20px;
  height: 20px;
  border: 4px solid #ccc;
  border-top: 4px solid #1890ff;
  border-radius: 50%;
  animation: ${spin} 1s linear infinite;
  margin-right: -2px;
`;

const StyledLayout = styled(Row)`
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
  background: linear-gradient(135deg, #e0e0e0, #f8f8f8);
`;

const SpaceBlank = styled.div`
  width: 20px;
  height: 20px;
`;

const options = {
  RUNING: 'RUNING',
  STOP: 'STOP',
  FINISHED: 'FINISHED'
}

const STEPS_DESCRIPTONS = {
  'FINDING_ALL_COMPANIES': 'FINDING_ALL_COMPANIES',
  'FINDING_ALL_FII': 'FINDING_ALL_FII',
  'FINDING_ALL_QUOTES': 'FINDING_ALL_QUOTES'
}
class InitialApplication extends React.Component {
  state = {
    title: 'Aguarde estamos inicializando a aplicação',
    stepsToInitApplication: {
      [STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES]: {
        status: options.STOP,
        description: 'Buscando empresas'
      },
      [STEPS_DESCRIPTONS.FINDING_ALL_FII]: {
        status: options.STOP,
        description: 'Buscando fundos imobiliários'
      },
      [STEPS_DESCRIPTONS.FINDING_ALL_QUOTES]: {
        status: options.STOP,
        description: 'Buscando cotações'
      },
    }
  };

  componentDidMount(){
    this.iniciarPagina();
    this.initWebSocket();
  }

  componentDidUpdate(nextProps){
    if(nextProps !== this.props ){
        if(nextProps.socket != this.props.socket){
          this.initWebSocket();
        }
    }         
  }

  async iniciarPagina(){
    const {hasCompanies, hasFII, hasQuotes} = await consultarSeAplicacaoJaIniciou();
    let iniciadoPagina = false;

    if(hasCompanies){
      this.setState({
        stepsToInitApplication: {
          ...this.state.stepsToInitApplication,
          [STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES]: {
            ...this.state.stepsToInitApplication.FINDING_ALL_COMPANIES,
            description: 'Buscado empresas',
            status: options.FINISHED,
          },
        }
      });
    } else {
      this.iniciarConsultasPagina();
      iniciadoPagina = true;
    }

    if(hasFII){
      this.setState({
        stepsToInitApplication: {
          ...this.state.stepsToInitApplication,
          [STEPS_DESCRIPTONS.FINDING_ALL_FII]: {
            ...this.state.stepsToInitApplication.FINDING_ALL_FII,
            description: 'Buscado fundos imobiliários',
            status: options.FINISHED,
          }
        }
      });
    } else if(!iniciadoPagina) {
      findAllFII();
      this.setState({
        stepsToInitApplication: {
          ...this.state.stepsToInitApplication,
          [STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES]: {
            ...this.state.stepsToInitApplication.FINDING_ALL_COMPANIES,
            description: 'Buscado empresas',
            status: options.FINISHED,
          }
        }
      })
      iniciadoPagina = true;
    }

    if(hasQuotes){
      this.setState({
        stepsToInitApplication: {
          ...this.state.stepsToInitApplication,
          [STEPS_DESCRIPTONS.FINDING_ALL_QUOTES]: {
            ...this.state.stepsToInitApplication.FINDING_ALL_QUOTES,
            description: 'Buscado cotações',
            status: options.FINISHED,
          }
        }
      });
    } else if(!iniciadoPagina) {
      atualizarCotacaoComGoogle(false)
      this.setState({
        stepsToInitApplication: {
          ...this.state.stepsToInitApplication,
          [STEPS_DESCRIPTONS.FINDING_ALL_QUOTES]: {
            ...this.state.stepsToInitApplication.FINDING_ALL_QUOTES,
            status: options.RUNING,
          },
        }
      });
      iniciadoPagina = true;
    }

    if(hasCompanies && hasFII && hasQuotes){
      this.setState({
        mudarRota: true
      });
    }

    this.setState({loadingPage: false})
  } 

  iniciarConsultasPagina(){
    findAllCompanies();
    this.setState({
      stepsToInitApplication: {
        ...this.state.stepsToInitApplication,
        [STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES]: {
          ...this.state.stepsToInitApplication.FINDING_ALL_COMPANIES,
          status: options.RUNING,
        },
      }
    });
  }

  checkChangeRouter() {
    const { stepsToInitApplication } = this.state;

    if(stepsToInitApplication[STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES].status === options.FINISHED &&
       stepsToInitApplication[STEPS_DESCRIPTONS.FINDING_ALL_QUOTES].status === options.FINISHED &&
       stepsToInitApplication[STEPS_DESCRIPTONS.FINDING_ALL_FII].status === options.FINISHED
    ){
      this.setState({ title: 'Inicialização concluida com sucesso!'})
      setTimeout(() => {
        this.setState({
          mudarRota: true
        });
      }, 5000);
    }
  }

  initWebSocket(){
    const { socket } = this.props;

    if(socket){
      socket.subscribe('/user/queue/finished-collecting-quotes', data => {
        const body = JSON.parse(data.body);
        this.setState({
          stepsToInitApplication: {
            ...this.state.stepsToInitApplication,
            [STEPS_DESCRIPTONS.FINDING_ALL_QUOTES]: {
              ...this.state.stepsToInitApplication.FINDING_ALL_QUOTES,
              description: 'Buscado cotações',
              status: options.FINISHED,
            }
          }
        }, () => this.checkChangeRouter());
      });
      socket.subscribe('/user/queue/finished-collecting-companies', data => {
        const body = JSON.parse(data.body);
        findAllFII();
        this.setState({
          stepsToInitApplication: {
            ...this.state.stepsToInitApplication,
            [STEPS_DESCRIPTONS.FINDING_ALL_COMPANIES]: {
              ...this.state.stepsToInitApplication.FINDING_ALL_COMPANIES,
              description: 'Buscado empresas',
              status: options.FINISHED,
            },
            [STEPS_DESCRIPTONS.FINDING_ALL_FII]: {
              ...this.state.stepsToInitApplication.FINDING_ALL_FII,
              status: options.RUNING,
            },
          }
        }, () => this.checkChangeRouter());
      });
      socket.subscribe('/user/queue/finished-collecting-fii', data => {
        const body = JSON.parse(data.body);
        atualizarCotacaoComGoogle(false)
        this.setState({
          stepsToInitApplication: {
            ...this.state.stepsToInitApplication,
            [STEPS_DESCRIPTONS.FINDING_ALL_FII]: {
              ...this.state.stepsToInitApplication.FINDING_ALL_FII,
              description: 'Buscado fundos imobiliários',
              status: options.FINISHED,
            },
            [STEPS_DESCRIPTONS.FINDING_ALL_QUOTES]: {
              ...this.state.stepsToInitApplication.FINDING_ALL_QUOTES,
              status: options.RUNING,
            },
          }
        }, () => this.checkChangeRouter());
      });
    }
  }

  renderLoadingStep = (step) => {
    if(step.status === options.FINISHED){
      return <Icon type="check-circle" />
    }
    if(step.status === options.RUNING){
      return <Spinner />
    }
    return <SpaceBlank />
  }

  render() {
    const { stepsToInitApplication, mudarRota = false, title = "", loadingPage = true } = this.state;

    return (
      <>
        {mudarRota && <Route render={() => <Redirect to={'/main/carteira/atual'} />} />}
        {loadingPage ? <SpinCuston /> : (
          <StyledLayout type="flex">
            <Content>
              <Title>
              {title} 
              </Title>
              <ContentSteps>
                {Object.keys(stepsToInitApplication).map((stepKey) => {
                  const step = stepsToInitApplication[stepKey];
                  return (
                    <ContentStep
                      key={stepKey}
                      className={step.status === options.RUNING ? 'running' : step.status === options.FINISHED ? 'finished' : 'stop'}
                    >
                      {this.renderLoadingStep(step)}
                      {step.description}
                    </ContentStep>
                  );
                })}
              </ContentSteps>
            </Content>
          </StyledLayout>
        )}
      </>    
    );
  }
}

const mapStateToProps = state => ({ 
  socket: state.websocket.socket
})

export default connect(mapStateToProps, undefined)(InitialApplication);
