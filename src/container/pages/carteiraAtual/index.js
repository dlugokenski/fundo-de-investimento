import React from 'react';
import { Modal, notification, Spin } from 'antd';
import { Switch, Route } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as actions} from 'Ducks/carteira'

import MainComponent from 'Pages/main'
import { buscarCarteiraFavoritaOuUltimaCadastrada } from 'Provider/Carteira'
import initCarteiraController from 'Pages/carteiraAtual/CarteiraAtualController'
import Ativo from 'Pages/ativo'
import Operacao from 'Pages/operacao'

class CarteiraAtual extends React.Component {
    
  state = {};

  componentDidMount(){ 
    this.iniciarTela() 
  }
  
  async iniciarTela(){
    this.props.adicionarCorAtivo([]);
    
    this.setState({ carregandoDados: true });

    const data = await buscarCarteiraFavoritaOuUltimaCadastrada();
    initCarteiraController(data);

    this.setState({ carregandoDados: false });
  }  

  info() {
    const { dataUltimAtualizacaoCotacao = "" } = this.state;
    Modal.info({
      title: 'Alerta',
      content: `Ultima atualização das cotações foi em ${dataUltimAtualizacaoCotacao} PM`,
    });
  }

  openNotification(type, title, description) {   
    notification[type || 'info']({
      message: title || 'Aviso',
      description: description,
    });
  }
    
  render(){ 
 
    const { 
      carregandoDados = false
    } = this.state;

    const { carteira } = this.props;
    
    return(
      <React.Fragment>
        {carregandoDados ?
          <Spin />
          : 
            carteira && carteira.listaDeOperacao && (
            <React.Fragment>
              <MainComponent  {...this.props} />  
              <Switch>
                <Route exact path="*/ativo" render={() => <Ativo reset={new Date().getTime()} />} />  
                <Route exact path="*/operacao" render={() => <Operacao reset={new Date().getTime()} />}  />  
              </Switch>          
            </React.Fragment>
          )                        
        }
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({ 
  usuario : state.configuracaoUsuario.usuario,
  carteira: state.carteira.carteira,
})

const mapDispatchToPros = dispatch => bindActionCreators(actions, dispatch)

export default connect(mapStateToProps,mapDispatchToPros)(CarteiraAtual)