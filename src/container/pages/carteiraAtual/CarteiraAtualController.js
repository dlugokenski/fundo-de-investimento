import { Creators as actions} from 'Ducks/carteira'
import { 
  buscarListaDeNoticias, 
  buscarCarteiraAtual,
  buscarAcoesCarteiraAtual,
  buscarTesouroCarteiraAtual,
  buscarFIICarteiraAtual,
  buscarComposicaoCarteiraAtual,
  getListAporteMensal,
  buscarListTradeInCache,
  buscarListaDeDividendosReceber,
  buscarListaDeDividendosReceberSeManter
} from 'Provider/Carteira'
import store from 'Store';

import { defaultCores } from 'Util/Cores';
import { hexToRGBA, randomHex } from 'Util/ColorUtil'

const initCarteiraController = async props => { 
  if(props){
    
    const listaNoticias = await buscarListaDeNoticias();
    const carteiraAtual = await buscarCarteiraAtual();
    const listaDeTrades = await buscarListTradeInCache();
    await adicionarCoresAtivos(listaDeTrades);
    composicao();

    store.dispatch(actions.atualizarCarteira({
      id: props.idCarteira ? props.idCarteira : 0,
      listaDeOperacao: props.listOperacoes,
      listaNoticias,
      carteiraAtual,
      listaDeTrades,
    }));
  }
};

const adicionarCoresAtivos = (listaDeTrades) =>{
  const newCoresAtivo = [];

  const setColor = (element,index) => {
    if(!newCoresAtivo.find(e => e.ativo == element.ticker.ativo.id)){
      let cor = defaultCores[index];
      cor = cor ? cor : randomHex();
      let colorRgb = hexToRGBA(cor)
      colorRgb = `rgb(${colorRgb.r}, ${colorRgb.g}, ${colorRgb.b})`
    
      const ativoCor = {
        ativo: element.ticker.ativo.id,
        corRgb: colorRgb,
        corHex: cor
      }
      newCoresAtivo.push(ativoCor)
    } 
  }
  listaDeTrades.filter(e => e.aberto).forEach((element, index) => setColor(element, index));
  listaDeTrades.forEach((element, index) => setColor(element, index));;
  store.dispatch(actions.adicionarCorAtivo(newCoresAtivo)); 
}

const composicao = async () => {
  const composicaoAcao = await buscarAcoesCarteiraAtual();
  const composicaoTesouro = await buscarTesouroCarteiraAtual();
  const composicaoFII = await buscarFIICarteiraAtual();
  const composicaoCarteira = await buscarComposicaoCarteiraAtual();   
  const listaAporteMensal = await getListAporteMensal();
  const listaDeDividendosReceber = await buscarListaDeDividendosReceber();
  const listaDeDividendosReceberSeManter = await buscarListaDeDividendosReceberSeManter();

  store.dispatch(actions.atualizarCarteira({
    listaDeAcoes: composicaoAcao,
    listaDeFundosImobiliarios: composicaoFII,
    listaDeTesouro: composicaoTesouro,
    composicaoCarteira,
    listaAporteMensal,
    listaDeDividendosReceber,
    listaDeDividendosReceberSeManter
  }));
}

export default initCarteiraController;

