import React from 'react';
import { Modal, Button } from 'antd';

import GraficoBar from 'Pages/charts/BarAporteMensal'

export default props => (
  <React.Fragment>
    <Modal
        width={1000}
        style={{ top: '20px' }}
        visible={props.visible}
        onCancel={() => props.handleCancel()}
        title="Aportes mensais"
        footer={[
            <Button key="back" onClick={() => props.handleCancel()}>
            {'Fechar'}
            </Button>
        ]}
    >
        <GraficoBar data={props.data} renderModeCarossel={true} />
    </Modal>
  </React.Fragment>
)