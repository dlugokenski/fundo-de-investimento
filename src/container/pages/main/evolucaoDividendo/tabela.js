import React from 'react';
import { Table, Checkbox } from 'antd';
import styled from 'styled-components';

import LifeCicleTimeLineHorizontal from 'Componente/LifeCicleTimeLineHorizontal'
import imagenArrowGreen from 'Imagens/arrow-green.png';
import imagenArrowRed from 'Imagens/arrow-red.png';

const StyleDivArrow = styled.div`
    background-image: ${props => props.aumentou === 'sim' ? `url(${imagenArrowGreen});` : `url(${imagenArrowRed});`}; 
    position: relative;
    height: 20px;
    top: 5px;
    width: 50px !important;   
    background-repeat: no-repeat;
    background-size: 100%; 
    background-position: center;
    opacity: 0.6;
    border: 0px;
`;

const StyleDivContent = styled.div`
    display: flex;
    justify-content: space-between;
`;

const StyleDivArrowAndPorcentagem = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const StyleDivAlteracaoValor = styled.div`
    font-size: 11px;
    color: ${props => props.aumentou === 'sim' ? 'green' : 'red'}; 
    margin-bottom: -13px;
    margin-top: -5px;
`;

const StyleCheckbox = styled(Checkbox)`
    margin-top: -20px !important;
    display: initial !important;
    left: 0;
    position: absolute;
`;

export default class TabelaEvolucaoDividendoComponent extends React.Component {
    
    state = {};

    componentDidMount(){
        this.iniciarTabela();
    }

    componentDidUpdate(){      
    }

    iniciarTabela(){
        const { data = [] } = this.props;        
        const anos = this.checkAnos();
        let dataState = {
            tipo: "",
            data: []
        };

        if(anos.length > 1 || data.length > 6){
            dataState = {
                tipo: "trimestre",
                data:  this.seprarPorAnos(this.separarPorTrimestre(anos), anos)
            }
        } else {
            dataState = {
                tipo: "mensal",
                data: this.separarPorMes(data)
            }
        }
        this.setState({ dataState });
    }

    seprarPorAnos = (data, anos) => {
        const dataReturn = [];
        const anoFilter = anos.map(element => data.filter(elementDate => elementDate.ano === element));
        for (let i = 0; i < anoFilter.length; i++) {
            const elementVez = anoFilter[i];
            let trimestreOne = elementVez.find(element => element.periodo.referencia === 1);
            let trimestreTwo = elementVez.find(element => element.periodo.referencia === 2);
            let trimestreThree = elementVez.find(element => element.periodo.referencia === 3);
            let trimestreFour = elementVez.find(element => element.periodo.referencia === 4);
            const valorTrimestreOne = trimestreOne && trimestreOne.valor ? trimestreOne.valor : 0;
            const valorTrimestreTwo = trimestreTwo && trimestreTwo.valor ? trimestreTwo.valor : 0;
            const valorTrimestreThree = trimestreThree && trimestreThree.valor ? trimestreThree.valor : 0;
            const valorTrimestreFour = trimestreFour && trimestreFour.valor ? trimestreFour.valor : 0;
            const valorTotal = valorTrimestreOne + valorTrimestreTwo + valorTrimestreThree + valorTrimestreFour;
            const media = valorTotal / 12;

            const dataElement = {
                ano: elementVez[0].ano,
                trimestreOne: valorTrimestreOne,
                trimestreTwo: valorTrimestreTwo,
                trimestreThree: valorTrimestreThree,
                trimestreFour: valorTrimestreFour,
                total: valorTotal,
                media
            }
            dataReturn.push(dataElement)       
        }
        return dataReturn;
    }


    separarPorMes = (data) => {
        const dataReturn = [];
        const dataVez = new Date(data[0].periodo);
        const ano = dataVez.getFullYear();
        let dataElement = {
            ano
        }
        data.forEach(element => {
            const mes = dataVez.getUTCMonth() + 1;
            dataElement[`${mes}`] = element.valor ? element.valor : 0;
        })
        dataReturn.push(dataElement);
        return dataReturn;
    }

    separarPorTrimestre(){
        const { data = [] } = this.props;        
        const trimestreAgrupado = [];
        const dataOrdenada = data.sort((a, b) => {
            return new Date(a.periodo) < new Date(b.periodo) ? -1 : 0
        });

        dataOrdenada.forEach(element => {
            const dataVez = new Date(element.periodo);
            const ano = dataVez.getFullYear();
            const mes = dataVez.getUTCMonth() + 1;
            const trimestre = this.indentificarTrimestre(mes);

            if(trimestreAgrupado.length === 0){
                trimestreAgrupado.push({ 
                    ano, 
                    periodo: {
                        tipo: "trimestre",
                        referencia: trimestre
                    },
                    valor: element.valor 
                })
            } else {
                let elementEncontrado = false;
                for (let i = 0; i < trimestreAgrupado.length; i++) {
                    const anoElementVez = trimestreAgrupado[i].ano;
                    const trimestreVez = trimestreAgrupado[i].periodo.referencia;
                    if( anoElementVez === ano && trimestreVez === trimestre){
                        elementEncontrado = true;
                        trimestreAgrupado[i].valor += element.valor ;
                        break;
                    }
                }
                if(!elementEncontrado){
                    trimestreAgrupado.push({ 
                        ano, 
                        periodo: {
                            tipo: "trimestre",
                            referencia: trimestre
                        }, 
                        valor: element.valor 
                    })
                }
            }
        });
        return trimestreAgrupado;
    }

    indentificarTrimestre(mes){
        if(mes > 0 && mes < 4){
            return 1;
        } else if(mes > 3 && mes < 7){
            return 2;
        } else if(mes > 6 && mes < 10){
            return 3;
        } else {
            return 4;
        }
    }

    checkAnos(){
        const { data = [] } = this.props;        
        const anos = [];
        
        data.forEach(element => {
            const anoElement = new Date(element.periodo).getFullYear();
            const indexElementArray = anos.indexOf(anoElement);
            if(indexElementArray === -1){
                anos.push(anoElement);
            }
        });
        return anos;
    }

    getColunsMes(){
        const { dataState = [] } = this.state;
        const obj = dataState.data[0] ? dataState.data[0] : {};

        const columnsTrimestre = [{
            title: 'Ano',
            dataIndex: 'ano'
        }]

        Object.keys(obj).forEach(function(key, index) {
            if(key != "ano"){
                columnsTrimestre.push({
                    title: `Mês ${key}`,
                    dataIndex: key,
                    render: valor => valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
                })
            }    
        });
        return columnsTrimestre;
    }

    checkRenderLineTableTable(valor, data, refElemento){
        const returnPadrao = valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        if(valor === 0){
            return returnPadrao;
        }

        let nextElement = {
            ref: "",
            value: 0
        }
        let getnextElement = false;
        Object.keys(data).forEach(function(key) {
            if(getnextElement){
                nextElement = {
                    ref: key,
                    value: data[key]
                }
                getnextElement = false;
            }
            if(key === refElemento){
                getnextElement = true;
            }    
        });
        if(nextElement.value === 0){
            return returnPadrao;
        }

        let aumentouDividendo = 'não';
        if(nextElement.value > valor){
            aumentouDividendo = "sim";
        }
        
        const porcentagemAlterado = `${(((nextElement.value * 100) / valor) - 100).toFixed(1)}%`;

        return this.renderLineTableTable(valor, aumentouDividendo, porcentagemAlterado);
    }

    renderLineTableTable = (previousElement, aumentouDividendo, porcentagemAlterado) => (
        <StyleDivContent>
            {previousElement.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}
            <StyleDivArrowAndPorcentagem>
                <StyleDivAlteracaoValor aumentou={aumentouDividendo}>
                    {porcentagemAlterado}
                </StyleDivAlteracaoValor>              
                <StyleDivArrow aumentou={aumentouDividendo} />
            </StyleDivArrowAndPorcentagem>           
        </StyleDivContent>
    )

    getColunsTrimestre(){
        return [
            {
                title: 'Ano',
                dataIndex: 'ano'
            },
            {
                title: 'Trimestre 1',
                dataIndex: 'trimestreOne',
                render: (valor, data) => this.checkRenderLineTableTable(valor, data, 'trimestreOne')
            },
            {
                title: 'Trimestre 2',
                dataIndex: 'trimestreTwo',
                render: (valor, data) => this.checkRenderLineTableTable(valor, data, 'trimestreTwo')
            },
            {
                title: 'Trimestre 3',
                dataIndex: 'trimestreThree',
                render: (valor, data) => this.checkRenderLineTableTable(valor, data, 'trimestreThree')
            },
            {
                title: 'Trimestre 4',
                dataIndex: 'trimestreFour',
                render: (valor, data) => this.checkRenderLineTableTable(valor, data, 'trimestreFour')
            },
            {
                title: 'Total',
                dataIndex: 'total',
                render: valor => valor.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
            }
        ];
    }

    getColuns(){
        const { dataState = [] } = this.state;
        if(dataState && dataState.data && dataState.data.length > 0){
            return dataState.tipo === "trimestre" ? this.getColunsTrimestre() : this.getColunsMes();
        } else{
            return [];
        }
        
    }

    render(){
        const { pageSize = 6 } = this.props;
        const { dataState = [], modeAnual = false } = this.state;

        return(
        <React.Fragment>
            {dataState && dataState.tipo &&  dataState.tipo === 'trimestre' && dataState.data && dataState.data.length > 1 && (
                <StyleCheckbox 
                    checked={ modeAnual }
                    onChange={e => this.setState({ modeAnual: e.target.checked})}
                >
                    {!modeAnual ? 'Visualização anualizada' : 'Visualização trimestral'}
                </StyleCheckbox> 
            )}
            {!modeAnual ? (
                <Table  
                    pagination={{ pageSize: pageSize }}
                    columns={this.getColuns()} 
                    dataSource={dataState.data} 
                    size="small"
                    rowKey={data => data.ano}
                />
            ) : (
                <LifeCicleTimeLineHorizontal 
                    listValores = {dataState.data}
                    evolucao={true}
                 />
            )}
           
        </React.Fragment>
        )
  }
}
