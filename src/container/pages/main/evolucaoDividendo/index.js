import React from 'react';
import styled from 'styled-components';

import BarEvolucaoDividendo from 'Pages/charts/BarEvolucaoDividendo';
import TabelaEvolucaoDividendo from './tabela'

const FlipCard = styled.div`
  background-color: transparent;
  width: 100%;
  height: 400px;
  perspective: 1000px;  
`;

const FlipCardInner = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  text-align: center;
  transition: transform 1s;
  transform-style: preserve-3d;


  ${props => props.visibleEvolucaoDividendoModeTable && `
    transform: rotateY(180deg);
  `}
`;

const FlipCardFrente = styled.div`
  background-color: white;
  color: black;
  position: absolute;
  width: 100%;
  height: 100%;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
`;

const FlipCardTras = styled.div`
  background-color: white;
  transform: rotateY(180deg);
  position: absolute;
  width: 100%;
  height: 100%;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
`;

export default class GraficoBar extends React.Component {
  render() {       
    return (
      <FlipCard visibleEvolucaoDividendoModeTable={this.props.visibleEvolucaoDividendoModeTable}>
        <FlipCardInner visibleEvolucaoDividendoModeTable={this.props.visibleEvolucaoDividendoModeTable}>
          <FlipCardFrente>
            <BarEvolucaoDividendo {...this.props} />
          </FlipCardFrente>
          <FlipCardTras>
            <TabelaEvolucaoDividendo {...this.props} />
          </FlipCardTras>
        </FlipCardInner>
      </FlipCard>
    );
  }
}
