import React from 'react';
import { Statistic, Icon } from 'antd';
import styled from 'styled-components';
import imagenDiv from 'Imagens/carteira.png';
import LabelMoney from 'Componente/LabelMoney';

const StyleBackground = styled.div`
    margin: 0 auto;
    background-image: url(${imagenDiv});
    height: 200px;
    width: 200px !important;
    left: ${(props) => (props.renderModeCarossel ? '0%' : '35%')} 
    top: ${(props) => (props.renderModeCarossel ? '0px' : '20px')} 
    position: relative;
    background-repeat: no-repeat;
    background-size: ${(props) => (props.renderModeCarossel ? '60%' : '80%')} 
    background-position: center;
    opacity: 0.6;
    border: 0px;

    @media (max-width: 400px) {
        left: 0%; 
        background-size: 60%;  
        top: 0px;  
    }
`;

const StyleDiv = styled.div`
  height: ${(props) => (props.renderModeCarossel ? '370px' : '400px')};
  width: 100%;
  overflow: hidden;
  text-align: center;

  @media (max-width: 400px) {
    ${(props) =>
      props.porcentagemHistorico &&
      props.porcentagemHistorico === 'nao' &&
      `height: 300px`}
    h2 {
      font-size: 1.3em;
    }
  }
`;

const StyleContent = styled.div`
  display: inline-flex;
  align-items: flex-end;
`;

const StyleConteudo = styled.div`
  margin-top: ${(props) => (props.porcentagemHistorico ? '0px' : '50px')} @media
    (max-width: 400px) {
    margin-top: 0px;
  }
`;

const StyledStatistic = styled(Statistic)`
  .ant-statistic-content {
    margin-bottom: -2px;
  }

  .ant-statistic-content-value-int {
    font-size: 17px;
  }

  .ant-statistic-content-value-decimal {
    font-size: 13px;
  }
`;

const StyleConteudoDivHistorico = styled.div`
    width: 64% 
    border-top: 1px solid;
    left: 18%;
    position: relative;
    margin-top: 10px;
    padding-top: 10px;

    @media (max-width: 400px) {
        width: 100%;
        left: 0%; 
    }

`;

export default class InformacaoCarteira extends React.Component {
  state = {
    porcentagemCarteiraAtual: undefined,
    porcentagemHistorico: undefined,
  };

  componentDidMount() {
    this.calularPorcentage();
  }

  componentDidUpdate(newProps) {
    if (newProps != this.props) {
      this.calularPorcentage();
    }
  }

  calularPorcentage() {
    const { data } = this.props;

    let porcentagemCarteiraAtual;
    let porcentagemHistorico;
    let valorCarteiraAtual;
    let valorCarteiraHistorico;

    if (data && data.valorTotalInvestido && data.valorAtualInvestido) {
      porcentagemCarteiraAtual = (
        (data.valorAtualInvestido * 100) / data.valorTotalInvestido -
        100
      ).toFixed(2);
      valorCarteiraAtual = (
        data.valorAtualInvestido - data.valorTotalInvestido
      ).toFixed(2);
      const newValorCarteiraAtual = valorCarteiraAtual
        .toString()
        .replace('.', ',');
      this.setState({
        porcentagemCarteiraAtual,
        valorCarteiraAtual: newValorCarteiraAtual,
      });
    }

    if (data && data.valorJusto && data.valorAtualInvestido) {
      porcentagemHistorico = (
        (data.valorAtualInvestido * 100) / data.valorJusto -
        100
      ).toFixed(2);
      valorCarteiraHistorico = (
        data.valorAtualInvestido - data.valorJusto
      ).toFixed(2);
      valorCarteiraHistorico = valorCarteiraHistorico
        .toString()
        .replace('.', ',');
      if (porcentagemCarteiraAtual != porcentagemHistorico) {
        this.setState({ porcentagemHistorico, valorCarteiraHistorico });
      }
    }
  }

  render() {
    const {
      porcentagemCarteiraAtual,
      porcentagemHistorico,
      valorCarteiraAtual,
      valorCarteiraHistorico,
    } = this.state;

    return (
      <StyleDiv
        {...this.props}
        porcentagemHistorico={porcentagemHistorico ? 'sim' : 'nao'}
      >
        <StyleConteudo {...this.state}>
          <h2>Rentabilidade da carteira atual</h2>
          <StyleContent>
            <h3>
              <StyledStatistic
                value={porcentagemCarteiraAtual}
                precision={2}
                valueStyle={{
                  color: porcentagemCarteiraAtual > 0 ? 'green' : 'red',
                }}
                prefix={
                  porcentagemCarteiraAtual > 0 ? (
                    <Icon width="9" height="9" type="arrow-up" />
                  ) : (
                    <Icon width="9" height="9" type="arrow-down" />
                  )
                }
                suffix="%"
              />
            </h3>
            <h3 style={{ marginLeft: '20px' }}>
              <LabelMoney animation={true} value={valorCarteiraAtual} />
            </h3>
          </StyleContent>
        </StyleConteudo>
        {porcentagemHistorico && (
          <StyleConteudoDivHistorico>
            <h2>Rentabilidade histórico</h2>
            <StyleContent>
              <h3>
                <StyledStatistic
                  value={porcentagemHistorico}
                  precision={2}
                  valueStyle={{
                    color: porcentagemHistorico > 0 ? 'green' : 'red',
                  }}
                  prefix={
                    porcentagemHistorico > 0 ? (
                      <Icon width="9" height="9" type="arrow-up" />
                    ) : (
                      <Icon width="9" height="9" type="arrow-down" />
                    )
                  }
                  suffix="%"
                />
              </h3>
              {valorCarteiraHistorico && (
                <h3 style={{ marginLeft: '20px' }}>
                  <LabelMoney animation={true} value={valorCarteiraHistorico} />
                </h3>
              )}
            </StyleContent>
          </StyleConteudoDivHistorico>
        )}
        <StyleBackground {...this.props} />
      </StyleDiv>
    );
  }
}
