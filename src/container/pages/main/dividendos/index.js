import React from 'react';
import { Tooltip, Table } from 'antd';
import styled from 'styled-components';
import { isMobile } from 'react-device-detect';
import moment from 'moment';

const StyleTable = styled(Table)`
  @media (max-width: 400px) {
    white-space: nowrap;

    td{
      padding: 6px 6px !important;
      padding-left: 10px !important;
    }
  } 
`;

export default class TabelaCarteiraDividendosReceber extends React.Component {
    
    state = {};
  
    componentDidUpdate(){
         
    }


    render(){ 
        const columnsDividendosaReceber = [
            {
                title: 'Ativo',
                dataIndex: 'ativo',
                render: data => data.nomePregao ? data.nomePregao : data.nome
            },
            {
                title: "Valor do provento",
                dataIndex: 'valorProvento',
                render: valorProvento => valorProvento.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
            },   
            {
                title: "Data pagamento",
                dataIndex: 'dataPagamento',
                render: data => data != 253402225200000 ? new moment(new Date(data)).format("DD/MM/YYYY") : "Não definido"
            }
        ];
        const { dataTable, pageSize = 8 } = this.props;

        return(
            <React.Fragment>
                <StyleTable  
                    pagination={{ pageSize: pageSize }}
                    columns={columnsDividendosaReceber} 
                    dataSource={dataTable} 
                    size="small"
                    rowKey={data => `${data.resultadoFinanceiro}${Math.random()}`}
                />
            </React.Fragment>
        )
    }
}
