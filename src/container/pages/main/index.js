import React from 'react';
import { isMobile } from 'react-device-detect';
import styled from 'styled-components';
import Loadable from 'react-loadable';
import {
  Button,
  Modal,
  Row,
  Tooltip,
  Table,
  Tag,
  Col,
  Card,
  Icon,
  Rate,
  List,
  Popconfirm,
  notification,
} from 'antd';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as actions } from 'Ducks/carteira';

import ModalRentabilidadeTrade from './ModalRentabilidadeTrade';
import ModalAportesMensal from './ModalAportesMensal';
import DividendosReceber from './dividendos';
import ImpostoDeRenda from './impostoDeRenda';
import SelectDelete from 'Componente/SelectDelete';
import SpinCuston from 'Componente/Spin';
import FlexBox from 'Componente/flexBox/FlexBox.jsx';
import {
  buscarListaDeCarteiras,
  apagarCarteira,
  buscarCarteira,
  adicionarCarteiraFavorita,
} from 'Provider/Carteira';
import { consultarAtivo } from 'Provider/Ativo';

import { apagarOperacao } from 'Provider/Operacao';
import initCarteiraController from 'Pages/carteiraAtual/CarteiraAtualController';
import { TIPO_NOTICIAS } from 'Util/Constantes';
import { dataFormatado } from 'Util/Formatacao';

const ListItem = List.Item;
const ListItemMeta = ListItem.Meta;

const StyleCard = styled(Card)`
  width: 100%;
  min-height: 500px;
  max-height: ${(props) => (props.maxHeight ? props.maxHeight : '500px')};
  margin-bottom: 20px !important;
  padding-bottom: -90px !important;

  .ant-card-body {
    margin-top: ${(props) =>
      props.poussioverflow && props.poussioverflow === 'sim' ? '0px' : '10px'};
    padding: ${(props) =>
      props.poussioverflow && props.poussioverflow === 'sim' ? '0px' : '24px'};
    max-height: ${(props) =>
      props.poussioverflow && props.poussioverflow === 'sim'
        ? '445px'
        : 'auto'};
    overflow: auto;
  }

  @media (max-width: 400px) {
    min-height: ${(props) =>
      props.modotable && props.modotable === 'sim' ? '0px' : '500px'};
    .ant-table-wrapper {
      padding: 0px;
      overflow-y: auto;
      display: flex;
    }

    .ant-pagination {
      position: sticky;
      right: 0px;
    }

    .chart-bar {
      overflow-y: auto;
      display: flex;
      left: 0;
    }
  }

  .ant-card-extra {
    padding: 0px;
  }
`;

const StyleRowSelectDelet = styled(Row)`
  margin-top: -20px;

  @media (max-width: 400px) {
    margin-left: 50px;
    margin-top: 40px;
  }
`;

const StyleList = styled(List)`
  overflow: auto;
  max-height: 400px;
  cursor: pointer;

  .ant-list-item-meta-content {
    padding-left: 10px;
  }
`;

// Render the nav arrows
function CustomArrow(props) {
  const { className, style, onClick, dir } = props;
  return (
    <button
      type="button"
      className={className}
      style={{ ...style }}
      onClick={onClick}
    >
      <Icon type={dir} />
    </button>
  );
}

// Default settings for Slick Slider
const settings = {
  dots: true,
  arrows: true,
  autoplay: true,
  centerMode: true,
  centerPadding: '0px',
  slidesToShow: 1,
  slidesToScroll: 1,
  nextArrow: <CustomArrow dir="right" />,
  prevArrow: <CustomArrow dir="left" />,
};

const Pie = Loadable({
  loader: () => import('Pages/charts/pie/Pie'),
  loading: () => <SpinCuston />,
});

const PieComposicaoCarteira = Loadable({
  loader: () => import('Pages/charts/pie/PieComposicaoCarteira'),
  loading: () => <SpinCuston />,
});

const InformacoesCarteira = Loadable({
  loader: () => import('./InformacoesCarteira'),
  loading: () => <SpinCuston />,
});

const GraficoBar = Loadable({
  loader: () => import('Pages/charts/BarCustoValorAtivos'),
  loading: () => <SpinCuston />,
});

const TableCateiraAtual = Loadable({
  loader: () => import('./tableCarteiraAtual'),
  loading: () => <SpinCuston />,
});

const GraficoBarHistorico = Loadable({
  loader: () => import('Pages/charts/BarHistoricoTrade'),
  loading: () => <SpinCuston />,
});

const GraficoBarAporteMensal = Loadable({
  loader: () => import('Pages/charts/BarAporteMensal'),
  loading: () => <SpinCuston />,
});

const GraficoBarDividendosRecebidos = Loadable({
  loader: () => import('Pages/charts/BarDividendosRecebidos'),
  loading: () => <SpinCuston />,
});

const GraficoBarDividendosAReceber = Loadable({
  loader: () => import('Pages/charts/BarDividendosAReceber'),
  loading: () => <SpinCuston />,
});

const GraficoBarDividendoOnCost = Loadable({
  loader: () => import('Pages/charts/BarDividendCost'),
  loading: () => <SpinCuston />,
});

const EvolucaoDividendo = Loadable({
  loader: () => import('./evolucaoDividendo'),
  loading: () => <SpinCuston />,
});

const PieComposicaoCarteiraSegmentos = Loadable({
  loader: () => import('Pages/charts/pie/PieComposicaoCarteiraSegmentos'),
  loading: () => <SpinCuston />,
});

class MainComponent extends React.Component {
  state = {
    renderCarrosel: true,
    pauseCarrosel: false,
    listCarteiras: [],
    loadingPage: false,
    dadosCarteiraAtual: undefined,
    renderModeCarossel: true,
  };

  componentDidMount() {
    this.initTela();
    this.verificarComposicaoCarteira();
  }

  componentDidUpdate(newProps) {
    if (
      newProps != this.props &&
      ((newProps.usuario != this.props.usuario &&
        newProps.usuario.id != this.props.usuario.id) ||
        newProps.carteira != this.props.carteira)
    ) {
      this.calcularValorAtualInvestiddo();
      this.verificarComposicaoCarteira();
    }
  }

  async initTela() {
    const modoCarossel = localStorage.getItem('renderModeCarossel');

    if (modoCarossel === 'nao' || isMobile) {
      this.setState({ renderModeCarossel: false });
    }
    await this.initSeletecCarteira();
    this.calcularValorAtualInvestiddo();
  }

  async verificarComposicaoCarteira() {
    const { carteira } = this.props;

    let possuiAcao = false;
    let possuiTesouroDireto = false;
    let possuiFundosImobiliarios = false;

    if (carteira) {
      if (
        carteira.listaDeAcoes &&
        carteira.listaDeAcoes.listaTrade &&
        carteira.listaDeAcoes.listaTrade.length > 0
      ) {
        possuiAcao = true;
      }

      if (
        carteira.listaDeTesouro &&
        carteira.listaDeTesouro.listaTrade &&
        carteira.listaDeTesouro.listaTrade.length > 0
      ) {
        possuiTesouroDireto = true;
      }

      if (
        carteira.listaDeFundosImobiliarios &&
        carteira.listaDeFundosImobiliarios.listaTrade &&
        carteira.listaDeFundosImobiliarios.listaTrade.length > 0
      ) {
        possuiFundosImobiliarios = true;
      }
    }
    this.setState({
      possuiAcao,
      possuiTesouroDireto,
      possuiFundosImobiliarios,
    });
  }

  async initSeletecCarteira() {
    const { usuario } = this.props;
    const listCarteiras = await buscarListaDeCarteiras(usuario.id);
    this.montedSelectDelete(listCarteiras);
  }

  montedSelectDelete(lista = []) {
    const listCarteiras = lista.map((element, index) => {
      return {
        value: element.id,
        label: element.name || `Carteira ${parseInt(index + 1)}`,
        handleChangeSelect: (event) => this.buscarCarteiraId(event),
        handleChangeDelete: (event) => this.apagarCarteira(event),
        extraItem: [
          [
            <Rate
              key={element.id}
              count={1}
              onChange={() =>
                this.adicionarFavorito(element.id, !element.favorita)
              }
              value={element.favorita ? 1 : 0}
              tooltips={[
                element.favorita
                  ? 'Carteira principal'
                  : 'Adicionar como carteira principal',
              ]}
            />,
          ],
        ],
        favorita: element.favorita || false,
      };
    });
    this.setState({ listCarteiras });
  }

  async adicionarFavorito(idCarteira, ativar) {
    const { listCarteiras } = this.state;
    const { usuario } = this.props;

    await adicionarCarteiraFavorita(usuario.id, idCarteira, ativar);
    this.initSeletecCarteira();

    const newListCarteiras = listCarteiras.map((element) => {
      return {
        ...element,
        favorita: element.value === idCarteira ? ativar : false,
      };
    });
    this.setState({ listCarteiras: newListCarteiras });
  }

  async buscarCarteiraId(id) {
    this.setState({ loadingPage: true });
    this.props.adicionarCorAtivo([]);
    const data = await buscarCarteira(id);
    initCarteiraController(data);
    this.setState({ loadingPage: false });
  }

  async apagarCarteira(id) {
    try {
      await apagarCarteira(id);
      this.openNotification(
        'success',
        'Alerta',
        'Carteira apagada com sucesso!'
      );
      this.initSeletecCarteira();
      this.props.adicionarCarteira({});
    } catch (err) {
      console.log('err', err);
    }
  }

  openNotification(type, title, description) {
    notification[type || 'info']({
      message: title || 'Aviso',
      description: description,
    });
  }

  info() {
    const { dataUltimAtualizacaoCotacao = '' } = this.state;
    Modal.info({
      title: 'Alerta',
      content: `Ultima atualização das cotações foi em ${dataUltimAtualizacaoCotacao} PM`,
    });
  }

  resetCarrosel = () => {
    this.setState({ renderCarrosel: false }, () =>
      this.setState({ renderCarrosel: true })
    );
  };

  renderModalAporteMensal() {
    const { visibleGraficoAporteMensal = false } = this.state;
    const { carteira = {} } = this.props;

    if (visibleGraficoAporteMensal) {
      return (
        <ModalAportesMensal
          visible={visibleGraficoAporteMensal}
          data={carteira.listaAporteMensal}
          handleCancel={() =>
            this.setState({ visibleGraficoAporteMensal: false }, () =>
              this.resetCarrosel()
            )
          }
        />
      );
    }
    return null;
  }

  renderModalRentabilidadeTrade() {
    const { modalRentabilidadeTrade = false } = this.state;
    const { carteira = {} } = this.props;

    if (modalRentabilidadeTrade) {
      return (
        <ModalRentabilidadeTrade
          visible={modalRentabilidadeTrade}
          data={carteira.listaDeTrades}
          handleCancel={() =>
            this.setState({ modalRentabilidadeTrade: false }, () =>
              this.resetCarrosel()
            )
          }
        />
      );
    }
    return null;
  }

  alterRenderModeVisualizacao(val) {
    localStorage.setItem('renderModeCarossel', val ? 'sim' : 'nao');
    this.setState({ renderModeCarossel: val });
  }

  calcularValorAtualInvestiddo() {
    const { carteira } = this.props;

    if (carteira.listaAporteMensal) {
      let dadosCarteiraAtual = Object.assign({}, carteira.carteiraAtual);
      let valorAtualInvestido = 0;

      carteira.listaAporteMensal.forEach((element) => {
        valorAtualInvestido += element.saldoTotal;
      });
      dadosCarteiraAtual = {
        ...dadosCarteiraAtual,
        valorJusto: valorAtualInvestido,
      };
      this.setState({ dadosCarteiraAtual });
    }
  }

  verificarSeExisteTradeFechado() {
    const { carteira = {} } = this.props;
    let possuiTradeFinalizado = false;

    if (carteira && carteira.listaDeTrades) {
      for (let i = 0; i < carteira.listaDeTrades.length; i++) {
        if (!carteira.listaDeTrades[i].aberto) {
          possuiTradeFinalizado = true;
          break;
        }
      }
    }
    return possuiTradeFinalizado;
  }

  checkVisibleDividendOnCost() {
    const { carteira = {} } = this.props;
    if (
      carteira &&
      carteira.carteiraAtual &&
      Object.keys(carteira.carteiraAtual).length !== 0 &&
      carteira.carteiraAtual.listaTrade &&
      carteira.carteiraAtual.listaTrade.length > 0
    ) {
      for (let i = 0; i < carteira.carteiraAtual.listaTrade.length; i++) {
        const element = carteira.carteiraAtual.listaTrade[i];
        if (
          element.ticker.ativo.dividendYield > 0 &&
          element.ticker.ativo.listaDeDividendos &&
          element.ticker.ativo.listaDeDividendos.length > 0
        ) {
          return true;
        }
      }
      return false;
    }
    return false;
  }

  verificarSeJaGanhouDividendos() {
    const { carteira = {} } = this.props;
    let possuiDividendosRecebidos = false;

    if (carteira && carteira.listaDeTrades) {
      for (let i = 0; i < carteira.listaDeTrades.length; i++) {
        const trade = carteira.listaDeTrades[i];
        if (trade.listaDeDividendos.length > 0) {
          possuiDividendosRecebidos = true;
          break;
        }
      }
    }
    return possuiDividendosRecebidos;
  }

  settingsCarrosel() {
    const { pauseCarrosel } = this.state;

    if (pauseCarrosel) {
      return {
        ...settings,
        autoplay: false,
      };
    } else {
      return settings;
    }
  }

  extraChartDesempenho() {
    return (
      <React.Fragment>
        <Tooltip placement="left" title="Expandir visualização">
          <Button
            type="primary"
            icon="zoom-in"
            onClick={() =>
              this.setState({ renderModalPrecoCusto: true }, () =>
                this.setState({ renderModalPrecoCusto: false })
              )
            }
            shape="circle"
          />
        </Tooltip>
      </React.Fragment>
    );
  }

  extraChartAporte() {
    return (
      <React.Fragment>
        <Tooltip placement="left" title="Expandir visualização">
          <Button
            type="primary"
            icon="zoom-in"
            onClick={() => this.setState({ visibleGraficoAporteMensal: true })}
            shape="circle"
          />
        </Tooltip>
      </React.Fragment>
    );
  }

  extraModeAnualizado() {
    const { visibleEvolucaoDividendoModeTable = false } = this.state;
    return (
      <React.Fragment>
        <Tooltip
          placement="left"
          title={
            visibleEvolucaoDividendoModeTable ? 'Modo Grafico' : 'Modo tabela'
          }
        >
          <Button
            style={{ marginRight: '5px' }}
            type="primary"
            icon={
              visibleEvolucaoDividendoModeTable ? 'bar-chart' : 'unordered-list'
            }
            onClick={() =>
              this.setState({
                visibleEvolucaoDividendoModeTable: !visibleEvolucaoDividendoModeTable,
              })
            }
            shape="circle"
          />
        </Tooltip>
      </React.Fragment>
    );
  }

  extraChartHistoricoTrade() {
    return (
      <React.Fragment>
        <Tooltip placement="left" title="Expandir visualização">
          <Button
            type="primary"
            icon="zoom-in"
            onClick={() => this.setState({ modalRentabilidadeTrade: true })}
            shape="circle"
          />
        </Tooltip>
      </React.Fragment>
    );
  }

  extraTabelaCarteiraAtual() {
    return (
      <React.Fragment>
        <Tooltip placement="left" title="Expandir visualização">
          <Button
            type="primary"
            icon="zoom-in"
            onClick={() =>
              this.setState({ renderModalTabelaCarteiraAtual: true }, () =>
                this.setState({ renderModalTabelaCarteiraAtual: false })
              )
            }
            shape="circle"
          />
        </Tooltip>
      </React.Fragment>
    );
  }

  extraSelecionadoSelect() {
    const { carteira } = this.props;
    const { listCarteiras } = this.state;

    if (listCarteiras && carteira) {
      for (let index = 0; index < listCarteiras.length; index++) {
        if (listCarteiras[index].value === carteira.id) {
          return (
            <Rate
              key={`${listCarteiras[index].value}${listCarteiras[index].label}`}
              style={{ marginLeft: '10px' }}
              count={1}
              value={listCarteiras[index].favorita ? 1 : 0}
              tooltips={[
                listCarteiras[index].favorita
                  ? 'Carteira principal'
                  : 'Adicionar como carteira principal',
              ]}
              onChange={() =>
                this.adicionarFavorito(
                  listCarteiras[index].value,
                  !listCarteiras[index].favorita
                )
              }
            />
          );
        }
      }
    }
  }

  getListOperations = () => {
    const { carteira } = this.props;
    if (carteira && carteira.listaDeOperacao) {
      return carteira.listaDeOperacao.reverse();
    } else {
      return [];
    }
  };

  render() {
    const columns = [
      {
        title: (e) => {
          return (
            <Tooltip placement="top" title="Data da negociação">
              <span>Data da neg.</span>
            </Tooltip>
          );
        },
        dataIndex: 'data',
        width: 100,
        render: (e) => dataFormatado(e),
      },
      {
        title: () => (
          <Tooltip placement="top" title="Compra/Venda">
            C/V
          </Tooltip>
        ),
        dataIndex: 'compraVenda',
        width: '9',
        render: (compraVenda) => {
          let acao;
          let color;
          let tooltip;
          if (compraVenda === 'C') {
            acao = 'C';
            color = 'green';
            tooltip = 'Compra';
          } else {
            acao = 'V';
            color = 'red';
            tooltip = 'Venda';
          }
          return (
            <Tooltip placement="top" title={tooltip}>
              <Tag color={color}> {acao}</Tag>
            </Tooltip>
          );
        },
      },
      {
        title: 'Ativo',
        dataIndex: 'ticker',
        render: (ticker) => ticker.descricao,
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade',
      },
      {
        title: 'Preço',
        dataIndex: 'preco',
        render: (preco) =>
          preco.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }),
      },
      {
        title: 'Valor total',
        dataIndex: 'valorOperacao',
        render: (valorOperacao) =>
          valorOperacao.toLocaleString('pt-BR', {
            style: 'currency',
            currency: 'BRL',
          }),
      },
      {
        render: (operacao) => (
          <Tooltip placement="top" title="Editar">
            <Icon
              type="edit"
              onClick={async () => {
                const ativo = await consultarAtivo(operacao.ativo.id);
                operacao.ativo = ativo;
                this.props.adicionarOperacao(operacao);
                this.props.history.push('/main/operacao');
              }}
            />
          </Tooltip>
        ),
      },
      {
        render: (e) => (
          <Tooltip placement="top" title="Excluir">
            <Popconfirm
              title={`Deseja remover o item`}
              onConfirm={() => apagarOperacao(e.id)}
              onCancel={() => {}}
              okText="Ok"
              cancelText="Cancelar"
            >
              <Icon type="delete" />
            </Popconfirm>
          </Tooltip>
        ),
      },
    ];

    const { carteira } = this.props;
    const {
      renderCarrosel,
      pauseCarrosel,
      listCarteiras = [],
      loadingPage = false,
      dadosCarteiraAtual,
      renderModeCarossel,
      renderModalPrecoCusto = false,
      renderModalTabelaCarteiraAtual = false,
      possuiAcao = false,
      possuiTesouroDireto = false,
      possuiFundosImobiliarios = false,
      visibleEvolucaoDividendoModeTable = false,
    } = this.state;

    return (
      <React.Fragment>
        {!isMobile && (
          <React.Fragment>
            {this.renderModalRentabilidadeTrade()}
            {this.renderModalAporteMensal()}
          </React.Fragment>
        )}

        {loadingPage && <SpinCuston />}
        {!loadingPage && carteira && (
          <React.Fragment>
            <StyleRowSelectDelet>
              {listCarteiras && listCarteiras.length > 0 && (
                <SelectDelete
                  itens={listCarteiras}
                  value={carteira.name || ''}
                  labelFomr={'Carteira'}
                  defaultLabel={'Selecione uma carteira'}
                  labelSelected={'Carteira selecionada:'}
                  labelModalRemove={'Realmente deseja excluir este item?'}
                  extraSelecionadoSelect={this.extraSelecionadoSelect()}
                />
              )}
            </StyleRowSelectDelet>
            <Row gutter={16}>
              {carteira &&
                carteira.listaAporteMensal &&
                carteira.listaAporteMensal.length > 0 &&
                carteira.carteiraAtual &&
                Object.keys(carteira.carteiraAtual).length !== 0 &&
                dadosCarteiraAtual && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Rentabilidade da carteira"
                      modotable={'sim'}
                    >
                      <InformacoesCarteira
                        renderModeCarossel={renderModeCarossel}
                        data={dadosCarteiraAtual}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira.listaAporteMensal &&
                carteira.carteiraAtual &&
                Object.keys(carteira.carteiraAtual).length !== 0 &&
                dadosCarteiraAtual && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Grafico de desempenho"
                      extra={isMobile ? null : this.extraChartDesempenho()}
                      poussioverflow={'sim'}
                    >
                      <GraficoBar
                        renderModeCarossel={renderModeCarossel}
                        resetCarrosel={this.resetCarrosel}
                        data={dadosCarteiraAtual}
                        renderModalPrecoCusto={renderModalPrecoCusto}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira.carteiraAtual &&
                Object.keys(carteira.carteiraAtual).length !== 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard title="Carteira completa">
                      <Pie
                        data={carteira.carteiraAtual}
                        renderModeCarossel={renderModeCarossel}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira.carteiraAtual &&
                Object.keys(carteira.carteiraAtual).length !== 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard title="Carteira segmento">
                      <PieComposicaoCarteiraSegmentos
                        data={carteira.carteiraAtual}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira.composicaoCarteira &&
                carteira.composicaoCarteira.composicaoAtivos &&
                carteira.composicaoCarteira.composicaoAtivos.length > 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard title="Composição da carteira">
                      <PieComposicaoCarteira
                        renderModeCarossel={renderModeCarossel}
                        data={carteira.composicaoCarteira}
                      />
                    </StyleCard>
                  </Col>
                )}
              {possuiAcao && (possuiFundosImobiliarios || possuiTesouroDireto) && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard title="Composição das ações">
                    <Pie
                      data={carteira.listaDeAcoes}
                      renderModeCarossel={renderModeCarossel}
                    />
                  </StyleCard>
                </Col>
              )}
              {possuiFundosImobiliarios && (possuiAcao || possuiTesouroDireto) && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard title="Composição dos fundos imobiliários">
                    <Pie
                      data={carteira.listaDeFundosImobiliarios}
                      renderModeCarossel={renderModeCarossel}
                    />
                  </StyleCard>
                </Col>
              )}
              {possuiTesouroDireto && (possuiAcao || possuiFundosImobiliarios) && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard title="Composição dos titulos publicos">
                    <Pie
                      data={carteira.listaDeTesouro}
                      renderModeCarossel={renderModeCarossel}
                    />
                  </StyleCard>
                </Col>
              )}
              {carteira.carteiraAtual &&
                Object.keys(carteira.carteiraAtual).length !== 0 &&
                carteira.carteiraAtual.listaTrade && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Tabela da carteira completa"
                      extra={isMobile ? null : this.extraTabelaCarteiraAtual()}
                      modotable={'sim'}
                    >
                      <TableCateiraAtual
                        pageSize={8}
                        dataTable={carteira.carteiraAtual.listaTrade}
                        renderModalTabelaCarteiraAtual={
                          renderModalTabelaCarteiraAtual
                        }
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira && Object.keys(carteira).length !== 0 && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard
                    title="Histórico de compra e venda de ativos"
                    modotable={'sim'}
                  >
                    <Table
                      pagination={{ pageSize: 7 }}
                      columns={columns}
                      dataSource={this.getListOperations()}
                      size="small"
                      rowKey={(data) => `${data.data}${Math.random()}`}
                    />
                  </StyleCard>
                </Col>
              )}
              {carteira.listaAporteMensal && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard
                    title="Histórico aportes mensais"
                    extra={isMobile ? null : this.extraChartAporte()}
                  >
                    <GraficoBarAporteMensal data={carteira.listaAporteMensal} />
                  </StyleCard>
                </Col>
              )}
              {this.verificarSeExisteTradeFechado() && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard
                    title="Histórico resultado trades finalizados"
                    extra={isMobile ? null : this.extraChartHistoricoTrade()}
                  >
                    <GraficoBarHistorico data={carteira.listaDeTrades} />
                  </StyleCard>
                </Col>
              )}
              {carteira.listaDeDividendosReceber &&
                carteira.listaDeDividendosReceber.length > 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Dividendos a receber (R$ Bruto)"
                      modotable={'sim'}
                    >
                      <DividendosReceber
                        dataTable={carteira.listaDeDividendosReceber}
                      />
                    </StyleCard>
                  </Col>
                )}
              {this.verificarSeJaGanhouDividendos() && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard
                    title="Dividendos recebidos ou a receber (R$ Bruto)"
                    extra={isMobile ? null : this.extraChartAporte()}
                  >
                    <GraficoBarDividendosRecebidos
                      data={carteira.listaDeTrades}
                    />
                  </StyleCard>
                </Col>
              )}
              {carteira.listaDeDividendosReceberSeManter &&
                carteira.listaDeDividendosReceberSeManter.length > 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Dividendos a receber se manter (R$ Bruto)"
                      extra={isMobile ? null : this.extraChartAporte()}
                    >
                      <GraficoBarDividendosAReceber
                        data={carteira.listaDeDividendosReceberSeManter}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira &&
                carteira.listaDedistribuicaoDeCapitalAgrupado &&
                carteira.listaDedistribuicaoDeCapitalAgrupado.length !== 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard
                      title="Evolução dos dividendos (R$ Bruto)"
                      extra={this.extraModeAnualizado()}
                    >
                      <EvolucaoDividendo
                        data={carteira.listaDedistribuicaoDeCapitalAgrupado}
                        visibleEvolucaoDividendoModeTable={
                          visibleEvolucaoDividendoModeTable
                        }
                      />
                    </StyleCard>
                  </Col>
                )}
              {this.checkVisibleDividendOnCost() && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard title="Dividend yield/ dividend on cost">
                    <GraficoBarDividendoOnCost
                      data={carteira.carteiraAtual.listaTrade}
                    />
                  </StyleCard>
                </Col>
              )}
              {carteira &&
                carteira.listaNoticias &&
                carteira.listaNoticias.length > 0 && (
                  <Col sm={24} md={24} xl={12} xxl={12}>
                    <StyleCard title="Noticias" poussioverflow={'sim'}>
                      <StyleList
                        dataSource={carteira.listaNoticias}
                        renderItem={(item) => (
                          <ListItem
                            key={item.id}
                            style={{
                              background:
                                item.tipoDeNoticia &&
                                item.tipoDeNoticia.descricao ==
                                  TIPO_NOTICIAS.RESULTADO
                                  ? '#fbfca4'
                                  : 'white',
                            }}
                          >
                            <ListItemMeta
                              onClick={() => window.open(item.link, '_blank')}
                              title={`${item.ativo.nomePregao} - ${item.dataPublicacao}`}
                              description={item.conteudo}
                            />
                          </ListItem>
                        )}
                      />
                    </StyleCard>
                  </Col>
                )}
              {carteira && Object.keys(carteira).length !== 0 && (
                <Col sm={24} md={24} xl={12} xxl={12}>
                  <StyleCard title="Imposto de renda" poussioverflow={'sim'}>
                    <ImpostoDeRenda />
                  </StyleCard>
                </Col>
              )}
            </Row>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  carteira: state.carteira.carteira,
  coresAtivo: state.carteira.coresAtivo,
});
const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToPros)(MainComponent);
