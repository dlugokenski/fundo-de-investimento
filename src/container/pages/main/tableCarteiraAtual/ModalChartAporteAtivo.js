import React from 'react';
import { Bar } from 'react-chartjs-2';
import { Checkbox, Button, Modal, Table, Tag } from 'antd';
import styled from 'styled-components';

import { dataFormatado } from 'Util/Formatacao'

const StyleTable = styled(Table)`
  @media (max-width: 400px) {
    white-space: nowrap;

    td{
      padding: 2px 2px !important;
      padding-left: 10px !important;
    }
  } 
`;

const StyleCheckbox = styled(Checkbox)`
  margin-left: 0px !important;
`;

const StyleGroupCheckbox = styled.div`
  display: flex;
  flex-direction: column;
`;

const StyleSubGroupCheckbox = styled.div`
  position: relative;
  float: left;
  margin-left: ${props => props.modeLista ? '0' : '25px'};
  margin-bottom: 25px;
  display: inline-flex;
`;


export default class ModalChartAporteAtivo extends React.Component {

  state={
    modeSoma: true,
    modeLista: false,
    visiblePrecoMedio: false
  };

  componentDidMount(){

  }

  getDataChart(){
    const { data } = this.props;
    const { modeSoma, visiblePrecoMedio } = this.state;

    const periodos = [];
    const valores = [];
    const precoMedio = [];

    data.listaDeOperacao.forEach(element => {
      if(element.compraVenda === 'C'){
        periodos.push(element.data);
        valores.push(modeSoma ? element.valorOperacao : element.preco);
        precoMedio.push(element.precoMedioOperacao)
      }
    });

    const dataChart = {
      labels: periodos,
      datasets: []
    }

    if(!modeSoma){
      dataChart.datasets.push(
        {
          label: "Preço medio",
          type: "line",
          borderColor: "rgba(252, 186, 3)",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: precoMedio,
        }       
      )

      if(visiblePrecoMedio){
        periodos.push('Cotação atual')
        valores.push(0)
        const cotacaoAtual = precoMedio.map(() => 0)
        cotacaoAtual.push(data.ticker.cotacao.preco || 0);
        precoMedio.push(data.precoMedio);
  
        dataChart.datasets.push(
          {
            label: "Valor atual",
            type: "bar",
            barPercentage: '1',
            categoryPercentage: '1',
            backgroundColor: 'rgb(255, 123, 8)',
            borderColor: 'rgb(29, 41, 57)',
            borderWidth: '1',
            borderSkipped: 'bottom',
            hoverBackgroundColor: 'rgb(224, 108, 7)',
            hoverBorderColor: 'rgb(29, 41, 57)',
            hoverBorderWidth: '1.7',
            data: cotacaoAtual
          }
        )
      }
    }

    dataChart.datasets.push(
      {
        type: "bar",
        label: `Aportes ${data.ticker.descricao}`,
        barPercentage: '1',
        categoryPercentage: '1',
        backgroundColor: 'rgb(94, 169, 255)',
        borderColor: 'rgb(109, 156, 65)',
        borderWidth: '1',
        borderSkipped: 'bottom',
        hoverBackgroundColor: 'rgb(80, 144, 217)',
        hoverBorderColor: 'rgb(29, 41, 57)',
        hoverBorderWidth: '1.7',
        data: valores
      }
    )

    return dataChart;
  }


  render() {     
    const { data } = this.props;
    const { modeSoma, modeLista, visiblePrecoMedio } = this.state;

    const columns = [
      {
        title: 'Data operacao',
        dataIndex: 'data'
      },
      {
        title: 'Compra/Venda',
        dataIndex: 'compraVenda',
        render: compraVenda => {
          let acao;
          let color;
          if(compraVenda === "C") {
            acao = "Compra";
            color = "green";
          } else {
            acao = "Venda";
            color = "red";
          }
          return(
            <Tag color={color} > {acao }</Tag>
          )
        }   
      }, 
      {
        title: "Quantidade",
        dataIndex: 'quantidade',
      },
      {
        title: 'Preço',
        dataIndex: 'preco',
        render: preco => preco.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      },
      {
        title: 'Valor total',
        dataIndex: 'valorOperacao',
        render: valorOperacao => valorOperacao.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      }
    ];

    const options = {
      responsive: true,
      elements: {
        line: {
          tension: 0
        }
      },
      tooltips: {
        callbacks: {
            label: function(tooltipItem, dataChart) {
                const dataset = dataChart.datasets[tooltipItem.datasetIndex];
                const currentValue = dataset.data[tooltipItem.index]; 
                const valorConvertido = currentValue.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
                return `${dataset.label}: ${valorConvertido}`;
            },
        }
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [{
          gridLines: {
            display: false,
          }
        }],
        yAxes: [{
          ticks: {
            beginAtZero: true
          },
          gridLines: {
            color: "black",
            borderDash: [2, 5],
          },
          scaleLabel: {
            display: true,
            labelString: "R$",
            fontColor: "green"
          }
        }]
      }
    }

    return (
      <React.Fragment>
        <Modal
          zIndex={10000}
          width={900}
          style={{ top: '20px' }}
          visible={this.props.visible}
          onCancel={() => this.props.handleCancel()}
          title={`Aportes ${data.ticker.descricao}`}
          footer={[
            <Button key="back" onClick={() => this.props.handleCancel()}>
            {'Fechar'}
            </Button>
          ]}
        >
          <StyleSubGroupCheckbox {...this.state}>
            {!modeLista && (
              <StyleGroupCheckbox>
                <Checkbox 
                  checked={!modeSoma} 
                  onChange={e => this.setState({ modeSoma: !e.target.checked })}
                >
                  {'Valor unitario'}
                </Checkbox>  
                {!modeSoma && (
                  <StyleCheckbox 
                    checked={visiblePrecoMedio} 
                    onChange={e => this.setState({ visiblePrecoMedio: e.target.checked })}
                  >
                    {'Mostrar cotação atual'}
                  </StyleCheckbox>  
                )}
              </StyleGroupCheckbox>
            )}  
            <Checkbox 
              checked={modeLista} 
              onChange={e => this.setState({ modeLista: e.target.checked })}
            >
              {'Modo tabela'}
            </Checkbox>               
          </StyleSubGroupCheckbox>
          {modeLista ? (
            <StyleTable  
              columns={columns} 
              dataSource={data.listaDeOperacao} 
              size="small"
              rowKey={data => `${data.resultadoFinanceiro}${Math.random()}`}
            />
          ) : (
            <Bar
              data={this.getDataChart() || {}} 
              height={500}
              width={700}
              options={options} 
            />
          )}
          
        </Modal>
      </React.Fragment>
    );
  }
}