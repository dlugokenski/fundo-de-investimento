import React from 'react';
import { Modal, Button, Table } from 'antd';

export default props => (
  <React.Fragment>
    <Modal
        width={1500}
        style={{ top: '20px' }}
        visible={props.visible}
        onCancel={() => props.handleCancel()}
        title="Tabela Carteira atual"
        footer={[
            <Button key="back" onClick={() => props.handleCancel()}>
            {'Fechar'}
            </Button>
        ]}
    >
           <Table 
                pagination={{ pageSize: 12 }}
                style={{ marginTop: "20px" }}
                columns={props.columns} 
                dataSource={props.data} 
                size="small"
                rowKey={data => `${data.resultadoFinanceiro}${Math.random()}`}
            />
    </Modal>
  </React.Fragment>
)