import React from 'react';
import { Tooltip, Table, Button, Tag, Icon, Statistic } from 'antd';
import styled from 'styled-components';
import { isMobile } from 'react-device-detect';

import LabelMoney from 'Componente/LabelMoney';
import Modal from './Modal';
import ModalChartAporteAtivo from './ModalChartAporteAtivo'
import Title from 'Componente/Title';


const StylePorcentagem = styled.div`
  .ant-statistic-content-value-int{
    font-size: 17px;
  }  

  .ant-statistic-content-value-decimal{
    font-size: 13px;
  }

  .anticon {
    font-size: 17px;
  }
`;

const StyleTable = styled(Table)`
  @media (max-width: 400px) {
    white-space: nowrap;

    td{
      padding: 0px 0px !important;
      padding-left: 10px !important;
    }
  } 
`;

export default class TabelaCarteiraAtualComponent extends React.Component {
    
  state = {};
  
  componentDidUpdate(nextProps){
    if(nextProps !== this.props ){
        if(nextProps.renderModalTabelaCarteiraAtual != this.props.renderModalTabelaCarteiraAtual){
          this.setState({modalRender: true});
        }
    }         
}

  renderModal(columns) {
    const { modalRender = false } = this.state;
    const { dataTable } = this.props;
    if (modalRender) {
      return (
        <Modal
          visible={modalRender}
          columns={columns}
          data={dataTable}
          handleCancel={() => this.setState({ modalRender: false })}
        />
      );
    }
    return null;
  }

  renderModalAporte() {
    const { modalRenderAporte = false, itemChartAporte } = this.state;


    if (modalRenderAporte, itemChartAporte) {
      return (
        <ModalChartAporteAtivo
          visible={modalRenderAporte}
          data={itemChartAporte}
          handleCancel={() => this.setState({ modalRenderAporte: false })}
        />
      );
    }
    return null;
  }

  render(){ 
    const columnsCarteiraAtualModal = [
      {
        title: 'Ativo',
        dataIndex: 'ticker',
        render: ticker => ticker.descricao,
        width: '200px'
      },
      {
        title: 'Quantidade',
        dataIndex: 'quantidade'
      },
      {
        title: "Preço medio",
        dataIndex: 'precoMedio',
        render: precoMedio => precoMedio.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      },
      {
        title: "Cotação atual",
        dataIndex: 'ticker.cotacao.preco',
        render: cotacao => cotacao.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      },       
      {
        title: "Rentabilidade",
        dataIndex: 'rentabilidade',
        render: rentabilidade => (
          <StylePorcentagem>
            <Statistic
              value={rentabilidade}
              precision={2}
              valueStyle={{ color: rentabilidade > 0 ?  'green' : 'red'}}
              prefix={
                rentabilidade > 0 ? 
                  <Icon width="9" height="9" type="arrow-up" /> 
                : 
                  <Icon width="9" height="9" type="arrow-down" />
              }
              suffix="%"
            />
          </StylePorcentagem>
        )  
      },  
      {
        title: "Ganho/Perda",
        dataIndex: 'resultadoFinanceiro',
        render: resultadoFinanceiro => {
          return(
            <LabelMoney value={resultadoFinanceiro.toLocaleString('pt-BR', { currency: 'BRL' })} />
          )
        }   
      },    
      {
        title: 'Percentual da carteira',
        dataIndex: 'porcentagemNaCarteira',
        render: element => (
          <StylePorcentagem>
            <Statistic
              value={element}
              precision={2}
              suffix="%"
            />
          </StylePorcentagem>
        )
      },
      {
        title:  () => (
          <Tooltip placement="top" title="Valor atual investido">
          {'Valor Atual'}
          </Tooltip> 
        ),
        dataIndex: 'valorAtualInvestido',
        render: valorAtualInvestido => valorAtualInvestido.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      },
      {
        title:  "Aportes",
        align: 'center',
        render: item => (
          <Tooltip placement="top" title="Aportes">
            <Icon 
              width="9" 
              height="9" 
              type="bar-chart" 
              onClick={() => this.setState({modalRenderAporte: true, itemChartAporte: item})}
            /> 
          </Tooltip> 
        )
      },     
    ];

    const columnsCarteiraAtual = [
      {
      title: 'Ativo',
      dataIndex: 'ticker',
      render: ticker => ticker.descricao
      },
      {
      title: () => (
          <Tooltip placement="top" title="Quantidade">
          {'Quant.'}
          </Tooltip> 
      ),
      dataIndex: 'quantidade'
      },
      {
      title: () => (
          <Tooltip placement="top" title="Preço medio">
          {'P/M'}
          </Tooltip> 
      ),
      dataIndex: 'precoMedio',
      render: precoMedio => precoMedio.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      },   
      {
      title: () => (
          <Tooltip placement="top" title="Percentual da carteira">
          {'%'}
          </Tooltip> 
      ),
      dataIndex: 'porcentagemNaCarteira',
      render: element => element ? element.toFixed(2) : null
      },  
      {
      title:  () => (
        <Tooltip placement="top" title="Valor atual investido">
        {'Valor Atual'}
        </Tooltip> 
      ),
      dataIndex: 'valorAtualInvestido',
      render: valorAtualInvestido => valorAtualInvestido.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })
      }
  ];
    const { dataTable, pageSize = 6, title, renderModeCarossel } = this.props;
    return(
      <React.Fragment>
        {!isMobile && this.renderModal(columnsCarteiraAtualModal)}
        {!isMobile && this.renderModalAporte()}
        
        { renderModeCarossel && title && (
          <div style={{ marginTop: '10px' }} >
            <Title title="Tabela da carteira completa" />  
            <Button 
              type="primary" 
              icon="zoom-in"
              onClick={() => this.setState({ modalRender: true })} 
              style={{ float: 'right', marginRight: '10px', marginTop: '-25px', position: 'relative' }} 
              shape="circle" 
            />
          </div>
        )}
        <StyleTable  
          pagination={{ pageSize: pageSize }}
          columns={isMobile ? columnsCarteiraAtualModal : columnsCarteiraAtual} 
          dataSource={dataTable} 
          size="small"
          rowKey={data => `${data.resultadoFinanceiro}${Math.random()}`}
        />
      </React.Fragment>
    )
  }
}
