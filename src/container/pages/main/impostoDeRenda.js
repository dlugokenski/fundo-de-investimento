import React from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as actions } from "Ducks/carteira";
import { Table, Tooltip, Divider, Tag, Icon, notification } from "antd";
import FieldSet from "Componente/FieldSet";
import styled from "styled-components";
import { TIPO_ATIVOS } from "Util/Constantes";

import { buscarImpostoDeRenda } from "Provider/Carteira";

const DivHeader = styled.div`
  position: sticky;
  top: 0;
  z-index: 1;
  -webkit-box-shadow: 0px 9px 7px -2px #eeeeee;
  box-shadow: 0px 9px 7px -2px #eeeeee;
  height: 30px;
  background: #c9c9c9;
  color: black;
  margin-bottom: 10px;
  color: rgba(0, 0, 0, 0.85);
  font-weight: 500;
  font-size: 16px;
  white-space: nowrap;
  text-align: left;
  font-variant: tabular-nums;
  line-height: 1.5;
  list-style: none;
  padding-left: 10px;
`;

const DivCard = styled.div`
  border: 1px solid #eeeeee;
  -webkit-box-shadow: 0px 4px 9px 3px #eeeeee;
  box-shadow: 0px 4px 9px 3px #eeeeee;
  margin-bottom: 10px;

  &:hover {
    background: #f8f8f8;
  }

  :last-child {
    margin-bottom: 0;
  }

  .metade-div {
    width: 50%;
  }

  .div-copiar{
    display: flex;
    flex-direction: row !important;

    .div-button{
      margin-left: 10px;
    }
  }

  .div-description {
    padding-left: 20px;
    padding-right: 3px;
    padding-top: 3px;
    display: flex;
    flex-direction: row !important;
  }

  .div-button {
    display: flex;
    height: 100%;
    justify-content: center;

    &:hover {
      color: #2679de;
    }
  }

  .container-group {
    padding: 3px 20px;
    display: flex;
    flex-direction: row;

    &.border {
      padding-bottom: 15px;

      .border-right {
        border-right: 1px solid #eeeeee;
        margin: -20px 0px;
        padding: 20px 0px;
      }
    }

    .flex-row {
      display: flex;
      flex-direction: row;
    }

    div {
      display: flex;
      flex-direction: column;
    }

    .div-one {
      width: 50%;
    }

    .value {
      font-size: 13px;
    }

    .title {
      color: rgba(0, 0, 0, 0.85);
      font-weight: 450;
      font-size: 16px;
      white-space: nowrap;
      text-align: left;

      font-variant: tabular-nums;
      line-height: 1.5;
      list-style: none;
    }
  }
`;

class ImpostoDeRenda extends React.Component {
  state = {
    listImpostoDeRenda: [],
  };
  componentDidMount() {
    this.buscarImpostoDeRendaCarteira();
  }

  copyText(text, descricao) {
    navigator.clipboard.writeText(text);
    this.openNotification(
      "success",
      "Sucesso",
      `${descricao} copiado(a) com sucesso!`
    );
  }

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  componentDidUpdate(newProps) {
    if (
      newProps != this.props &&
      ((newProps.usuario != this.props.usuario &&
        newProps.usuario.id != this.props.usuario.id) ||
        newProps.carteira != this.props.carteira)
    ) {
      this.buscarImpostoDeRendaCarteira();
    }
  }

  async buscarImpostoDeRendaCarteira() {
    const { carteira } = this.props;
    if(carteira && carteira.id){
      const listImpostoDeRenda = await buscarImpostoDeRenda(carteira.id);
      const oneYearAgo = new Date().getFullYear() - 1;
      if(listImpostoDeRenda && listImpostoDeRenda.length > 0){
        const listImpostoDeRendaBeforeYear = listImpostoDeRenda.find(
          (e) => e.ano === oneYearAgo
        );
    
        if (listImpostoDeRendaBeforeYear) {
          this.setState({ listImpostoDeRenda: listImpostoDeRendaBeforeYear });
        }
      }  
    } 
  }

  getDescription(value) {
    let descriptionType = "";
    let nomeAtivo = "";
    if (value.ativo.tipo.descricao === TIPO_ATIVOS.ACOES) {
      descriptionType = "AÇÕES";
      nomeAtivo = value.ativo.razaoSocial.toUpperCase();
    } else if (value.ativo.tipo.descricao === TIPO_ATIVOS.FII) {
      nomeAtivo = value.ativo.nome.toUpperCase();
      descriptionType = "COTAS";
    }

    return `${value.quantidadeFinal} ${descriptionType} DE ${nomeAtivo} CNPJ: ${
        this.formatCNPJ(value.ativo.cnpj)
    }. CORRETORA BANCO INTER.`;
  }

  formatCNPJ = (value) => {
    const cnpj = value.replace(/\D/g, ''); // Remove all non-digit characters
    const formattedCNPJ = cnpj.replace(
      /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/,
      '$1.$2.$3/$4-$5'
    );

    return formattedCNPJ;
  };

  copiarValorDinheiro = (valor) => {
    let formattedValue =  valor.toLocaleString("pt-BR", {
      style: "currency",
      currency: "BRL",
    })
    formattedValue = formattedValue.replace("R$", "");
    formattedValue = formattedValue.replaceAll(" ", "");
    return formattedValue;
  }

  render() {
    const { listImpostoDeRenda } = this.state;

    return (
      <React.Fragment>
        <DivHeader>Aba bens e direitos</DivHeader>

        {listImpostoDeRenda &&
          listImpostoDeRenda.listImpostoDeRendaAtivo &&
          listImpostoDeRenda.listImpostoDeRendaAtivo.map((e, index) => (
            <DivCard key={index}>
              <FieldSet text="Ativo" />
              <div className="container-group">
                <div className="metade-div flex-row">
                  <div className="metade-div">
                    <div className="div-copiar">
                      <span className="title">Ticket</span>
                      <div className="div-button">
                        <Tooltip placement="top" title="Copiar ticker">
                          <Icon
                            type="copy"
                            fill="default"
                            onClick={() => this.copyText(e.ticker.descricao, "Ticker")}
                          />
                        </Tooltip>
                      </div>
                    </div>
                    <span>
                      <Tag color="green"> {e.ticker.descricao}</Tag>
                    </span>                     
                  </div>
                  <div className="metade-div">
                    <div className="div-copiar">
                      <span className="title">CNPJ</span>
                      <div className="div-button">
                        <Tooltip placement="top" title="Copiar CNPJ">
                          <Icon
                            type="copy"
                            fill="default"
                            onClick={() => this.copyText(this.formatCNPJ(e.ativo.cnpj), "CNPJ")}
                          />
                        </Tooltip>
                      </div>
                    </div>
                    <span className="value">{this.formatCNPJ(e.ativo.cnpj)}</span>
                  </div>
                </div>
                <div>
                  <div>
                    <span className="title">{e.ativo.tipo.descricao === TIPO_ATIVOS.ACOES ? "Razão social" : "Nome"}</span>
                    {e.ativo.tipo.descricao === TIPO_ATIVOS.ACOES && (
                      <span className="value">
                        {e.ativo.razaoSocial && e.ativo.razaoSocial.toUpperCase()}
                      </span>
                    )}
                     {e.ativo.tipo.descricao === TIPO_ATIVOS.FII && (
                      <span className="value">
                        {e.ativo.nome && e.ativo.nome.toUpperCase()}
                      </span>
                    )}
                  </div>
                </div>
              </div>
              <FieldSet text="Valores" />
              <div className="container-group border">
                <div className="metade-div flex-row border-right">
                  <div className="metade-div">
                    <div className="metade-div">
                    <div className="div-copiar">
                      <span className="title">Patrimonio inicial</span>
                      <div className="div-button">
                        <Tooltip placement="top" title="Copiar patrimonio inicial">
                          <Icon
                            type="copy"
                            fill="default"
                            onClick={() => this.copyText(this.copiarValorDinheiro(e.valorInitialAportado), "Patrimonio inicial")}
                          />
                        </Tooltip>
                      </div>
                    </div>
                      <span className="value">
                        {e.valorInitialAportado.toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })}
                      </span>
                    </div>
                    <div className="metade-div">
                      <span className="title">Quantidade inicial</span>
                      <span className="value">{e.quantidadeInitial}</span>
                    </div>
                  </div>
                  <div className="metade-div">
                    <div className="metade-div">
                    <div className="div-copiar">
                      <span className="title">Patrimonio final</span>
                      <div className="div-button">
                        <Tooltip placement="top" title="Copiar patrimonio final">
                          <Icon
                            type="copy"
                            fill="default"
                            onClick={() => this.copyText(this.copiarValorDinheiro(e.valorFinalAportado), "Patrimonio final")}
                          />
                        </Tooltip>
                      </div>
                    </div>
                      <span className="title"></span>
                      <span className="value">
                        {e.valorFinalAportado.toLocaleString("pt-BR", {
                          style: "currency",
                          currency: "BRL",
                        })}
                      </span>
                    </div>
                    <div className="metade-div">
                      <span className="title">Quantidade final</span>
                      <span className="value">{e.quantidadeFinal}</span>
                    </div>
                  </div>
                </div>
                <div className="div-description metade-div">
                  <div className="description">
                    <span className="title">Descrição</span>
                    <span>{this.getDescription(e)}</span>
                  </div>
                  <div className="div-button">
                    <Tooltip placement="top" title="Copiar descrição">
                      <Icon
                        type="copy"
                        fill="default"
                        onClick={() => this.copyText(this.getDescription(e), "Descrição")}
                      />
                    </Tooltip>
                  </div>
                </div>
              </div>
            </DivCard>
          ))}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  carteira: state.carteira.carteira,
  coresAtivo: state.carteira.coresAtivo,
});
const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToPros)(ImpostoDeRenda);
