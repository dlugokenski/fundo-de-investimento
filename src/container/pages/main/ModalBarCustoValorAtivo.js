import React from 'react';
import { Button, Modal, Checkbox  } from 'antd';
import { Bar } from 'react-chartjs-2';

export default props => (
  <React.Fragment>
      <Modal
          width={1000}
          style={{ top: '20px' }}
          visible={props.visible}
          onCancel={() => props.handleCancel()}
          title="Custo de aquisição e valores atuais dos ativos"
          footer={[
              <Button key="back" onClick={() => props.handleCancel()}>
              {'Fechar'}
              </Button>
          ]}
      >
          <div style={{ position: 'relative', float: 'left', marginLeft: "25px" }}>
            <Checkbox checked={props.checkAtive} onChange={e => props.onChangeCheckBox(e)}>{'Ocultar consolidado'}</Checkbox>           
          </div>
          <Bar
            data={props.data} 
            height={500}
            width={700}
            options={props.options} 
            redraw
          />
      </Modal>
    </React.Fragment>
)
