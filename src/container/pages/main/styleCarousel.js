
import styled from 'styled-components';

const StyledCard = styled.div`
 .ant-carousel {
  width: 460px;
  margin: 0 auto;

  .slick-slider {
    .slick-arrow {
      top: 40%;
      color: #999999;
      z-index: 1;

      .anticon {
        font-size: 20px;
      }
    }
  }


  .slick-slide {
    text-align: center;

    > div {
      overflow: auto;
      transform: scale(0.9);
      opacity: 0.7;
      transition: all 300ms ease;
    }

    &.slick-center {
      &:hover {
        .slide-overlay {
          left: 0;
        }
      }

      > div {
        opacity: 1;
        transform: scale(1);
      }
    }
  }

  .slick-no-slide .slick-slide {

    > div {
      display: inline-block;
      float: none;
      opacity: 1;
      transform: scale(1);
    }

    &:hover {
        .slide-overlay {
          left: 0;
        }
      }

      > div {
        transform: scale(1);
      }
  }

  .slide-overlay {
    display: flex;
    align-content: center;
    position: absolute;
    width: 150px;
    height: 112px;
    top: 0;
    left: -150px;
    background: rgba(0,0,0,0.75);
    transition: all 300ms ease;
  }

  .slide-actions {
    display: flex;
    align-items: center;
    width: 80px;
    height: 40px;
    margin: auto;

    > button {
      color: #fff;
      background: transparent;
      border: 0;
      font-size: 18px;
      transition: all 300ms ease-in;

      &:hover {
        transform: scale(1.2);
      }
    }

    > span {
      display: inline-block;
      border-right: 1px solid #fff;
      height: 40px;
      margin: 0 5px;
    }
  }

  .slick-dots {

    li {
      button {
        display: none;
      }

      &.slick-active {
        button {
          display: none;
        }
      }
    }
  }
}
`;

export default StyledCard;
