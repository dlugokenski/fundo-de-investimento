import React from 'react';
import { Button, Col, Row, Form, Input, Icon, notification } from 'antd';
import { Route, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import imagenDiv from 'Imagens/grande-logo.png';
import imagenLogo from 'Imagens/mini-logo.png';
import { login } from 'Provider/Login';
import { cadastrar } from 'Provider/Usuario';

const FormItem = Form.Item;
const ImputPassword = Input.Password;

const StyledLayout = styled(Row)`
  height: 100vh;
`;

const ButtonSubmit = styled(Button)`
  font-size: 14px;
  margin-top: 16px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;

const SiderLogin = styled(Col)`
  background-color: #e4e4e4;

  > div {
    position: relative;
    display: flex;
    height: 100vh;
    align-items: center;
    justify-content: center;
  }

  .public-access {
    background-color: #f2f2f2;

    h3 {
      font-size: 1.25em;
      margin: 0;

      .anticon {
        font-size: 24px;
        vertical-align: -0.25em;
        margin-right: 12px;
      }
    }

    > div > span {
      display: inline-block;
      margin: 24px 45px;
    }

    > button {
      color: #a6a6a6;

      @media (min-width: 992px) {
        display: none;
      }
    }
  }

  .box-access {
    width: 90%;
    text-align: center;

    input.ant-input-lg {
      padding-left: 40px;
      font-size: 14px;
    }

    @media (min-width: 768px) {
      width: 360px;
    }
  }
`;

const StyleBackground = styled(Col)`
  background-image: url(${imagenDiv});
  background-repeat: no-repeat;
  background-position: center;
  background-position: center; /* center the image */
  background-size: cover;
`;

const StyleTitleNovoCadastro = styled.div`
  font-size: 14px;
  margin-bottom: 10px;
  text-align: initial;
  margin-top: -10px;
  font-weight: 500;
`;

const BackgroundImage = styled(Col)`
  display: none;
  background: url(${imagenDiv}) center center;
  background-size: cover;

  @media (min-width: 992px) {
    display: block;
  }
`;

const StylCadastroAndNovaSenha = styled.div`
  display: flex;
  flex-direction: column;
  text-align: initial;
  margin-bottom: -10px;

  a {
    font-size: 10px;
    max-width: max-content;
    padding: 3px 0px;
  }
`;

const StylButtonsCadastro = styled.div`
  display: flex;
  flex-direction: column;
  text-align: initial;
  margin-bottom: -10px;

  a {
    font-size: 10px;
    max-width: max-content;
    padding: 3px 0px;
  }
`;

class Login extends React.Component {
  state = {
    modeLogin: true,
  };

  componentDidMount() {
    if (sessionStorage.getItem('tokenHash')) {
      this.setState({ mudarRota: true });
    }
  }

  renderRouter = () => {
    const { rota = '/main/initial-application' } = this.state;
    return <Route render={() => <Redirect to={rota} />} />;
  };

  handleSubmitLogin = () => {
    this.setState({ loading: true });
    const { validateFields } = this.props.form;
    validateFields(async (err, values) => {
      if (!err) {
        const json = await login({
          userName: values.login,
          password: values.senha,
        });
        if (json && !json.error) {
          this.validarLogin(json);
        } else {
          this.openNotification('error', 'Alerta', 'Falha na autenticação.');
        }
      }
    });
    this.setState({ loading: false });
  };

  handleSubmitCadastro = () => {
    const { validateFields } = this.props.form;
    this.setState({ loading: true });
    validateFields(async (err, values) => {
      if (!err) {
        const nome = values.nome;
        const sobrenome = values.sobrenome;
        const userName = values.email;
        const senhaOne = values.senhaOne;
        const senhaTwo = values.senhaTwo;

        if (senhaOne !== senhaTwo) {
          this.openNotification('warning', 'Alerta', 'Senhas incopatíveis.');
        } else {
          const parans = {
            nome,
            sobrenome,
            userName,
            password: senhaOne,
          };

          try {
            const json = await cadastrar(parans);
            this.openNotification(
              'success',
              'Alerta',
              'Por favor, verifique sua caixa de e-email'
            );
          } catch (error) {
            this.openNotification(
              'error',
              'Alerta',
              error.message ? error.message : 'Ocorreu um erro!'
            );
          }
        }
      }
    });
    this.setState({ loading: false });
  };

  openNotification = (type = 'warning', title = '', description = '') => {
    notification[type]({
      message: title,
      description: description,
    });
  };

  async validarLogin(token) {
    if (typeof token !== 'object') {
      sessionStorage.setItem('tokenHash', token);
      this.setState({ showModalLogar: false, mudarRota: true });
      this.renderRouter();
    } else {
      this.openNotification('error', 'Alerta', 'Falha na autenticação.');
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const { mudarRota = false, modeLogin, loading = false } = this.state;

    return (
      <StyledLayout type="flex">
        <BackgroundImage md={12} lg={12} xl={12} />
        <SiderLogin md={12} lg={12} xl={12}>
          <div className="retricted-access">
            <div className="box-access">
              <Form>
                <img
                  alt="Meus investimentos"
                  src={imagenLogo}
                  style={{ padding: '0 0 30px', width: '100%' }}
                />
                {modeLogin ? (
                  <React.Fragment>
                    <FormItem>
                      {getFieldDecorator('login', {
                        rules: [
                          {
                            required: true,
                            message: 'Campo usuário é obrigatório!',
                          },
                        ],
                      })(
                        <Input
                          size="large"
                          prefix={
                            <Icon
                              type="user"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                          placeholder="Digite seu e-mail"
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('senha', {
                        rules: [
                          {
                            required: true,
                            message: 'Campo senha é obrigatório!',
                          },
                        ],
                      })(
                        <ImputPassword
                          size="large"
                          prefix={
                            <Icon
                              type="lock"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                          placeholder="Digite sua senha"
                        />
                      )}
                    </FormItem>
                    <StylCadastroAndNovaSenha>
                      <a
                        onClick={() => this.setState({ modeLogin: !modeLogin })}
                        href="#"
                      >
                        Cadastrar-se
                      </a>
                      <a href="">Esqueci minha senha</a>
                    </StylCadastroAndNovaSenha>
                    <ButtonSubmit
                      loading={loading}
                      onClick={this.handleSubmitLogin}
                      type="primary"
                      size="large"
                      block
                      style={{ backgroundColor: '#004191' }}
                    >
                      Entrar
                    </ButtonSubmit>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <StyleTitleNovoCadastro>
                      Novo cadastro
                    </StyleTitleNovoCadastro>
                    <FormItem>
                      {getFieldDecorator('nome', {
                        rules: [
                          { required: true, message: 'Campo obrigatorio' },
                        ],
                      })(
                        <Input
                          size="large"
                          placeholder="Nome"
                          prefix={
                            <Icon
                              type="user"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('sobrenome', {
                        rules: [
                          { required: true, message: 'Campo obrigatorio' },
                        ],
                      })(
                        <Input
                          size="large"
                          placeholder="Sobrenome"
                          prefix={
                            <Icon
                              type="user"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('email', {
                        rules: [
                          { required: true, message: 'Campo obrigatorio' },
                        ],
                      })(
                        <Input
                          size="large"
                          placeholder="E-mail"
                          prefix={
                            <Icon
                              type="mail"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('senhaOne', {
                        rules: [
                          { required: true, message: 'Campo obrigatorio' },
                        ],
                      })(
                        <ImputPassword
                          size="large"
                          prefix={
                            <Icon
                              type="lock"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                          placeholder="Digite sua senha"
                        />
                      )}
                    </FormItem>
                    <FormItem>
                      {getFieldDecorator('senhaTwo', {
                        rules: [
                          { required: true, message: 'Campo obrigatorio' },
                        ],
                      })(
                        <ImputPassword
                          size="large"
                          prefix={
                            <Icon
                              type="lock"
                              style={{ color: 'rgba(0,0,0,.25)', fontSize: 16 }}
                            />
                          }
                          placeholder="Repita sua senha"
                        />
                      )}
                    </FormItem>
                    <StylCadastroAndNovaSenha>
                      <a
                        onClick={() => this.setState({ modeLogin: !modeLogin })}
                        href="#"
                      >
                        Já possuo cadastro!
                      </a>
                    </StylCadastroAndNovaSenha>
                    <ButtonSubmit
                      type="primary"
                      size="large"
                      block
                      style={{ backgroundColor: '#004191' }}
                      loading={loading}
                      onClick={() => this.handleSubmitCadastro()}
                    >
                      {'Confirmar'}
                    </ButtonSubmit>
                  </React.Fragment>
                )}
              </Form>
            </div>
          </div>
        </SiderLogin>
        {mudarRota && this.renderRouter()}
      </StyledLayout>
    );
  }
}

export default Form.create()(Login);
