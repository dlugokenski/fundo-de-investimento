import React from "react";
import { Table, notification, Modal } from "antd";

import Drawer from "Componente/Drawer";
import OperacaoCadastro from "Pages/operacao/cadastrar";
import { lerPdfOperacao } from "Provider/Operacao";
import editTable from "Componente/EditableTable/TableEditHOC";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as actions } from "Ducks/carteira";

const { confirm } = Modal;

class OperacaoComponente extends React.Component {
  state = {
    visible: true,
    close: false,
    pageAtiva: "cadastrar",
  };

  componentDidMount() {}

  componentDidUpdate(nextProps) {
    if (nextProps !== this.props) {
      if (nextProps.reset != this.props.reset) {
        this.setState({ visible: true });
      }

      if (nextProps.operacao != this.props.operacao && this.props.operacao.id) {
        this.setState({ pageAtiva: "editar" });
      }
    }
  }

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  setarAtivo = (value) => {
    const { operacao } = this.props;

    if (Object.keys(operacao.ativo).length === 0) {
      this.setState({ pageAtiva: "cadastrar" });
    }

    this.props.adicionarOperacao({
      ...operacao,
      ativo: value,
    });
  };

  getTitlePage() {
    const { pageAtiva } = this.state;

    if (pageAtiva === "cadastrar") {
      return "Cadastrar operacao";
    } else if (pageAtiva === "editar") {
      return "Editar operacao";
    }
  }

  async uploadFile(file) {
    const formData = new FormData();
    formData.append("uploadedFile", file);
    const dataModal = await lerPdfOperacao(formData);
    if (dataModal.message) {
      this.openNotification("error", "Alerta", dataModal.message);
    } else {
      this.setState({ modalRender: true, dataModal: dataModal.listOperacao });
    }
  }

  geButtons() {
    const { pageAtiva } = this.state;
    const { configuracaoAplication = [] } = this.props;

    const enabledReadingOfOperationsByPdf = configuracaoAplication.find(
      (element) => element.tag === "enabled_reading_of_operations_by_pdf"
    );
    const buttons = [
      {
        icon: "plus",
        label: "Novo",
        onClick: () => this.setarAtivo({}),
        disabled: pageAtiva === "cadastrar" ? true : false,
      },
      {
        icon: "delete",
        label: "Deletar",
        onClick: () => this.showConfirm(this.removerAtivo),
        disabled: pageAtiva === "editar" ? false : true,
      },
    ];

    if (
      enabledReadingOfOperationsByPdf &&
      enabledReadingOfOperationsByPdf.ativo
    ) {
      buttons.push({
        icon: "file-pdf",
        label: "Ler pdf",
        type: "file",
        onClick: (e) => this.uploadFile(e),
        disabled: pageAtiva === "cadastrar" ? false : true,
      });
    }
    return buttons;
  }

  render() {
    const {
      visible,
      close,
      pageAtiva,
      modalRender = false,
      dataModal = [],
    } = this.state;
    const { operacao = {} } = this.props;

    return (
      <React.Fragment>
        {modalRender ? (
          <Modal
            visible={modalRender}
            handleCancel={() => this.setState({ modalRender: false })}
          >
            {editTable({
              handleSave: (documento) => console.log(documento),
              handleDelete: (regra) => console.log(regra),
            })(
              <Table
                rowKey={(record) => record.id}
                pagination={false}
                scroll={{ y: 240 }}
                size="small"
                columns={[
                  {
                    title: "Código",
                    width: 80,
                    editable: true,
                    dataIndex: "codigo",
                  },
                  {
                    title: "Descrição",
                    width: 150,
                    editable: true,
                    dataIndex: "descricao",
                  },
                  {
                    title: "Acesso",
                    width: 110,
                    editable: true,
                    dataIndex: "acesso",
                  },
                ]}
                dataSource={dataModal}
              />
            )}
          </Modal>
        ) : (
          <Drawer
            buttons={this.geButtons()}
            title={this.getTitlePage()}
            visible={visible}
            width={350}
            close={close}
            onVisible={(o) => this.setState({ visible: o })}
            onClose={(o) => this.setState({ close: o })}
          >
            <OperacaoCadastro
              setarAtivo={this.setarAtivo}
              pageAtiva={pageAtiva}
              operacao={operacao}
            />
          </Drawer>
        )}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  operacao: state.carteira.operacao,
  configuracaoAplication: state.configuracaoAplication.configuracoes,
});
const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToPros)(OperacaoComponente);
