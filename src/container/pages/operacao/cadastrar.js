import React from "react";
import {
  Form,
  Input,
  Select,
  Button,
  notification,
  DatePicker,
  AutoComplete,
} from "antd";
import moment from "moment";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Creators as actions } from "Ducks/carteira";

import { converterDinheiroParaFloat } from "Util/Formatacao";
import { InputMoneyForm } from "Componente/InputMoney";
import { InputNumberForm } from "Componente/InputNumber";

import { cadastrarOperacao } from "Provider/Operacao";
import { seachAtivo } from "Provider/Ativo";
import { buscarListaDeCarteiras } from "Provider/Carteira";
import initCarteiraController from "Pages/carteiraAtual/CarteiraAtualController";
import { buscarCarteiraFavoritaOuUltimaCadastrada } from "Provider/Carteira";
import { TIPO_ATIVOS } from "Util/Constantes";

const FormItem = Form.Item;
const Option = Select.Option;
const { Search } = Input;
const OptionAutoComplete = AutoComplete.Option;

class CadastrarOperacaoComponente extends React.Component {
  state = {};

  componentDidMount() {
    this.initSeletecCarteira();
    if (this.props.operacao.ativo.id) {
      this.resetForm();
      this.setValuesForm();
    }
  }

  async componentDidUpdate(nextProps) {
    if (nextProps !== this.props) {
      if (
        (nextProps.carteira &&
          nextProps.carteira.carteira &&
          nextProps.carteira.carteira.data &&
          nextProps.carteira.carteira.id) !=
        (this.props.carteira &&
          this.props.carteira.carteira &&
          this.props.carteira.carteira.data &&
          this.props.carteira.carteira.id)
      ) {
        this.setCarteira();
      }
      if (
        nextProps.operacao != this.props.operacao &&
        this.props.operacao.id &&
        this.props.operacao.id != nextProps.operacao.id
      ) {
        this.resetForm();
        this.setValuesForm();
      }

      if (
        this.props.operacao &&
        nextProps.operacao &&
        nextProps.operacao.ativo &&
        this.props.operacao.ativo &&
        this.props.operacao.ativo != nextProps.operacao.ativo &&
        this.props.operacao.ativo.id
      ) {
        this.resetForm();
        this.setValuesForm();
      }
    }
  }

  setCarteira() {
    const {
      form: { setFieldsValue },
      carteira,
    } = this.props;

    setFieldsValue({
      carteira: carteira.carteira.id,
    });
  }

  async initSeletecCarteira() {
    const { usuario } = this.props;
    const listCarteiras = await buscarListaDeCarteiras(usuario.id);
    const locale = window.navigator.userLanguage || window.navigator.language;
    this.setState({ listCarteiras, locale: locale });
    this.setCarteira();
  }

  resetForm() {
    const {
      form: { resetFields },
    } = this.props;
    resetFields();
    this.setCarteira();
  }

  setValuesForm() {
    const {
      form: { setFieldsValue },
      operacao = {},
    } = this.props;
    setFieldsValue({
      pesquisa: operacao.ativo.ticker,
      empresa: operacao.ativo.nomePregao
        ? operacao.ativo.nomePregao
        : operacao.ativo.nome,
      ticker: operacao.ticker ? operacao.ticker.id : undefined,
      quantidade: operacao.quantidade,
      valor: operacao.preco,
      compra: operacao.compraVenda,
    });
  }

  setDefaultValues() {
    const {
      form: { setFieldsValue },
    } = this.props;
    setFieldsValue({
      empresa: undefined,
      ticker: undefined,
      ativo: undefined,
      compraVenda: undefined,
      data: undefined,
    });
  }

  montedOptionsTipoAtivo = () => {
    const { tipoAtivo } = this.state;

    if (tipoAtivo) {
      return tipoAtivo.map((element, index) => (
        <Option key={index} value={element.id}>
          {element.descricao}
        </Option>
      ));
    }
  };

  montedOptionsCarteiras = () => {
    const { listCarteiras } = this.state;

    if (listCarteiras) {
      return listCarteiras.map((element, index) => (
        <Option key={index} value={element.id}>
          {element.name}
        </Option>
      ));
    }
  };

  openNotification(type, title, description) {
    notification[type || "info"]({
      message: title || "Aviso",
      description: description,
    });
  }

  handleSubmit = () => {
    const {
      form: { validateFields },
      pageAtiva,
      operacao = {},
      usuario,
    } = this.props;
    this.setState({ loading: true, listAtivosAutoComplete: [] });
    validateFields(async (err, values) => {
      if (!err) {
        const valor = converterDinheiroParaFloat(
          values.valor ? values.valor : 0
        );
        const quantidade = values.quantidade;
        const carteira = values.carteira;
        const compraVenda = values.compra;
        const dataForm = values.data;
        const ticker = values.ticker;

        const data = await cadastrarOperacao({
          id: parseInt(operacao.id) || 0,
          preco: valor,
          quantidade: quantidade,
          ativo: {
            id: parseInt(operacao.ativo.id),
          },
          ticker: {
            id: ticker,
          },
          compraVenda: compraVenda,
          data: dataForm,
          carteira: {
            id: carteira,
          },
          createAt:
            operacao && operacao.createAt ? operacao.createAt : new Date(),
        });

        if (data) {
          if (data.id && usuario && usuario.id) {
            const dataCarteira = await buscarCarteiraFavoritaOuUltimaCadastrada();
            initCarteiraController(dataCarteira);
            this.openNotification(
              "success",
              "Sucesso",
              pageAtiva === "editar"
                ? "Operação editada com sucesso."
                : `Operacao cadastrada com sucesso.`
            );
            this.resetForm();
          } else if (data.message) {
            this.openNotification("error", "Alerta", data.message);
            this.resetForm();
          }
        } else {
          this.openNotification(
            "error",
            "Alerta",
            "Ocorreu um erro ao cadastrar a operação!"
          );
          this.resetForm();
        }
      }
    });
    this.setState({ loading: false });
  };

  async buscarAtivo(value) {
    this.setState({ loading: true, listAtivosAutoComplete: [] });
    this.props.setarAtivo({});
    const listAtivos = await seachAtivo(value);
    this.setState({ loading: false, listAtivosAutoComplete: listAtivos });
  }

  selectAtivo = async (value) => {
    const {
      form: { setFieldsValue },
    } = this.props;
    const { listAtivosAutoComplete = [] } = this.state;
    const object = listAtivosAutoComplete.find(
      (element) => element.id == value
    );
    await this.props.setarAtivo(object);

    setFieldsValue({
      ticker: undefined,
    });
  };

  renderTextOptionAtivos(element) {
    if (element.tipo.descricao === TIPO_ATIVOS.ACOES) {
      const listTickers = [];
      element.listaDeTicker.forEach((element) => {
        listTickers.push(element.descricao);
      });
      return `${element.nomePregao} - ${listTickers.join(", ")}`;
    } else if (
      element.tipo.descricao === TIPO_ATIVOS.TESOURO_DIRETO ||
      element.tipo.descricao === TIPO_ATIVOS.FII
    ) {
      return element.listaDeTicker[0].descricao;
    }
  }

  renderOptionAtivos = () => {
    const { listAtivosAutoComplete = [] } = this.state;
    return listAtivosAutoComplete.map((element) => (
      <OptionAutoComplete key={element.id}>
        {this.renderTextOptionAtivos(element)}
      </OptionAutoComplete>
    ));
  };

  render() {
    const {
      loading = false,
      tipoAtivo = [],
      listCarteiras = [],
      quantidade = 0,
      locale,
    } = this.state;
    const { operacao = {} } = this.props;

    const { getFieldDecorator } = this.props.form;
    const message = "Campo obrigatorio";
    const { pageAtiva } = this.props;

    return (
      <Form>
        <div className="content-form">
          <FormItem label="Nome empresa/ticker:" name="pesquisa">
            {getFieldDecorator("pesquisa", {
              rules: [{ required: false }],
            })(
              <AutoComplete
                style={{ width: "100%" }}
                dataSource={this.renderOptionAtivos()}
                onSelect={(value) => this.selectAtivo(value)}
                onSearch={(value) => this.buscarAtivo(value)}
              >
                <Search
                  onSearch={(value) => this.buscarAtivo(value)}
                  placeholder="Nome empresa/ticker"
                  enterButton
                />
              </AutoComplete>
            )}
          </FormItem>
          <FormItem label="Carteira:">
            {getFieldDecorator("carteira", {
              rules: [{ required: true, message }],
            })(
              <Select disabled defaultActiveFirstOption={true}>
                {this.montedOptionsCarteiras()}
              </Select>
            )}
          </FormItem>
          {operacao &&
            operacao.ativo &&
            operacao.ativo.id &&
            operacao.ativo.tipo.descricao !== TIPO_ATIVOS.TESOURO_DIRETO && (
              <FormItem label="Nome empresa:">
                {getFieldDecorator("empresa", {
                  rules: [{ required: pageAtiva === "editar", message }],
                })(<Input disabled={true} />)}
              </FormItem>
            )}
          {operacao && operacao.ativo && operacao.ativo.id && (
            <FormItem label="Codigo cotação:">
              {getFieldDecorator("ticker", {
                rules: [{ required: true, message }],
              })(
                <Select
                  disabled={
                    (!operacao && !operacao.ativo) ||
                    Object.keys(operacao.ativo).length === 0
                  }
                  placeholder="Selecione um ticker..."
                >
                  {operacao.ativo.listaDeTicker.map((ticker, index) => (
                    <Option key={index} value={ticker.id}>
                      {ticker.descricao}
                    </Option>
                  ))}
                </Select>
              )}
            </FormItem>
          )}
          <FormItem label="Data da operação:">
            {getFieldDecorator("data", {
              rules: [{ required: true, message }],
              initialValue: moment(),
            })(
              <DatePicker
                locale={locale}
                format="DD/MM/YYYY"
                placeholder="Selecione uma data"
                style={{ width: "100%" }}
                disabled={
                  (!operacao && !operacao.ativo) ||
                  Object.keys(operacao.ativo).length === 0
                }
              />
            )}
          </FormItem>
          <FormItem label="Quantidade/Papeis:">
            {getFieldDecorator("quantidade", {
              rules: [{ required: true, message }],
              initialValue: 0,
            })(
              <InputNumberForm
                ref={(input) => input}
                disabled={
                  (!operacao && !operacao.ativo) ||
                  Object.keys(operacao.ativo).length === 0
                }
              />
            )}
          </FormItem>
          <FormItem label="Valor unitario cota:">
            {getFieldDecorator("valor", {
              rules: [{ required: false, message }],
            })(
              <InputMoneyForm
                disabled={
                  (!operacao && !operacao.ativo) ||
                  Object.keys(operacao.ativo).length === 0
                }
              />
            )}
          </FormItem>
          <FormItem label="Compra/Venda:">
            {getFieldDecorator("compra", {
              rules: [{ required: true, message }],
            })(
              <Select
                disabled={
                  (!operacao && !operacao.ativo) ||
                  Object.keys(operacao.ativo).length === 0
                }
                placeholder="Compra/Venda"
              >
                <Option key={1} value={"C"}>
                  Compra
                </Option>
                <Option key={2} value={"V"}>
                  Venda
                </Option>
              </Select>
            )}
          </FormItem>
        </div>
        <FormItem>
          <Button
            type="primary"
            loading={loading}
            disabled={
              (!operacao && !operacao.ativo) ||
              Object.keys(operacao.ativo).length === 0
            }
            htmlType="submit"
            onClick={() => this.handleSubmit()}
            className="button-submit"
          >
            {operacao && Object.keys(operacao).length !== 0
              ? "Cadastrar"
              : "Editar"}
          </Button>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = (state) => ({
  usuario: state.configuracaoUsuario.usuario,
  carteira: state.carteira,
});
const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToPros
)(Form.create()(CadastrarOperacaoComponente));
