import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const isAuthenticated = () => !!sessionStorage.getItem('tokenHash');

const PrivateRoute = ({ component: Component, ...props }) => (
  <Route
    render={props => (isAuthenticated() ? (
      <Component {...props} />
    ) : (
      <Redirect
        to='/' 
      />
    ))
    }
  />
)

const DefaultPrivateRouter = ({ component: Component, ...props }) => (
  <Route
    render={props => (isAuthenticated() ? (
      <Redirect
        to='/main' 
      />
    ) : (
      <Redirect
        to='/' 
      />
    ))
    }
  />
)


export { PrivateRoute, DefaultPrivateRouter };