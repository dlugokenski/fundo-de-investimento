import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Home from "Pages/home";
import MainContainer from "Componente/ContainerMain/MainContainer";
import { PrivateRoute } from "RoutersPage/PrivateRoute";

export default () => (
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={Home} />
      <PrivateRoute path="/main" component={MainContainer} />
      <PrivateRoute path="/*" component={Home} />
    </Switch>
  </BrowserRouter>
);
