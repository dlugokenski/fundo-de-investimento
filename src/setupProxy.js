const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api',
    createProxyMiddleware({
      target: `http://${process.env.REACT_APP_API_URL}`,
      changeOrigin: true,
      pathRewrite: {
        '^/api': '',
      },
    })
  );
  app.use(
    '/ws',
    createProxyMiddleware({
      target: `ws://${process.env.REACT_APP_API_URL}`,
      changeOrigin: true,
      ws: true,
      pathRewrite: {
        '^/ws': '',
      },
    })
  );
};
