import moment from 'moment';

export function converterDinheiroParaFloat(valor){
    let newValor = null;
	newValor = valor.toString().replace("R$", "");
    newValor = newValor.toString().replace(",00", "");
	newValor = newValor.toString().replace(" ", "");
    if(newValor.includes(',')){
        newValor = newValor.toString().replace(".", "");
    } 
    newValor = newValor.toString().replace(",", ".");
    return parseFloat(newValor);
}


function  convertDateToUTC(date) { 
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()); 
} 

export function dataFormatado(element) { 
    return moment(convertDateToUTC(new Date(element)).toISOString()).format("DD/MM/YYYY")
} 


  
  
