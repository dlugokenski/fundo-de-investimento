const TIPO_ATIVOS = {
  ACOES: "Ações",
  TESOURO_DIRETO: "Tesouro Direto",
  FII: "FII",
};

const TIPO_NOTICIAS = {
  AGE: "Reunião conselho administração",
  RESULTADO: "Resultado trimestral",
  DIVERSOS: "Diversos",
};

const VALID_REPONSE = {
  INVALID_TOKEN: "invalid_token",
  AUTHENTICATION_REQUIRED:
    "Full authentication is required to access this resource",
};

export { TIPO_ATIVOS, TIPO_NOTICIAS, VALID_REPONSE };
