
export function toRGBA( r, g, b, a){
   return `rgba(${r},${g},${b},${a})`
}

export function toColor (color) {
    const rgba = color.replace(/^rgba?\(|\s+|\)$/g, '').split(',');
    return {
      r: Number(rgba[0]),
      g: Number(rgba[1]),
      b: Number(rgba[2]),
      a: Number(rgba[3]),
    };
}

export function hexToRGBA (hex) {
  const rgb = hexToRgb(hex);
    return {
      r: rgb[0],
      g: rgb[1],
      b: rgb[2],
      a: 1,
    };
}

export function randomHex () {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

export function hexToRgb (hex) {
  const removeHash = (h) => {
    const arr = h.split('');
    arr.shift();
    return arr.join('');
  };

  const expand = h => h
    .split('')
    .reduce((accum, value) => accum.concat([value, value]), [])
    .join('');

  if (hex.charAt && hex.charAt(0) === '#') {
    hex = removeHash(hex);
  }

  if (hex.length === 3) {
    hex = expand(hex);
  }

  const bigint = parseInt(hex, 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;

  return [r, g, b];
};

