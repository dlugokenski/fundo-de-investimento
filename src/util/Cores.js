const defaultCores = {
    0: '#f49502',
    1: '#0245f7',
    2: '#63ab31',
    3: '#a21849',
    4: '#f72612',
    5: '#cae42a ',
    6: '#3b019f',
    7: '#f3b702',
    8: '#038ec8',
    9: '#f65108',
    10: '#f7f732',
    11: '#8201aa',
    12: '#00aaff',
    13: '#25bb07',
    14: '#ff0055',
    15: '#8000ff',
    16: '#04c4ea',
    17: '#00ffff',
    18: '#ffd500',
}

const coresTiposAtivos = {
    "Ações": {
        cor: '#f49502',
        hover: '#f49502'
    },
    "FII": {
        cor: '#63ab31',
        hover: '#63ab31'
    },
    "Tesouro Direto": {
        cor: '#0245f7',
        hover: '#0245f7'
    },
}

export {
    defaultCores,
    coresTiposAtivos
}