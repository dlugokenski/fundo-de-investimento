import { createFactory } from 'react';

export default props => (Component) => {
  const factory = createFactory(Component);
  const DefaultProps = ownerProps => factory(ownerProps);
  DefaultProps.defaultProps = props;
  return DefaultProps;
};
