import React from 'react';

import {
  Col, Form, Icon, Popconfirm, Row, Button,
} from 'antd';
import EditableCell from './EditableCell';

export const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);
const EditableFormRow = Form.create()(EditableRow);

export default class EditableTable extends React.Component {
  constructor(props) {
    super(props);
    const { columns: propsColumns } = this.props;
    this.state = { editingKey: '' };
    this.columns = [
      ...propsColumns,
      {
        title: 'Ações',
        key: 'acoes',
        fixed: props.actionsFixed,
        dataIndex: 'operation',
        render: (_, record) => (
          <Row gutter={16}>
            {this.actionEdit(record)}
            {this.actionDelete(record)}
            {this.appendActions(record)}
          </Row>
        ),
      },
    ];
  }

  actionEdit = (record) => {
    const { handleSave, handleEdit } = this.props;
    const editable = this.isEditing(record);

    if (handleSave || handleEdit) {
      return editable && !handleEdit ? (
        <Col span={12}>
          <span style={{ width: 32, display: 'inline-block' }}>
            <EditableContext.Consumer>
              {form => <Icon style={{ color: 'green', marginRight: 3 }} type="check" onClick={() => this.save({ form, record })} />}
            </EditableContext.Consumer>

            <Popconfirm title="Deseja cancelar?" onConfirm={() => this.cancel(record)}>
              <Icon type="close" style={{ color: 'red' }} />
            </Popconfirm>
          </span>
        </Col>
      ) : (
        <Col span={6}>
          <Button shape="circle" icon="edit" size="small" type="primary" onClick={() => this.edit(record)} />
        </Col>
      );
    }
    return null;
  };

  appendActions = (record) => {
    const { appendActions } = this.props;
    const editable = this.isEditing(record);
    return appendActions && !editable ? appendActions(record) : null;
  };

  actionDelete = (record) => {
    const { handleDelete, dataSource } = this.props;
    const editable = this.isEditing(record);
    if (handleDelete && dataSource.length >= 1 && !editable) {
      return (
        <Col span={6}>
          <Popconfirm title="Confirma exclusão?" onConfirm={() => this.handleDelete(record)}>
            <Button shape="circle" icon="delete" size="small" type="primary" style={{ marginLeft: '10px' }} loading={this.props.loadingButtonDelete} />
          </Popconfirm>
        </Col>
      );
    }
    return null;
  };

  getRecordKey = (record, index) => {
    const { rowKey } = this.props;
    return typeof rowKey === 'function' ? rowKey(record, index) : record[rowKey];
  };

  isEditing = (record) => {
    const { editingKey } = this.state;
    return this.getRecordKey(record) === editingKey;
  };

  handleDelete = (record) => {
    const { dataSource, handleDelete } = this.props;
    const newData = [...dataSource];
    const index = newData.findIndex(item => this.getRecordKey(record) === this.getRecordKey(item));
    handleDelete && handleDelete(newData[index]);
  };

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  save({ form, record }) {
    const { dataSource, handleSave } = this.props;
    form.validateFields((error, row) => {
      if (error) {
        return;
      }

      const newData = [...dataSource];
      const index = newData.findIndex(item => this.getRecordKey(record) === this.getRecordKey(item));

      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row,
        });
        this.setState({ editingKey: '' });
      }

      handleSave && handleSave(newData[index]);
    });
  }

  edit(record) {
    const { handleEdit } = this.props;
    if (handleEdit) {
      handleEdit(record);
    } else {
      this.setState({ editingKey: this.getRecordKey(record) });
    }
  }

  render() {
    const { Table } = this.props;

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell,
      },
    };

    const columns = this.columns.map((col) => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.inputType || 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record),
        }),
      };
    });

    return React.cloneElement(Table, {
      ...this.props,
      columns,
      components,
    });
  }
}
