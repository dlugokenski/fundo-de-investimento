import React from 'react';
import {
  Form, Input, InputNumber, Select,
} from 'antd';
import { EditableContext } from './EditableTable';

const FormItem = Form.Item;

export default class EditableCell extends React.Component {
  getInputType() {
    const { dataIndex, inputType, record } = this.props;
    return typeof inputType === 'function' ? inputType({ record, dataIndex }) : inputType;
  }

  getInput(form) {
    const { dataIndex, title, record } = this.props;
    const { getFieldDecorator } = form;
    const inputType = this.getInputType() || {};
    const { options = [], key, label } = inputType;

    let input;
    let initialValue = record[dataIndex];

    switch (inputType.type) {
      case 'number':
        input = <InputNumber />;
        break;
      case 'select':
        input = (
          <Select style={{ width: '100%' }}>
            {options.map(o => (
              <Select.Option key={`opt${o[key]}`} value={o[key]}>
                {o[label]}
              </Select.Option>
            ))}
          </Select>
        );
        initialValue = options.find(o => String(o[key]) === String(record[dataIndex]));
        initialValue = initialValue ? initialValue[key] : undefined;
        break;
      default:
        input = <Input size="1" />;
        break;
    }

    return getFieldDecorator(dataIndex, {
      rules: [
        {
          required: true,
          message: `Preencha o campo ${title}.`,
        },
      ],
      initialValue,
    })(input);
  }

  render() {
    const { editing, ...restProps } = this.props;
    return (
      <EditableContext.Consumer>
        {form => (
          <td>
            {editing ? (
              <FormItem style={{ margin: 0 }}>
                {this.getInput(form)}
              </FormItem>
            ) : restProps.children}
          </td>
        )}
      </EditableContext.Consumer>
    );
  }
}
