import React from 'react';

import EditableTable from './EditableTable';

export default hocProps => Table => <EditableTable {...Table.props} {...hocProps} Table={Table} />;
