import React from "react";
import styled from "styled-components";

const StyledSpan = styled.div`
  padding: 0px 5px;
`;

const FieldSet = styled.div`
  position: relative;
  display: inline-block;
  display: table;
  margin: 5px 0;
  color: rgba(0, 0, 0, 0.85);
  font-weight: 500;
  font-size: 16px;
  white-space: nowrap;
  text-align: left;
  background: transparent;
  font-variant: tabular-nums;
  line-height: 1.5;
  list-style: none;
  clear: both;
  width: 100%;
  min-width: 100%;
  height: 1px;

  &:after {
    position: relative;
    top: 50%;
    width: 95%;
    display: table-cell;
    border-top: 1px solid #e8e8e8;
    -webkit-transform: translateY(50%);
    -ms-transform: translateY(50%);
    transform: translateY(50%);
    content: "";
  }

  &:before {
    content: "";
    top: 50%;
    width: 2%;
    position: relative;
    display: table-cell;
    border-top: 1px solid #e8e8e8;
    -webkit-transform: translateY(50%);
    -ms-transform: translateY(50%);
    -webkit-transform: translateY(50%);
    -ms-transform: translateY(50%);
    transform: translateY(50%);
    content: "";
  }
`;

export default (props) => (
  <FieldSet>
    <StyledSpan>{props.text}</StyledSpan>
  </FieldSet>
);
