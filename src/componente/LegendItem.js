import React from 'react';
import { hexToRGBA } from 'Util/ColorUtil';
import ColorPicker from 'Componente/ColorPicker';

export default props => (
  <ColorPicker
    color={hexToRGBA(props.color)}
    onChangeColor={props.onChangeColor}
    index = { props.index}
  />
);

