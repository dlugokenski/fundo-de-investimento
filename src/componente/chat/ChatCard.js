import React from 'react';
import styled from 'styled-components';
import { Input, Button, Tooltip } from 'antd';
import HeaderChat from './HeaderChat'

import { connect } from 'react-redux';

const { TextArea } = Input;

const StyleDivMain = styled.div`
  width: 250px;
  height: ${state => state.collapsed ? '30px' : '300px'};
  border-radius: 4px;
  border: 1px solid #e8e8e8;
  margin-right: 30px;
  background: white;
  align-self: flex-end;
  transition: all 200ms ease-in;
  margin-bottom: 1px;
  display: flex;
  flex-direction: column;
`;

//focus-within
const StyleDivMessages = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
  overflow-wrap: anywhere;
`;

const StyleMessage = styled.div`
  background-color: ${state => state.origin && state.origin === "sim" ? '#e8ffd1' : '#fafafa'};
  padding: 5px 3px;
  margin-left: 6px;
  margin-right: 6px;
  display: flex;
  justify-content: ${state => state.origin && state.origin === "sim" ? 'flex-start' : 'flex-end'};
  border-radius: 5px;
  border: 1px solid #e8e8e8;
  margin-bottom: 5px;

  &:first-child{
    margin-top: 5px;
  } 
`;

const StyleDivFooter = styled.div`
  width: 100%;
  margin-left: -1px;
  margin-right: 1px;
  display: flex;
  flex-direction: row;
  
  >textarea{
    width: 100%;
    resize: none;
    overflow: auto;
    padding: 2px;
    outline: none;
    margin-top: 10px;

    /* &::-webkit-scrollbar { 
      opacity: 1;
      transition: .3s;
      transition-delay: .5s;
    }  */

    &:focus, &:hover{
      border-color: #cccccc;
      outline: none;
      -webkit-box-shadow: none;
      box-shadow: none;
    }
  }

  >button{
    margin: auto 2px;
    padding: 2px;
  }
`;


export default class ChatComponent extends React.Component {

  messagesEndRef = React.createRef();

  state = {
    socketOn: false,
    socket:undefined,
    collapsed: false
  }

  componentDidMount(){
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    this.messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  handleCollapsed = () => {
    const { collapsed = false } = this.state;
    this.setState({ collapsed: !collapsed })
  }

  enviarMessage(){
    const { message } = this.state;
    const { dataChat = {} } = this.props; 
    this.scrollToBottom()
    this.props.sendMessageWebsocket(message, dataChat);
    this.setState({ message: ""})
  }

  handleInput = e => {
    this.setState({ message: e.target.value})
  };

  render() {
    const { collapsed, message = "", } = this.state; 
    const { dataChat = {}, status, listMessages, usuario } = this.props; 

    return (
      <StyleDivMain {...this.state}>
        <HeaderChat 
          label={dataChat.from.userName}
          removeChat={this.props.removeChat}
          collapsed={collapsed}
          handleCollapsed={this.handleCollapsed}
          visibleStatus={true}
          contactConnected={status}
          id={dataChat.id}
        />
        <StyleDivMessages id="teste">
          {listMessages.map((element, index) => (
            <StyleMessage 
              key={index} 
              origin={usuario && 
              usuario.nome && 
              element.userTo && 
              usuario.nome === element.userTo.userName ? "sim" : "nao"}>
              {element.text}
            </StyleMessage>
          ))}          
          <div ref={this.messagesEndRef} />   
        </StyleDivMessages>     
   
        <StyleDivFooter>
          <TextArea 
            value={message}
            onChange={e => this.handleInput(e)}
            autoSize={{ minRows: 1, maxRows: 3 }}
          />
          <Tooltip placement="top" title="Enviar">
            <Button 
              onClick={() => this.enviarMessage()}
              disabled={!message}
              type="primary" 
              shape="circle" 
              icon="caret-right" 
              size="small"
            />
          </Tooltip>            
        </StyleDivFooter>      
      </StyleDivMain>
    );
  }
}
