import React from 'react';
import styled from 'styled-components';

import { connect } from 'react-redux';

import ChatCard from 'Componente/chat/ChatCard';
import TimeListUser from 'Componente/chat/TimeListUser';
import { buscarMessagesChat } from 'Provider/Chat'

const StyleDivMainChart = styled.div`
  position: fixed;
  z-index: 99;
  bottom: 0;
  right: 0;
  background: transparent;  
  justify-content: flex-end;
  display: flex;
`;

class ChatMainComponent extends React.Component {

  state = {
    listChat: [],
    listUserConnected: []
  }

  componentDidMount(){
    this.initWebSocket();
  }

  componentDidUpdate(nextProps){
    if(nextProps !== this.props ){
        if(nextProps.socket != this.props.socket){
          this.initWebSocket();
        }
    }         
  }

  sendMessage = (message, dataChat) => {
    const { socket } = this.props; 

    socket.send(
      '/app/chat_room', 
      { "content-type": "application/json"},
      JSON.stringify({
        userFrom: dataChat.from,
        userTo: dataChat.to,
        text: message,
        data: new Date()
      })
    );
  };

  removeChat = (id) => {
    const { listChat = [] } = this.state;
    const index = listChat.indexOf(listChat.find(element => element.id === id));
    if(index != -1){
      listChat.splice(index,1);
    }  
    this.setState({listChat});
  }

  addChat = async (userAdd) => {
    const { usuario } = this.props;
    const { listChat = [] } = this.state;
    const messages = await buscarMessagesChat(userAdd.id);
    listChat.push({
      to: usuario,
      from: userAdd,
      messages,
      id: listChat.length + 1
    })
    this.setState({listChat});
  }

  initListStartTimeLineUser = (list) =>{
    const { usuario } = this.props;
    list.forEach(element => {
      this.sendMessageAlterStatusWebsocket(true, usuario.nome, element.userName, true);
    });
  }

  sendMessageAlterStatusWebsocket = (status, to, from, newStatus) => {
    const { socket } = this.props; 
    socket.send(
      '/app/chat_alter_status', 
      { "content-type": "application/json"},
      JSON.stringify({from, to, status, newStatus })
    );
  };

  statusAlterUser = (data) => {
    const { listUserConnected } = this.state;

    if(data){
      if(data.newStatus){
        this.sendMessageAlterStatusWebsocket(true, data.from, data.to, false);
      }

      const index = listUserConnected.indexOf(listUserConnected.find(element => element.nome === data.to));
      if(index != -1){
        listUserConnected[index].status = data.status
      } else {
        listUserConnected.push({
          nome: data.to,
          status: data.status
        })
      } 
      this.setState({listUserConnected});
    }   
  }

  initSubscribeWebSocket = () => {
    const { socket } = this.props; 

    if(socket){
      socket.subscribe('/user/queue/chat/specific-user', data => this.addMessages(JSON.parse(data.body)));
    }
  }

  addMessages = (body) => {
    const { listChat = [] } = this.state; 

    const index = listChat.indexOf(listChat.find(element => element.from.userName === body.userTo.userName));
    if(index != -1){
      listChat[index].messages.push(body.text);
      this.setState({listChat});
    } else {
      this.addChat(body.userTo);
    }      
  }

  getStatusUser = (user) => {
    const { listUserConnected } = this.state;
    const userStatus = listUserConnected.find(element => element.nome === user);
    return userStatus ? userStatus.status : false;
  }

  initWebSocket(){
    const { socket } = this.props;  
    
    if(socket){;  
      this.initSubscribeWebSocket();    
      socket.subscribe('/user/queue/chat/status', data => this.statusAlterUser(JSON.parse(data.body)));
    }
  }

  render(){
    const { listChat } = this.state;
    const { usuario } = this.props;

    return(
      <StyleDivMainChart> 
        {listChat.map((item, index) => (
          <ChatCard 
            listMessages={item.messages}
            dataChat={item} 
            key={index}
            removeChat={this.removeChat}
            sendMessageWebsocket={this.sendMessage}
            status={this.getStatusUser(item.from.userName)}
            usuario={usuario}
          />  
        ))}        
        <TimeListUser 
          sendStatus={this.initListStartTimeLineUser}
          usuario={usuario}
          addChat={this.addChat}
          getStatus={this.getStatusUser}
        />
      </StyleDivMainChart>
    )
  }
}

const mapStateToProps = state =>  ({ 
  usuario : state.configuracaoUsuario.usuario,
  socket : state.websocket.socket 
})
export default connect(mapStateToProps,null)(ChatMainComponent)


