import React from 'react';
import styled from 'styled-components';
import { List, Avatar, Input, Icon } from 'antd';

import { consultarUsuariosAtivos } from 'Provider/Usuario'
import HeaderChat from './HeaderChat'

const ListItem = List.Item;

const StyleDivMain = styled.div`
    width: 200px;
    height: ${state => state.collapsed ? '30px' : '300px'};
    border-radius: 4px;
    border: 1px solid #e8e8e8;
    margin-right: 30px;
    background: white;
    align-self: flex-end;
    transition: all 200ms ease-in;
    margin-bottom: 1px;
    cursor: pointer;

    .ant-input-affix-wrapper{
        padding-top: 5px;
    }

    .ant-list{        
        overflow-y: auto;
        height: 100%;
        padding-top: 10px;
    }

    li{
        padding-top: 4px !important;
        padding-bottom: 4px !important;;

        &:hover{
            background: #e6f4ff;
        }
    }
`;

const StyleDivHeader = styled.div`
    height: 34px;
    border-bottom: 1px solid #e8e8e8;
    display: flex;
    width: 100%;
`;

const StyleDivAvatarStatus = styled.div`
    display: flex;
    flex-direction: row;

    .ant-avatar{
        margin-left: 10px;
    }
`;

const StyleDivHeaderStatus = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 20px;
    height: auto;
    align-items: center;
    padding-left: 3px;
    margin-left: -11px;
    z-index: 1;
    margin-bottom: -17px;
`;

const StyleDivHeaderStatusBoilinha = styled.div`
    display: flex;
    justify-content: flex-start;
    margin-right: 1px;
    background-position: -15px -15px;
    border-radius: 50%;
    width: 8px;
    height: 8px;
    background:${state => state.contactConnected ? '#3aba1a' : 'red'}; 
`;

const StyleDivHeaderName = styled.div`
    width: 100%;
    max-width: 22ch;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
`;

const StyleInput = styled(Input)`
    margin-left: -1px;
    margin-right: -1px;
    margin-top: -1px;
    width: 101%;
`;

export default class TimeListUser extends React.Component {

    state = {
        collapsed: false,
    }

    componentDidMount(){
        this.getUsuarios();
    }

    handleCollapsed = () => {
        const { collapsed = false } = this.state;
        this.setState({ collapsed: !collapsed })
      }
    
    async getUsuarios(search = ""){
        const { usuario } = this.props;
        
        const listUser = await consultarUsuariosAtivos(search);
        const listSemUserLogado = listUser.filter(element => element.userName !== usuario.nome);
        this.props.sendStatus(listSemUserLogado);
        this.setState({ listUser: listSemUserLogado })
    }

    render(){
        const { listUser = [], collapsed } = this.state;

        return(
            <StyleDivMain {...this.state}>
                <HeaderChat 
                    label={"Lista de amigos"}
                    visibleButtonClose={false}
                    collapsed={collapsed}
                    handleCollapsed={this.handleCollapsed}
                />
                <StyleInput
                    placeholder="Pesquisar..."
                    prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    onKeyUp={e => this.getUsuarios(e.target.value)}
                />
                <List
                    size="small"
                    dataSource={listUser}
                    renderItem={item => (
                        <ListItem onDoubleClick={() => this.props.addChat(item)}>
                            <StyleDivAvatarStatus>
                                <Avatar                                 
                                    src={item.avatar || undefined}
                                    icon= {item.avatar && item.avatar === "" ? undefined : "user"}
                                />
                                <StyleDivHeaderStatus>
                                    <StyleDivHeaderStatusBoilinha contactConnected={this.props.getStatus(item.userName)} />
                                </StyleDivHeaderStatus> 
                            </StyleDivAvatarStatus>                                                       
                            <StyleDivHeaderName>
                                {item.userName}
                            </StyleDivHeaderName>
                        </ListItem>
                    )}
                />
            </StyleDivMain>
        )
    }   
}