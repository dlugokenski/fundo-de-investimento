import React from 'react';
import styled from 'styled-components';
import { Button, Tooltip } from 'antd';

const StyleDivHeader = styled.div`
    height: 34px;
    border-bottom: 1px solid #e8e8e8;
    display: flex;
    width: 100%;
    background-color: #f2f2ed;
`;

const StyleDivHeaderStatus = styled.div`
    display: flex;
    justify-content: flex-start;
    width: 25px;
    height: auto;
    align-items: center;
    padding-left: 3px;
`;

const StyleDivHeaderStatusBoilinha = styled.div`
    display: flex;
    justify-content: flex-start;
    margin-right: 1px;
    background-position: -15px -15px;
    border-radius: 50%;
    width: 8px;
    height: 8px;
    background:${state => state.contactConnected ? '#3aba1a' : 'red'}; 
`;

const StyleDivHeaderName = styled.div`
    width: 100%;
    max-width: 22ch;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    padding-top: 6px;
    margin-left: 5px;
`;

const StyleDivHeaderButtons = styled.div`
    flex-direction: row;
    display: flex;
    >button{
        border: 0px;
        background: transparent;
        
        &:hover{
            background: #4e99ec;

            >i{
                color: white;
            }
        }
    }
`;

export default ({
        visibleStatus = false, 
        label, 
        collapsed, 
        handleCollapsed, 
        removeChat, 
        id, 
        contactConnected,
        visibleButtonClose = true
    }) => (
    <StyleDivHeader>
        {visibleStatus && (
            <StyleDivHeaderStatus>
                <StyleDivHeaderStatusBoilinha contactConnected={contactConnected} />
            </StyleDivHeaderStatus>
        )}
        <StyleDivHeaderName>
            {label}
        </StyleDivHeaderName>
        <StyleDivHeaderButtons>
            <Tooltip placement="top" title={collapsed ? "Maximizar": "Minimizar" }>
                <Button 
                    type="default" 
                    icon={collapsed ? "border": "minus" }
                    onClick={() => handleCollapsed()} 
                />
            </Tooltip> 
            {visibleButtonClose && (
                <Tooltip placement="top" title="Fechar">
                    <Button
                        type="default" 
                        icon="close" 
                        key="back" 
                        onClick={() => removeChat(id)}
                    />
                </Tooltip> 
            )}                
        </StyleDivHeaderButtons>
  </StyleDivHeader>
)
