import React from 'react';
import styled from 'styled-components';
import CountUp from 'react-countup';

import { converterDinheiroParaFloat } from 'Util/Formatacao'

const StyleDivMain = styled.div`
    display: flex;
    align-items: flex-end;
    flex-direction: row;
    font-family: 
        Tahoma, 
        'Helvetica Neue', 
        -apple-system, 
        BlinkMacSystemFont, 
        'Segoe UI', 
        'PingFang SC', 
        'Hiragino Sans GB', 
        'Microsoft YaHei', 
        'Helvetica Neue', 
        Helvetica, 
        Arial, 
        sans-serif, 
        'Apple Color Emoji', 
        'Segoe UI Emoji', 
        'Segoe UI Symbol';
`;

export default class LabelMoney extends React.Component {
    
    state = {
        animation: false
    }
    
    componentDidMount(){
        this.mountedValue();
    }

    componentDidUpdate(nextProps){
        if(nextProps !== this.props){
            this.mountedValue();
        }         
    }

    mountedValue(){
        const { value = 0, separator = ',', animation = false } = this.props;
        const newValue = value.toString().split(separator);
        const color = converterDinheiroParaFloat(value) > 0 ? 'green' : 'red'
        
        this.setState({
            color,
            valueFirt: newValue[0],
            valueLast: newValue[1],
            animation
        })
    }

    renderDivMain = (valueFirt, valueLast, color) => {
        const { prefix = "R$: " } = this.props;
        return (
            <StyleDivMain>
                <div style={{ display: 'inline', color: color, marginRight: '3px' }}>
                    {prefix}
                </div>
                <div style={{ fontSize: '17px', display: 'inline', color: color , marginBottom: '-1px' }}>
                    { valueFirt }
                </div>
                <div style={{ fontSize: '13px', display: 'inline', color: color }}>
                    ,{ valueLast }
                </div>    
            </StyleDivMain>
        )
    }

    getValueComPonto(value){
        return Number(value.toString().split(",").join("."));
    }

    render(){
        const { value, prefix = "R$: " } = this.props;
        const { animation, valueFirt = 0, valueLast = '00', color } = this.state;
        
        return(
            <React.Fragment>
                {value && (
                    <React.Fragment>
                        {animation ? (
                            <CountUp 
                                start={0} 
                                delay={0}
                                redraw={true}
                                duration={3}
                                decimals={2}
                                decimal=","
                                prefix={prefix}
                                end={this.getValueComPonto(value)} 
                            >
                                {({ countUpRef }) => (
                                    <StyleDivMain>                                       
                                        <span 
                                            style={{ 
                                                fontSize: '17px', 
                                                display: 'inline', 
                                                color: this.getValueComPonto(value) > 0 ?  'green' : 'red', 
                                                marginBottom: '1px' 
                                            }}
                                            ref={countUpRef} 
                                        />
                                    </StyleDivMain>
                                )}
                            </CountUp>
                        ) : (
                            this.renderDivMain(valueFirt, valueLast, color)
                        )}
                    </React.Fragment>
                )}
            </React.Fragment>
        )
    }
}

