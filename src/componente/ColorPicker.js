import React from 'react';
import styled from 'styled-components';
import { ChromePicker } from 'react-color';
import { Popover, Row, Button, Card, Tooltip } from 'antd';

const ColorBox = styled.div`
    background-color: ${({
    color = {
      r: 1,
      g: 1,
      b: 1,
      a: 1,
    },
  }) => `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`};
    height: 29px;
    width: 29px;
    margin: 4px;
    border-radius: 4px;
    border: 1px solid;
`;

export default class ColorPicker extends React.Component {
  state = {};

  componentDidMount(){
    const { color } = this.props;
    this.setState({ color, corSelecionado: color })
  }

  mudarCor(){
    const { corSelecionado, index } = this.state;
    this.setState({ color: corSelecionado.rgb, visiblePoupover: false});
    this.props.onChangeColor(corSelecionado, index);
  }

  render(){

    const { color, corSelecionado, visiblePoupover = false } = this.state;
    const { index } = this.props;

  
    const DivPicker = () => (
      <Card 
        title="Palheta de cores" 
        style={{margin: '-12px -16px'}}
        extra={ 
          <Tooltip placement="rightBottom" title="Fechar">
            <Button
              style={{ marginRight: "-15px" }} 
              type="primary" 
              icon="close" 
              key="back" 
              onClick={() => this.setState({ visiblePoupover: false })}
            />
          </Tooltip>  
        }>
        <Row>
          <ChromePicker 
            color={corSelecionado} 
            onChangeComplete={cor => this.setState({ corSelecionado: cor, index }) } 
          />
        </Row>
        <center>
          <Row style={{ marginTop: '15px' }}>
            <Button disabled={ !corSelecionado} type="primary" key="back" onClick={() => this.mudarCor()}>
              {'Aplicar'}
            </Button>
          </Row>
        </center>
      </Card>
    )  

    return (
      <Popover 
        content={ <DivPicker/> }         
        trigger="click"
        visible={visiblePoupover}
        onVisibleChange={() => this.setState({ visiblePoupover: true })}
      >
        <ColorBox color={color} role="presentation" />
      </Popover>
    );
  }
}