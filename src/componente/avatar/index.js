import React from 'react';
import { Avatar, Button, Tooltip, notification } from 'antd';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Creators as actions} from 'Ducks/configuracaoUsuario'
import styled from 'styled-components';

import { cadastrarAvatarUsuario } from 'Provider/Usuario'
import ModalCadastrarFoto from './ModalCadastrarFoto'

const StyleDivUpload = styled.div`
    min-height: 49px;

    .ant-avatar{
        display: block;
    }

    .ant-btn{
        position: relative;
        margin-left: 65px;
        top: -35px;
        opacity : 0.8;
        border: 1px solid;
        display: none;

        &:hover {
            opacity : 1;
            display:block;
            margin-bottom: -31px;
        }
    }
    
    >span:hover {

        ~button{
            display: ${props => props.collapsed ? 'none' : 'block'};
            margin-bottom: -31px;
        }
    }
`;
class Avatart extends React.Component {
    state = {};

    componentDidMount() {
        this.buscarAvatarCliente();
    }

    componentDidUpdate(nextProps){
        if(nextProps !== this.props ){
            this.buscarAvatarCliente();
        }         
    }


    buscarAvatarCliente() {
       const { configuracoes } = this.props;
       if(configuracoes && configuracoes.usuario.avatar){
           this.setState({ imageAvatar : configuracoes.usuario.avatar })
       }
    }
   
    async onOk(image){
        const formData = new FormData();
        formData.append('uploadedFile', image);
        const newUsuario = await cadastrarAvatarUsuario(formData);
        this.props.alterarUsuario(newUsuario);
        this.openNotification('success',"Sucesso", "Avatar alterado com sucesso!");
        this.setState({showModalCadastrarFoto: false});
    }

    openNotification(type, title, description) {   
        notification[type || 'info']({
          message: title || 'Aviso',
          description: description,
        });
    }

    renderAvatar = props => {
        const { collapsed } = this.props;
        const { imageAvatar } = this.state;
        
        return(
            <StyleDivUpload {...this.props}> 
                <Avatar 
                    size={ collapsed ? 40 : 100} 
                    src={imageAvatar || undefined}
                    icon= {imageAvatar && imageAvatar === "" ? undefined :"user"}
                    style={{ margin: '9px 0px 9px 0px' }}
                />
                <Tooltip placement="right" title="Alterar avatar">
                    <Button 
                        type="dashed" 
                        size="small" 
                        shape="circle" 
                        onClick={() => this.setState({showModalCadastrarFoto: true})} 
                        icon="edit" 
                    />
                </Tooltip>
            </StyleDivUpload>
        ) 
    }

    renderModalCadastrarFoto() {
        const { showModalCadastrarFoto = false } = this.state;
        if (showModalCadastrarFoto) {
          return (
            <ModalCadastrarFoto
              onOk={imageUrl => this.onOk(imageUrl)}
              visible={showModalCadastrarFoto}
              handleCancel={() => this.setState({ showModalCadastrarFoto: false })}
              usuario={this.props.configuracoes.usuario || {}}
            />
          );
        }
        return null;
      }
    
    render(){
        return(
            <React.Fragment>
                {this.renderModalCadastrarFoto()}
                {this.renderAvatar()}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({ configuracoes : state.configuracaoUsuario })
const mapDispatchToPros = dispatch => bindActionCreators(actions, dispatch)

export default connect(mapStateToProps,mapDispatchToPros)(Avatart)