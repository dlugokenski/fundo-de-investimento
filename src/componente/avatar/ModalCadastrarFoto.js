import React from 'react';
import { Modal, Button,  Upload, Icon, notification, Tooltip, Spin } from 'antd';

export default class ModalCadastrar extends React.Component {
  state = { 
    loadingImagen: false,
    imagenSelecionada: false
  };

  componentDidMount(){
    this.iniciarPagina();  
  }

  iniciarPagina(){
    const { usuario } = this.props;
    if(usuario && usuario.avatar){
      this.setState({ imageBase64: usuario.avatar });
    }   
  }

  handleCancel() {
    this.props.handleCancel();
  }

  handleSubmit = async () => {
    const { file } = this.state;
    this.setState({ imagenSelecionada: false });
    this.props.onOk(file);
  }

  openNotification(type, title, description) {   
    notification[type || 'info']({
      message: title || 'Aviso',
      description: description,
    });
  }

  onHandleUpload = async (file) => {
    this.setState({ loadingImagen: true });
    if( await this.checarArquivo(file)){
      this.converterFileToArray(file);      
      this.setState({  file, loadingImagen: false, imagenSelecionada: true });
    }
    this.setState({  loadingImagen: false });
    return true;
  };


  converterFileToArray(file){
    const reader  = new FileReader();
    reader.onloadend = () => {
     this.setState({ imageBase64: reader.result });
    }
    reader.readAsDataURL(file);
}


  renderDivRetratoImagen(){
    return(
      <div>
        <Icon style={{ marginRight: '10px' }} type={ 'upload' } />
        Escolha uma imagen
      </div>
    )
  }

  checarArquivo = (file) => {
    const isJPG = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJPG) {
      this.openNotification('error',"Erro",'Extensão do arquivo não permitido!');
      return false;
    }
    const isLt2M = file.size / 1024 / 1024 < 2;
    if (!isLt2M) {
      this.openNotification('error',"Erro",'Tamanho maximo do arquivo e 2MB!');
      return false;
    }
    return true;
  }
  
  render() {
    const { imageBase64, imagenSelecionada, loadingImagen } = this.state;

    return (
        <React.Fragment>
            <Modal
                style={{ top: '20px' }}
                visible={this.props.visible}
                onCancel={() => this.handleCancel()}
                title="Alterar imagen do perfil"
                footer={[
                    <Button key="back" onClick={() => this.handleCancel()}>
                    {'Cancelar'}
                    </Button>,
                    <Button
                    disabled={!imagenSelecionada}
                    key="submit"
                    type="primary"
                    onClick={() => this.handleSubmit()}
                    >
                    {'Confirmar'}
                    </Button>,
                ]}
            >
                <Upload 
                  customRequest={() => (false)}
                  beforeUpload={this.onHandleUpload} 
                  showUploadList={false}
                >
                  <Tooltip placement="top" title="Adicionar">
                    <Button type="primary">
                      <Icon type="upload" />
                    </Button>
                  </Tooltip>
                </Upload>
              <center>
                {loadingImagen ?
                  <Spin />
                : 
                imageBase64 ? <img  style={{width: '180px', height: '180px'}} src={imageBase64} alt="avatar" /> : this.renderDivRetratoImagen()
                }
              </center>
            </Modal>
        </React.Fragment>
    );
  }
}
