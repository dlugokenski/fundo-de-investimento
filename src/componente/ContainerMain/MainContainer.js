import React from 'react';
import { notification } from 'antd';
import { Switch, Route, Redirect } from 'react-router-dom';
import {isMobile} from 'react-device-detect';
import Loadable from 'react-loadable';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Creators as actionsConfiguracao} from 'Ducks/configuracaoUsuario'
import { Creators as actionsMenu} from 'Ducks/menu'
import { Creators as actionsConfiguracaoAplication} from 'Ducks/configuracaoAplication'
import { Creators as actionsAtivo} from 'Ducks/ativos'

import websocketController from 'Store/webSocket/WebsocketController'
import { PrivateRoute, DefaultPrivateRouter } from 'RoutersPage/PrivateRoute'
import { me } from 'Provider/Usuario'
import { refreshToken } from 'Provider/Login'
import { buscarConfiguracoesAplication } from 'Provider/Util'
import SpinCuston from 'Componente/Spin';

const LayoutPage = Loadable({
  loader: () => import('./LayoutPage'),
  loading: () => <SpinCuston />,
});

const InitialApplication = Loadable({
  loader: () => import('Pages/initial-aplication'),
  loading: () => <SpinCuston />,
});

class MainContainer extends React.Component {

  state = {
    tokenExpiraIn: 250000,
    pegouTokenAoIniciarPagina: false,
    initWebSocketAlert: false,
    propsScoketJaIniciado: false
  };

  componentDidMount(){
    this.pegarInformacaoUsuario();
    this.getConfiguracaoAplication();
    if(isMobile){
      this.props.changeCollapsed(true)
    }
  }

  componentDidUpdate(){
    const { initWebSocketAlert, propsScoketJaIniciado } = this.state;

    if(!initWebSocketAlert && this.props.socket){
      this.initWebSocket();
    }   

    if(!propsScoketJaIniciado && this.props.socket){
      this.setState({ propsScoketJaIniciado: true });
    } 
   

    if(propsScoketJaIniciado && !this.props.socket){
      this.checkStatusWebsocket();
    }    
  }

  async getConfiguracaoAplication(){
    const configuracoes = await buscarConfiguracoesAplication();
    this.props.adicionarConfiguracaoAplication({configuracoes});
  }

  initWebSocket(){
    const { socket } = this.props;

    if(socket){       
      this.setState({ initWebSocketAlert: true})
    }
  }

  gerarAlert(alerta){
    this.openNotification(alerta.type, "Alerta", alerta.msg);
  } 
  InitialAplication
  openNotification(type, title, description) {   
    notification[type || 'info']({
      message: title || 'Aviso',
      description: description,
    });
  }

  checkStatusWebsocket = () => {
    const { socket } = this.props;
    const { initWebSocketAlert } = this.state;

    if(!socket){
      if(initWebSocketAlert){
        this.setState({initWebSocketAlert: false})
      }
   
      setTimeout(async () => {
        if(!this.props.socket || !this.props.socket.connected){
          await websocketController();    
        }
        this.checkStatusWebsocket();
      }, 10000);  
    }
  }

  async pegarInformacaoUsuario(){
    this.refreshToken();
    websocketController();
    const json = await me();
    const usuario = {
      roles: json.listaDePemissoes.map(element => element.permissao.descricao),
      id: json.id,
      nome: json.userName,
      avatar: `data:image/png;base64,${json.avatar}`
    }
  
    const configuracaoUsuario = {
      permissao: true,
      usuario
    }
    this.props.adicionarConfiguracao(configuracaoUsuario);
    this.setState({loadingPage: false})
  }

   refreshToken = async () => {
    await this.pegarToken()
  }

  getTime(){
    const { tokenExpiraIn, pegouTokenAoIniciarPagina } = this.state;
    if(pegouTokenAoIniciarPagina){
      return tokenExpiraIn - 300;
    } else {
      return 0;
    }
  }

  pegarToken(){
    setTimeout(async () => {
      const token = await  refreshToken();
      if(token){
        sessionStorage.setItem('tokenHash', token.token);   
      }  
      this.setState({
        pegouTokenAoIniciarPagina: true
      })
      this.refreshToken();    
    }, this.getTime());   
  }

  render() {  
    const { loadingPage = false, mudarRota = false } = this.state;

    if (mudarRota) {
      return <Redirect to="/initial-application" />;
    }
    
    return(
      <>
        {
          loadingPage ? <SpinCuston /> :
          <Switch>
            <PrivateRoute path="*/carteira" component={LayoutPage} />      
            <PrivateRoute path="*/initial-application" component={InitialApplication} />
            <DefaultPrivateRouter path="/*" />
          </Switch>
        }
      </>
    );
  }
}

const mapStateToProps = state => ({ 
  collapsed: state.menu.collapsed,
  socket: state.websocket.socket
})
const mapDispatchToPros = dispatch => bindActionCreators({
  ...actionsMenu, 
  ...actionsConfiguracao,
  ...actionsConfiguracaoAplication,
  ...actionsAtivo
}, dispatch)

export default connect(mapStateToProps,mapDispatchToPros)(MainContainer)
