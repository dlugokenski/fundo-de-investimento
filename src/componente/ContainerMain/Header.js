import React from 'react';
import { Layout, Icon, Menu, Switch, Dropdown, Tooltip, Progress } from 'antd';
import { Route, Redirect } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { isMobile } from 'react-device-detect';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as actions } from 'Ducks/configuracaoAplication';

import loggoutSystem from 'Shared/util';

const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;
const { Header } = Layout;

const StyleHeader = styled(Header)`
  position: sticky;
  top: 0;
  max-width: 100%;
  z-index: 1;
  width: 100%;
  background: #fff;
  padding: 0;
  max-height: 48px;

  display: flex;
  justify-content: end;
  background: white !important;
  border: 0px;

  .ant-menu-submenu,
  .ant-dropdown-trigger {
    margin-right: 20px;
  }

  a {
    color: #5a5a5a;

    &:hover {
      color: #0258d1;
      border-bottom: 2px solid #0258d1;
    }
  }

  @media (max-width: 400px) {
    height: 0px;
    width: 100%;
  }
`;

const StyleMenu = styled(Menu)`
  display: flex;
  justify-content: end;

  li {
    :last-child(-1) {
      //  float: right;
      margin-right: 30px;
    }
  }
`;

const StyleSubMenu = styled(SubMenu)`
  div {
    padding-left: 0;
  }

  @media (max-width: 400px) {
    margin-right: 10px;

    .ant-menu-submenu .ant-menu-submenu-horizontal {
      max-height: 30px;
    }
  }
`;

const StyleSubMenuCollapsed = styled(SubMenu)`
  max-height: 48px;
  float: left;

  .ant-menu-submenu-title {
    padding: 0px !important;
    > span {
      i {
        line-height: 10px !important;
      }
      .anticon {
        line-height: 10px;
      }
    }
  }

  @media (max-width: 400px) {
    max-height: 50px;
    max-width: 60px;
  }
`;
const blink = keyframes`
  0%, 100% { color: rgba(0, 128, 0, 0.5); }
  50% { color: green; }
`;

const StyleDivProgress = styled.div`
  margin-right: 20px;
  width: 180px;
  display: flex;
  flex-direction: row;
  align-items: center;

  span {
    margin-right: 5px;
    font-size: 12px;
  }
`;

const BlinkingText = styled.span`
  animation: ${blink} 2s infinite;
`;

const TextColorGreen = styled.span`
  color: green;
`;

const optionsVisibleCotacao = {
  RUNING: 'RUNING',
  STOP: 'STOP',
  FINISHED: 'FINISHED'
}

class HeaderPage extends React.Component {
  state = {
    visibleMenuConfig: false,
    stateVisibleContentCotacao: optionsVisibleCotacao.STOP
  };

  componentDidUpdate(previusProps){
    if(previusProps != this.props){
      if(this.props.coletaCotacaoEmAndamento){
        this.setState({
          stateVisibleContentCotacaostateVisibleContentCotacao: optionsVisibleCotacao.RUNING
        })
      }

      if(previusProps.coletaCotacaoEmAndamento && !this.props.coletaCotacaoEmAndamento){
        this.setState({
          stateVisibleContentCotacaostateVisibleContentCotacao: optionsVisibleCotacao.FINISHED
        })

        setTimeout(async () => {
          this.setState({
            stateVisibleContentCotacaostateVisibleContentCotacao: optionsVisibleCotacao.STOP
          })  
        }, 5000);   
      }
       
    }
  }

  showContentColetandoCotacao = () => {
    const { totalColetadoEmPorcentagem = 0 } = this.props;
    const { stateVisibleContentCotacaostateVisibleContentCotacao } = this.state;
    
    if(stateVisibleContentCotacaostateVisibleContentCotacao === optionsVisibleCotacao.RUNING){
      return(
        <StyleDivProgress>
          <BlinkingText>Coletando cotação</BlinkingText>   
          <Progress 
            width={35}
            type="circle"
            percent={ totalColetadoEmPorcentagem } 
            size="small" 
            status="active"
          />
        </StyleDivProgress>    
      )
    } else if(stateVisibleContentCotacaostateVisibleContentCotacao === optionsVisibleCotacao.FINISHED){
      return(
        <TextColorGreen>
          <BlinkingText>Coleta de cotação finalizada</BlinkingText> 
        </TextColorGreen>    
      )
    } else {
      return <></>
    } 
  }

  renderMenus = () => (
    <Menu>
      <MenuItem
        onClick={(e) => {
          this.props.adicionarConfiguracaoAplication({
            exponentialGraph:
              this.props.exponentialGraph === 'true' ? 'false' : 'true',
          });
        }}
      >
        <Switch checked={this.props.exponentialGraph === 'true'} />
        Graficos exponeciais
      </MenuItem>
    </Menu>
  );

  renderRouter = () => {
    const { rota = '/' } = this.state;
    return <Route render={() => <Redirect to={rota} />} />;
  };

  sairSistema() {
    loggoutSystem();
    this.setState({ mudarRota: true });
  }

  handleVisibleChange = (flag) => {
    this.setState({ visibleMenuConfig: flag });
  };

  render() {
    const { nome = '', coletaCotacaoEmAndamento = false, totalColetadoEmPorcentagem = 0 } = this.props;
    const { mudarRota = false, visibleMenuConfig } = this.state;

    return (
      <StyleHeader collapsed={this.props.collapsed ? 'sim' : 'nao'}>
         {this.showContentColetandoCotacao()}
        <StyleMenu mode="horizontal">
          {isMobile && (
            <StyleSubMenuCollapsed
              title={
                <span>
                  <Icon
                    style={{
                      fontSize: '18px',
                      lineHeight: '64px',
                      padding: '0 24px',
                      cursor: 'pointer',
                      transition: ' color .3s',
                    }}
                    type={this.props.collapsed ? 'menu-unfold' : 'menu-fold'}
                    onClick={this.props.toggle}
                  />
                </span>
              }
            />
          )}
          <Dropdown
            trigger={['click']}
            visible={visibleMenuConfig}
            onVisibleChange={this.handleVisibleChange}
            overlay={this.renderMenus()}
          >
            <Tooltip placement="left" title="Configurações">
              <a onClick={(e) => e.preventDefault()}>
                <Icon type="setting" />
              </a>
            </Tooltip>
          </Dropdown>
          <StyleSubMenu
            title={
              <span>
                <Icon type="user" />
                {`Olá ${nome || ''}`}
              </span>
            }
          >
            <MenuItem>Alterar perfil</MenuItem>
            <MenuItem onClick={() => this.sairSistema()}>Sair</MenuItem>
          </StyleSubMenu>
        </StyleMenu>
        {mudarRota && this.renderRouter()}
      </StyleHeader>
    );
  }
}

const mapDispatchToPros = (dispatch) => bindActionCreators(actions, dispatch);
const mapStateToProps = (state) => ({
  nome: state.configuracaoUsuario.usuario.nome,
  socket: state.websocket.socket,
  exponentialGraph: state.configuracaoAplication.exponentialGraph,
  coletaCotacaoEmAndamento: state.ativos.coletaCotacaoEmAndamento,
  totalColetadoEmPorcentagem: state.ativos.totalColetadoEmPorcentagem,
  quantidadeDeCotacoesAindaNaoColetadaAinda: state.ativos.quantidadeDeCotacoesAindaNaoColetadaAinda,
});

export default connect(mapStateToProps, mapDispatchToPros)(HeaderPage);
