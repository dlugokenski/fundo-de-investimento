import React from 'react';
import { Layout } from 'antd';
import styled from 'styled-components';

import Menu from './Menu';

const { Sider } = Layout;

const StyleSider = styled(Sider)`
  overflow: auto;
  height: 100vh;
  left: 0;

  @media (max-width: 400px) {
    display: ${(state) => (state.collapsed ? 'none' : '')};
    margin-top: ${(state) => (state.collapsed ? '0px' : '48px')};
  }
`;

export default (props) => (
  <StyleSider
    {...props}
    theme="light"
    trigger={null}
    collapsible
    collapsed={props.collapsed}
  >
    <Menu collapsed={props.collapsed} />
  </StyleSider>
);
