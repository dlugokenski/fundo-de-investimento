import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom'
import {isMobile} from 'react-device-detect';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Creators as actionsMenu} from 'Ducks/menu'
import { 
    atualizarEventos, 
    atualizarDividendos, 
    atualizarNoticias, 
    atualizarCotacaoComGoogle,
    atualizarCodigoCvm,
    validarCodigosCvm
  } from 'Provider/Ativo'

import Avatar from 'Componente/avatar';

const SubMenu = Menu.SubMenu;
const MenuItem = Menu.Item;

class MenuComponent extends React.Component {
    state = {};
    
    changeCollapsed(){
        if(isMobile){
            this.props.changeCollapsed(true)
        }        
    }

    checkPermissionAdmin(){
        const { usuarioLogado } = this.props;
        const permission = usuarioLogado.usuario && usuarioLogado.usuario.roles && usuarioLogado.usuario.roles.find(element => element === 'ROLE_ADMIN')
        if(permission){
            return true;
        } else {
            return false;
        }
    }

    render(){
        const { collapsed, carteira } = this.props;
        const isAdmin = this.checkPermissionAdmin();

        return(
            <React.Fragment>
                <center><Avatar collapsed={collapsed} /></center>
                <hr />
                <Menu 
                    theme="light" 
                    defaultSelectedKeys={['1']} 
                    mode="inline"
                    defaultOpenKeys={['sub1', 'sub2']}
                >
                    <SubMenu 
                        key="sub1"
                        title={<span><Icon type="pie-chart" /><span>Carteira</span></span>}
                    >
                        <MenuItem onClick={() => this.changeCollapsed()} disabled={!carteira || Object.keys(carteira).length === 0 || !carteira.id}>
                            <span>Minha Carteira</span>
                            <Link to="/main/carteira/atual"  />
                        </MenuItem>
                        {isAdmin && (
                            <MenuItem  
                                disabled={!carteira || Object.keys(carteira).length === 0 || !carteira.id}  
                                onClick={() => this.changeCollapsed()} 
                            >
                                <span>Ativos</span>
                                <Link to="/main/carteira/atual/ativo" />
                            </MenuItem>
                        )}    
                        <MenuItem 
                            disabled={!carteira || Object.keys(carteira).length === 0 || !carteira.id}   
                            onClick={() => this.changeCollapsed()}
                        >
                            <span>Operações</span>
                            <Link to="/main/carteira/atual/operacao" />
                        </MenuItem>
                    </SubMenu>    
                    {isAdmin && (
                        <SubMenu 
                            key="sub2"
                            title={<span><Icon type="tool" /><span>Administração</span></span>}
                        >
                            <MenuItem onClick={() => atualizarCotacaoComGoogle(false)}>
                                <span>Att. cotações</span>
                            </MenuItem>
                            <MenuItem onClick={() => atualizarCotacaoComGoogle(true)}>
                                <span>Att. cotações ativas</span>
                            </MenuItem>
                            <MenuItem onClick={() => atualizarDividendos()}>
                                <span>Att. dividendos</span>
                            </MenuItem>
                            <MenuItem onClick={() => atualizarNoticias()}>
                                <span>Att. noticias</span>
                            </MenuItem>
                            <MenuItem onClick={() => atualizarEventos()}>
                                <span>Att. evento</span>
                            </MenuItem>
                            <MenuItem onClick={() => atualizarCodigoCvm()}>
                                <span>Att. cod. CVM</span>
                            </MenuItem>
                            <MenuItem onClick={() => validarCodigosCvm()}>
                                <span>Validar cod. CVM</span>
                            </MenuItem>
                            
                        </SubMenu>     
                    )}
                    <MenuItem onClick={() => this.changeCollapsed()}>
                        <Icon type="fund" />
                        <span>Inserir carteira</span>
                        <Link to="/main/carteira/nova" />
                    </MenuItem>
                </Menu>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({ 
    carteira: state.carteira,
    usuarioLogado: state.configuracaoUsuario
})
const mapDispatchToPros = dispatch => bindActionCreators(actionsMenu, dispatch)

export default connect(mapStateToProps,mapDispatchToPros)(MenuComponent)

