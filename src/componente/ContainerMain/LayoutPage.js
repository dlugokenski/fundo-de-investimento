import React from 'react';
import { Layout, notification } from 'antd';
import { Switch } from 'react-router-dom';
import styled from 'styled-components';
import {isMobile} from 'react-device-detect';
import Loadable from 'react-loadable';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Creators as actionsConfiguracao} from 'Ducks/configuracaoUsuario'
import { Creators as actionsMenu} from 'Ducks/menu'
import { Creators as actionsConfiguracaoAplication} from 'Ducks/configuracaoAplication'
import { Creators as actionsAtivo} from 'Ducks/ativos'


import { PrivateRoute, DefaultPrivateRouter } from 'RoutersPage/PrivateRoute' 
import SideMenu from './SideMenu'
import HeaderComponent from './Header'
import SpinCuston from 'Componente/Spin';

const { Content } = Layout;

const StyleContent = styled(Content)`
  margin: 24px 16px;
  max-height: calc(100vh - 100px);

  @media (max-width: 400px) {
    z-index: ${state => state.collapsed === 'sim'  ? '0' : '-1'} !important;
    padding: 24px 0px; 
  }
`;

const LayoutStyle = styled(Layout)`
  background: transparent;

  .ant-layout {
    background: #f8f8f8;
  }

  .ant-layout-content{
    @media (max-width: 400px) {
      margin: 0px !important;    
    }
  }

  .ant-layout-header {
    height: 64px;
    padding: 0 0px;
}

  @media (max-width: 400px) {
    margin-left: 0px;    
  }
`;

const TradePage = Loadable({
  loader: () => import('Pages/historico'),
  loading: () => <SpinCuston />,
});

const CarteiraAtual = Loadable({
  loader: () => import('Pages/carteiraAtual'),
  loading: () => <SpinCuston />,
});

class LayoutPage extends React.Component {

  state = {};

  componentDidMount(){
    if(isMobile){
      this.props.changeCollapsed(true)
    }
  }

  componentDidUpdate(previusProps){
    if(previusProps != this.props){
      if(previusProps.socket != this.props.socket){
        this.initObservableWebSocket();
      }
    }  
  }

  initObservableWebSocket(){
    const { socket, listaDeCotacoesColetadas = [], quantidadeTotalDeCotacaoColetar } = this.props;

    if(socket){;  
      socket.subscribe('/user/queue/send_alert', data => this.gerarAlert(JSON.parse(data.body)));
      socket.subscribe('/user/queue/starting-collecting-quotes', data => {
        const body = JSON.parse(data.body);
        this.props.alterarStatusCotacao(true);
        this.props.alterarQuantidadeTotalDeCotacaoColetar(body);
      });
      socket.subscribe('/user/queue/quote-colleted', data => {
        const body = JSON.parse(data.body); 
        this.props.alterarListaDeCotacoesColetadas([body, ...listaDeCotacoesColetadas]);
      });
      socket.subscribe('/user/queue/number-of-quotes-missing', data => {
        const body = JSON.parse(data.body);
        this.props.alterarQuantidadeDeCotacoesAindaNaoColetada(body);
      });
      socket.subscribe('/user/queue/finished-collecting-quotes', data => {
        this.props.alterarStatusCotacao(false);
      }); 
    }
  }

  gerarAlert(alerta){
    this.openNotification(alerta.type, "Alerta", alerta.msg);
  } 

  openNotification(type, title, description) {   
    notification[type || 'info']({
      message: title || 'Aviso',
      description: description,
    });
  }

  toggle = () => {
    const { collapsed } = this.props;
    this.props.changeCollapsed(!collapsed)
  }

   render() {  
    const { collapsed } = this.props;

    return (
      <LayoutStyle>
        <SideMenu collapsed={collapsed} />
        <Layout>
          <HeaderComponent toggle={this.toggle} collapsed={collapsed} />
          <StyleContent collapsed={collapsed ? "sim" : "nao"}>
            <Switch>
              <PrivateRoute path="*/nova" component={TradePage}/>
              <PrivateRoute path="*/atual" component={CarteiraAtual}/>        
              {/* <DefaultPrivateRouter path="/*" /> */}
            </Switch>
          </StyleContent>
        </Layout>
      </LayoutStyle>
    );
  }
}

const mapStateToProps = state => ({ 
  collapsed: state.menu.collapsed,
  socket: state.websocket.socket,
  quantidadeTotalDeCotacaoColetar: state.ativos.quantidadeTotalDeCotacaoColetar,
  listaDeCotacoesColetadas: state.ativos.listaDeCotacoesColetadas,
})
const mapDispatchToPros = dispatch => bindActionCreators({
  ...actionsMenu, 
  ...actionsConfiguracao,
  ...actionsConfiguracaoAplication,
  ...actionsAtivo
}, dispatch)

export default connect(mapStateToProps,mapDispatchToPros)(LayoutPage)
