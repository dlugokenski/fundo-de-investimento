import React from 'react';
import {  Dropdown, Menu, Button } from 'antd';
import styled from 'styled-components';

const StyleDropdown = styled(Dropdown)`
    @media (max-width: 400px) {
        position: fixed;
        right: 12px;
        top: 55px;
    }
`;

const StyleDropdownItens = styled(Menu.Item)`
    background-color: transparent;
`;

const StyleMenu = styled(Menu)`
    background-color: transparent !important;
    box-shadow: none !important;
`;

const StyleComponent = styled.div`
    .ant-dropdown{
        left: 296px !important;
        position: fixed;
    }
`;  



export default (props) => {

    const menu = (           
        <StyleMenu>           
            {props.buttons.map(
                button => (
                    <StyleDropdownItens key={button.icon}> 
                        <Button
                            shape="circle"
                            type="primary"
                            key={`btn${button.icon}`}
                            icon={button.icon}
                            title={button.title}
                            onClick={() => button.onClick()}
                            disabled={button.disabled}
                            style={button.style}
                            size={'large'}
                        />
                    </StyleDropdownItens>
                )
            )}           
        </StyleMenu>
    )

    return(
        <StyleComponent id="button-dropdown-group">
            <StyleDropdown overlay={menu} trigger={['click']}  getPopupContainer={() => document.getElementById('button-dropdown-group')}>
                <Button type="primary" shape="circle" icon="tags" size={'large'} />
            </StyleDropdown>
        </StyleComponent>
    )
}

