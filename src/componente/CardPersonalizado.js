import React from 'react';
import { Card, Button, Tooltip } from 'antd';
import styled from 'styled-components';

const { Group } = Button;

const ButtonContainer = styled.div`
  position: absolute;
  top: 10px;
  right: -120px;
  width: 120px;
  z-index: 10;

  .ant-btn-group {
    display: flex;
    flex-direction: column;
  }

  .ant-btn {
    overflow: hidden;
    width: 50px;
    border-radius: 0 !important;
    margin-left: 0 !important;
    margin-bottom: 1px;
  }

  .ant-btn:hover {
    width: 100px;
  }

  .ant-btn > span {
    display: none;
  }

  .ant-btn:hover > span {
    display: inline;
  }

  .ant-btn-group > .ant-btn:first-child:not(:last-child) {
    border-bottom: 0;
  }
`;

export default props => (
    <div>
      <Card title={props.titleCard}>
        {props.children}
      </Card>
      <ButtonContainer>
        <Group>
          {props.buttons.map(( element, index) => (
            <Button 
                key={index}
                type="primary" 
                icon={element.icon}
                onChange={element.onChange}
            >
                {element.titleTooltip}
            </Button>
          ))}
        </Group>
      </ButtonContainer>
    </div>
)


