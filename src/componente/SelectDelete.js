import React from 'react';
import { Menu, Dropdown, Icon, Modal, Tooltip, Form } from 'antd';
import styled from 'styled-components';

const { confirm } = Modal;
const FormItem = Form.Item;

const FormStyle = styled(FormItem)`
    .ant-form-item-control{
       line-height: unset;
    }
`;

const StyleIcon = styled.div`
    padding-left: 5px;
    display: flex;
    align-items: center;
    .anticon{
        margin-right: ${props => props.extra ? '1px' : '0px'}; 
        float: right;
    }

    .ant-rate {
        font-size: 17px;
    }

    ${props => props.iconDelete && 'padding-bottom: 3px;'}
`;

const DivLabel = styled.div`
    width: 220px;
    display: inline;
    width: 100%;
    margin-left: -5px;
`;

const DivSelect = styled.div`
    background: white;
    padding: 5px 5px 5px 5px;
    border: 1px solid green;
    border-radius: 4px;
    display:  inline-block;
    width: 250px;

    .ant-rate{
        float: right;
        margin-top: -7px;
        padding-right: 3px;
    }
`;

export default class SelectDelete extends React.Component {

    state = {
        itemSelecionado: ""
    }

    componentDidMount(){
        const { value, defaultLabel = ''} = this.props;
        this.setValue(value);
        this.setState({
            defaultLabel            
        })
    }

    componentDidUpdate(previusProps){
        if(previusProps != this.props){
            if(previusProps.value){
                this.setValue(previusProps.value)
            }
        }
    }

    setValue(value){
        const { labelSelected = '' } = this.props;
        let label = '';
        label += labelSelected ? `${labelSelected} `  : '';
        label += value ? value : '';

        if(label != ''){
            this.setState({
                itemSelecionado: label            
            })
        }
    }

    showConfirm = (item) => {
        const { labelModalRemove = '' } = this.props;
        confirm({
          title: labelModalRemove,
          onOk() {
            return item.handleChangeDelete(item.value)
          },
          onCancel() {},
        });
    }

    itemSelect(item){
        this.setValue(item.label);
        item.handleChangeSelect(item.value)
    }
    
    render(){
        const { itemSelecionado } = this.state;
        const { defaultLabel = '', value = '', extraSelecionadoSelect, labelFomr = '' } = this.props;

        const menu = (           
            <Menu>
                {this.props.itens.map((element, index) => (
                    <Menu.Item key={index} style={{display: 'flex'}}> 
                        <DivLabel onClick={() => this.itemSelect(element)}>
                            {element.label} 
                        </DivLabel>
                        {element.extraItem && element.extraItem.map((element, indexTwo) => (                  
                            <StyleIcon extra={true} key={`${index}${indexTwo}`}>                
                                {element}
                            </StyleIcon>    
                        ))}
                        <StyleIcon iconDelete={true}>  
                            <Tooltip placement="top" title="Apagar" key={`${index}${element.value}`} >
                                <Icon type="delete" fill="primary" onClick={() => this.showConfirm(element)} />
                            </Tooltip>            
                        </StyleIcon>
                    </Menu.Item>
                ))}
            </Menu>
            
        )
        return(
            <FormStyle label={labelFomr}>
                <Dropdown overlay={menu} trigger={['click']}>
                    <DivSelect>
                        <a onClick={e => e.preventDefault()}>
                            {value ? (
                                <React.Fragment>
                                    {itemSelecionado} {extraSelecionadoSelect}
                                </React.Fragment>
                            ) : (
                                <React.Fragment>
                                    {defaultLabel}<Icon style={{ float: 'right', marginRight: '5px', marginLeft: '5px', marginTop: '3px'}} type="down" />
                                </React.Fragment>
                            )}

                        </a>
                    </DivSelect>
                </Dropdown>
            </FormStyle>
        ) 
    }    
}


