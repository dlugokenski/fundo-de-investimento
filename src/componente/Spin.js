import React from 'react';
import { Spin } from 'antd';
import styled from 'styled-components';

const StyleSpin = styled(Spin)`
    height: 10em;
    display: flex !important;
    align-items: center;
    justify-content: center;
`;

export default () => (
    <StyleSpin />
);
