import React, { forwardRef } from "react";
import styled from 'styled-components';
import { InputNumber as InputNumberAntd } from 'antd';

const StyleInputNumber = styled(InputNumberAntd)`
    width: 100% !important;
    
    .ant-input-number-handler-wrap{
        display: none;
    }
`;

const InputNumber  = props => (
    <StyleInputNumber 
        disabled={props.disabled}
        onChange = {event => props.onChangeValue(event)}
        value = {props.value}
    />
);

const InputNumberForm = forwardRef((props, ref) => { 
    return (
        <StyleInputNumber 
            {...props}
            ref={ref}
            disabled={props.disabled ? props.disabled : null}
        />
    );
});

export {InputNumber, InputNumberForm};