import React, { forwardRef } from "react";
import { Input } from "antd";
import ReactInputMask from "react-input-mask";

const InputMask = forwardRef((props, ref) => {
  return (
    <ReactInputMask {...props}>
      {(inputProps) => (
        <Input
          {...inputProps}
          ref={ref}
          disabled={props.disabled ? props.disabled : null}
        />
      )}
    </ReactInputMask>
  );
});

{
  /* <FormItem label="Valor unitario cota:">
{getFieldDecorator('teste', {
  rules: [{ required: true, message }]
})(
  <InputMask
    mask="(99) 99999-9999"
    placeholder="(__) _____-____"
  />
)}
</FormItem>  */
}

export default InputMask;
