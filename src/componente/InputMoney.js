import React, { forwardRef } from "react";
import CurrencyInput from "react-currency-input";
import styled from "styled-components";

const StyleCurrencyInput = styled(CurrencyInput)`
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  font-variant: tabular-nums;
  line-height: 1.5;
  list-style: none;
  font-feature-settings: "tnum", "tnum";
  position: relative;
  display: inline-block;
  width: 100%;
  height: 32px;
  padding: 4px 11px;
  color: rgba(0, 0, 0, 0.65);
  font-size: 14px;
  line-height: 32px;
  background-color: #fff;
  background-image: none;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  transition: all 0.3s;

  :focus,
  :hover {
    border-color: #1890ff;
    outline: 0;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    box-sizing: border-box;
  }
`;

const InputMoney = (props) => (
  <StyleCurrencyInput
    className="ant-input"
    decimalSeparator=","
    thousandSeparator="."
    precision="2"
    prefix="R$ "
    allowNegative={true}
    value={props.amount}
    onChangeEvent={(event) =>
      props.onChangeValue(event.target.value, props.typeChange)
    }
    disabled={props.disabled}
  />
);

const InputMoneyForm = forwardRef((props, ref) => {
  return (
    <StyleCurrencyInput
      {...props}
      ref={ref}
      className="ant-input"
      decimalSeparator=","
      thousandSeparator="."
      precision="2"
      prefix="R$ "
      allowNegative={true}
      disabled={props.disabled}
    />
  );
});

export { InputMoney, InputMoneyForm };
