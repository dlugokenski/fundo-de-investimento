import styled from 'styled-components';
import { isMobile } from 'react-device-detect';

const StyledContainer = styled.div({
  display: ({ props }) => props.display || 'flex',
  padding: ({ props }) => props.padding || props.p || '0',
  flexWrap: ({ props }) => props.flexWrap,
  margin: ({ props }) => props.margin || props.m || (isMobile ? '5px' : '0px'),
  height: ({ props }) => props.height,
  maxWidth: ({ props }) => props.maxWidth || props.mw || 'unset',
  minWidth: ({ props }) => props.minWidth,
  overflow: ({ props }) => props.overflow,
  width: ({ props }) => props.w || props.width || '100%;',
  minHeight: ({ props }) => props.minHeight,
  flexDirection: ({ props }) => props.flexDirection || 'unset',
  alignItems: ({ props }) => props.alignItems || 'unset',
  flex: ({ props }) => props.flex || 'unset',
  color: ({ props }) => props.color || props.c,
  justifyContent: ({ props }) => props.justifyContent || 'unset',
  zIndex: ({ props }) => props.zIndex,
  position: ({ props }) => props.position,
  backgroundColor: ({ props }) => props.backgroundColor,
  border: ({ props }) => props.border,
  borderRadius: ({ props }) => props.borderRadius,
  borderRight: ({ props }) => props.borderRight,
  borderLeft: ({ props }) => props.borderLeft,
  borderBottom: ({ props }) => props.borderBottom,
  borderTop: ({ props }) => props.borderTop,
  whiteSpace: ({ props }) => props.whiteSpace,
  backgroundImage: ({ props }) =>
    props.backgroundImage && `url(${props.backgroundImage})`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  textAlign: ({ props }) => props.textAlign,
  gap: ({ props }) => props.gap,
  top: ({ props }) => props.top,
});

export default StyledContainer;
