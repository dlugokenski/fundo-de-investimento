import React from 'react';
import { DatePicker, Popover, Button, Col, Tooltip, Form, Row, Input, Card, notification } from 'antd';
import moment from 'moment';
import locale from 'antd/lib/date-picker/locale/pt_BR';

import { converterDinheiroParaFloat } from 'Util/Formatacao'
import { InputMoney } from 'Componente/InputMoney'

const { MonthPicker } = DatePicker;
const FormItem = Form.Item;

const dateFormat = 'MM/YYYY';

function disabledDate(current, dataLimiteInferior, dataLimiteSuperior) {
    if(current && dataLimiteInferior && dataLimiteSuperior){
        return current && ((current > dataLimiteSuperior.endOf('day') || current < dataLimiteInferior.endOf('day') ));
    }
}
class RangedFilter extends React.Component {

    state={};
  
    componentDidMount(){
      this.iniciarPagina();
    }

    componentDidUpdate(nextProps){
        if(nextProps !== this.props ){
            if(nextProps.menorValor != this.props.menorValor || nextProps.maiorValor != this.props.maiorValor){
                this.iniciarPagina();
            }
        }         
    }
  
    iniciarPagina(){
        const { menorValor = 0, maiorValor = 0 , form: { setFieldsValue } } = this.props;
        
        let { maiorData, menorData } = this.props;

        const valorMaximoState = maiorValor.toString().replace(".", ",");
        const valorMinimoState = menorValor.toString().replace(".", ",");
        const limiteInferiorSelecionado = menorData ? moment(menorData, dateFormat) : null;
        maiorData = maiorData ? moment(maiorData, dateFormat) : null;
        menorData = menorData ? moment(menorData, dateFormat) : null;

        if(maiorData && menorData){
            setFieldsValue({
                dataFinal:  maiorData,
                dataInicial: menorData
            }); 
        }       

        this.setState({ menorValor , maiorValor, valorMinimoState, valorMaximoState, maiorData, menorData, limiteInferiorSelecionado })
    }
    
    openNotification = (type = 'warning', title = "", description = "") => {
        notification[type]({
          message: title,
          description: description,
        });
    }

    onOk = () => {
        const { validateFields } = this.props.form;
        const { valorMinimoState, valorMaximoState, maiorData, menorData} = this.state;
        let dataInicial = {}
        const dataFinal = {}

        validateFields(async (err, values) => {

            if (!err) {
               if(maiorData && menorData){
                    dataInicial = {
                        mes: (moment(values.dataInicial).format('MM')),
                        ano: (moment(values.dataInicial).format('YYYY'))
                    }
                    dataFinal = {
                        mes: (moment(values.dataFinal).format('MM')),
                        ano: (moment(values.dataFinal).format('YYYY'))
                    }
                }   
                
                const valorMinimo = converterDinheiroParaFloat(valorMinimoState);
                const valorMaximo = converterDinheiroParaFloat(valorMaximoState);
                
                let formularioComErros = false;
                if(values.dataInicial && values.dataFinal){
                    if(values.dataInicial > values.dataFinal){
                        formularioComErros = true;
                        this.openNotification('error',"Alerta",'O periodo inicial deve ser menor que o periodo final!' )
                    }
                }

                if(valorMinimo && valorMaximo){
                    if(valorMinimo > valorMaximo){
                        formularioComErros = true;
                        this.openNotification('error',"Alerta",'O valor minimo deve ser menor que o valor maximo!' )
                    }
                }

                if(!formularioComErros){
                    this.props.onOk({
                        filtros: {
                            filtrar: true,
                            values: {dataInicial, dataFinal, valorMinimo, valorMaximo}
                        }
                    });
                }             
            }
        });          
    }

    chengeValueMoney = (value, typeChange) => {
        if(typeChange === "menorValor"){
            this.setState({valorMinimoState: value}) ;
        } else  if(typeChange === "valorMaximo"){
            this.setState({ valorMaximoState: value}) ;
        }
    }

    resetarValores(){
        this.iniciarPagina();
        this.props.onOk({ 
            filtros: {
            filtrar: false,
            values: {}
        } })
    }

    buttonResetarValores = () => (
        <Tooltip placement="left" title="Resetar valores">
            <Button 
                style={{ float: "right", position: 'relative'}}
                type="primary" 
                icon="reload"
                shape="circle" 
                onClick={() => this.resetarValores()}
            />
        </Tooltip>
    )

    conteudoFiltro = () => {
        const { getFieldDecorator } = this.props.form;
        const { valorMinimoState , valorMaximoState, maiorData, menorData, limiteInferiorSelecionado } = this.state;

        
        return(
            <Card 
                style={{border:'0px' , margin: '-12px -16px'}} 
                title="Aplicar filtros gráfico" 
                extra={this.buttonResetarValores()}
            >                
                <Form>                         
                    {maiorData && menorData && (
                        <Row gutter={16}>
                            <Col md={12}>
                                <FormItem label="Periodo inicial" style={{ marginTop: '-15px' }}>
                                    {getFieldDecorator('dataInicial', {})(
                                        <MonthPicker 
                                            placeholder='Periodo inicial'
                                            format={dateFormat}
                                            disabledDate={event => disabledDate(event, menorData, maiorData )}
                                            locale={locale}
                                        /> 
                                    )}   
                                </FormItem>                            
                            </Col>
                            <Col md={12}>
                                <FormItem label="Periodo final" style={{ marginTop: '-15px' }}>
                                    {getFieldDecorator('dataFinal', {})(
                                        <MonthPicker 
                                            placeholder='Periodo final'
                                            format="MM/YYYY"
                                            disabledDate={event => disabledDate(event, limiteInferiorSelecionado, maiorData )}
                                            locale={locale}
                                        />
                                    )} 
                                </FormItem>                                       
                            </Col>     
                        </Row> 
                    )}    
                    <Row gutter={16}>
                            <Col md={12}>
                                <FormItem label="Valor minimo" style={{ marginTop: '-15px' }}>
                                    {getFieldDecorator('valorMinimo', {})(
                                        <InputMoney amount={valorMinimoState} onChangeValue={this.chengeValueMoney} typeChange="menorValor" />
                                    )}                                
                                </FormItem>
                            </Col>
                            <Col md={12}>
                                <FormItem label="Valor maximo"  style={{ marginTop: '-15px' }}>
                                    {getFieldDecorator('valorMaximo', {})(
                                        <InputMoney amount={valorMaximoState} onChangeValue={this.chengeValueMoney} typeChange="maiorValor"/>
                                    )}                                 
                                </FormItem>
                            </Col>
                        </Row>  
                    <div style={{ position:'relative', float: 'right' }}>
                        <Button 
                            onClick = { () => this.onOk()}
                            type="primary" 
                        >
                            Aplicar
                        </Button>
                    </div>
                </Form>
            </Card>
        )
    }
  
    render() {     

        return (
            <Tooltip placement="left" title="Aplicar filtros grafico"  style={{ position: 'relative', float: 'right' }}>
                <Popover content={this.conteudoFiltro()} placement="bottomRight" trigger="click">
                    <Button 
                        disabled={false}
                        type="primary" 
                        shape="circle" 
                        icon="filter" 
                    />
                </Popover>
            </Tooltip> 
        );
    }
}

export default Form.create()(RangedFilter);