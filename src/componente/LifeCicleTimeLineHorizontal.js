import React from 'react';
import styled from 'styled-components';
import { Tooltip } from 'antd';

import imagenArrowBow from 'Imagens/arrowBow.png';

//https://stackoverflow.com/questions/16989585/css-3-slide-in-from-left-transition
//https://atitudereflexiva.wordpress.com/2013/08/22/bloqueando-copia-de-conteudo-de-pagina-com-jquery/

const StyleDivArrowImage = styled.div`
    background-image: url(${imagenArrowBow});
    position: relative;
    height: 20px;
    top: 5px;
    width: 100px !important;
    background-repeat: no-repeat;
    background-size: 100%; 
    background-position: center;
    opacity: 0.6;
    border: 0px;
    margin-bottom: 30px;
`;

const DivContainer = styled.div`
    display: -webkit-box;
    overflow-x: auto;
    justify-content: center;
    align-items: center;
    height: 100%;
`;

const DivConteudo = styled.div`
    width: 200px;
    height: 200px;
    border: #a6a6a6 2px solid;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;

    ${props => props.haveonclick && `
        cursor: pointer;
    `}
`;

const DivLinha = styled.div`
    width: 100px;
    height: 1px;
    border: #a6a6a6 1px solid;
`;

const DivConteudoLinha = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const DivConteudoCicle = styled.div`
    display: flex;
    flex-direction: column;
    width: 80%;
    top: -12px;
    position: relative;
`;

const DivTitleCicleTop = styled.div`
    font-family: "Times New Roman", Times, serif;
    font-size: 45px;
    color: black;
    font-weight: 600;
`;

const DivSubTitleCicleTop = styled.div`
    font-family: "Times New Roman", Times, serif;
    font-size: 15px;
    color: black;
    font-weight: bold;
`;

const DivResultCicle = styled.div`
    font-family: "Times New Roman", Times, serif;
    font-size: 15px;
    color: black;
    font-weight: bold;
    color: #5ea9ff;
`;

const DivConteudoCicleBot = styled.div`
    border-top: #a6a6a6 1px solid;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

const DivVariacao = styled.div`
    ${props => props.value > 0 && 'color: green;'}
    ${props => props.value < 0 && 'color: red;'}
    font-weight: bold;
`;

const divDefault = (title, tooltip, value) => (
    <div>
        <Tooltip placement="top" title={tooltip}>
            <DivSubTitleCicleTop> 
                {title}
            </DivSubTitleCicleTop>
        </Tooltip>  
        <DivResultCicle> 
            {value.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' })}
        </DivResultCicle>        
    </div>   
)

function calcularValorizacao(elementOne, elementTwo){
    return (((elementTwo * 100) / elementOne) - 100).toFixed(1);
}

export default props => (
    <DivContainer>              
        {props.listValores.map((element, index) => (    
            <React.Fragment key={Math.random()}>        
                <DivConteudo key={index} onClick={() => console.log('clicpu aqui')} haveonclick={props.onChange}>
                    {props.evolucao ? (
                        <React.Fragment key={Math.random()}>
                            <DivConteudoCicle>
                                <DivTitleCicleTop>
                                    {element.ano}
                                </DivTitleCicleTop>
                                <DivConteudoCicleBot>
                                    {divDefault('Total', 'Total do ano', element.total)}
                                    {divDefault('Média', 'Média mensal', element.media)}
                                </DivConteudoCicleBot>
                            </DivConteudoCicle>
                        </React.Fragment>                                        
                    ) : (
                        {index}      
                    )}
                    
                </DivConteudo>
                {props.listValores.length !== index + 1 && (
                    <DivConteudoLinha key={`${index}${element}`}>
                        {props.evolucao ? (
                            <React.Fragment key={Math.random()}>
                                <DivVariacao value={calcularValorizacao(props.listValores[index].total, props.listValores[index + 1].total)}>
                                    {`${calcularValorizacao(props.listValores[index].total, props.listValores[index + 1].total)}%`}
                                </DivVariacao>
                                <StyleDivArrowImage />
                            </React.Fragment>                                        
                        ) : (
                            <DivLinha key={`${index}${element}final`} />       
                        )}
                                                
                    </DivConteudoLinha>
                )}
            </React.Fragment> 
        ))}
    </DivContainer>     
);
