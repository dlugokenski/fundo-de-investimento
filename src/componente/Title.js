import React from 'react';
import styled from 'styled-components';

const DivTitle = styled.div`
    font-size: 16px;
    font-weight: 500;
    color: rgba(0, 0, 0, 0.85);
`;

export default props => (
    <DivTitle>
        {props.title}
    </DivTitle>
);