import React from "react";
import { Select } from "antd";
import styled from "styled-components";

const { Option } = Select;

const DivSelect = styled.div`
  display: flex;
  width: (4 / 4) * 1em;
  justify-content: space-between;

  span {
    font-weight: bold;
  }
`;

const StyleItemDropDown = styled.div`
    position: absolute;
    z-index: -1;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    background: ${(props) => {
      if (props && props.item) {
        const image = require(`../imagens/4x3/${props.item.isoCountry}.svg`);
        return `url(${image}) center center`;
      }
    }};
    opacity: 0.3;
    width: 100%;
    height: 100%;
    background-size: cover;
    background-repeat: no-repeat;
}`;

const StyleSelect = styled(Select)`
  .ant-select-selection {
    background-size: cover;
    background-image: linear-gradient(
        rgba(255, 255, 255, 0.7),
        rgba(255, 255, 255, 0.7)
      ),
     ${(props) => {
       if (props && props.item) {
         const image = require(`../imagens/4x3/${props.item.isoCountry}.svg`);
         return `url(${image}`;
       }
     }});
    ${(props) => props.item && `height: 75px;   transition: all 200ms ease-in;`}
    background-position: 50%;
    background-repeat: no-repeat;
  }
  
  .ant-select-selection__rendered{
    display: flex;
  }

  .ant-select-selection--single {
    height: 32px !important;
  }

  .ant-select-dropdown-menu-item {
    padding: 0px;
  }

  .ant-select-selection-selected-value {
    max-width: 90%;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
    margin-right: 10px;
    float: initial;
  }
`;

export default class SelectCountry extends React.Component {
  state = {
    itemSelected: undefined,
  };

  componentDidUpdate(nextProps) {
    if (nextProps.ativo != this.props.ativo) {
      this.setState({
        itemSelected: undefined,
        setedCountryAtivoPros: false,
      });
    }
    this.setDefaultValue();
  }

  setDefaultValue(nextProps) {
    const { setedCountryAtivoPros = false, itemSelected } = this.state;
    const {
      form: { setFieldsValue },
      ativo = {},
      itens,
    } = this.props;

    if (
      ativo &&
      Object.keys(ativo).length !== 0 &&
      ativo.countryWithListing &&
      ativo.countryWithListing.id &&
      itens &&
      itens.length > 0 &&
      !setedCountryAtivoPros
    ) {
      setFieldsValue({ country: ativo.countryWithListing.id });
      this.setState({
        itemSelected: itens.find((e) => e.id === ativo.countryWithListing.id),
        setedCountryAtivoPros: true,
      });
    }
  }

  render() {
    const { itemSelected } = this.state;
    const {
      form: { getFieldDecorator },
      message,
      itens,
    } = this.props;

    return (
      <React.Fragment>
        {getFieldDecorator("country", {
          rules: [{ required: true, message }],
        })(
          <StyleSelect
            dropdownClassName="dropdown-widhout-background"
            onChange={(e) => {
              this.setState({
                itemSelected: itens.find((b) => b.id === e),
              });
            }}
            item={itemSelected}
          >
            {this.props.itens.map((item, index) => (
              <Option key={index} value={item.id}>
                <DivSelect>
                  <StyleItemDropDown item={item} />
                  <span>{item.description}</span>
                </DivSelect>
              </Option>
            ))}
          </StyleSelect>
        )}
      </React.Fragment>
    );
  }
}
