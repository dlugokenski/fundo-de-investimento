import React from 'react';
import { Button, Drawer as AntdDrawer, Upload, Tooltip } from 'antd';
import styled from 'styled-components';
import {isMobile} from 'react-device-detect';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ButtonsDropDown from 'Componente/ButtonsDropDown';

const ButtonGroup = Button.Group;

const StyledDrawer = styled(AntdDrawer)`
  width: 0 !important;
  left: ${props => props.collapsed ? '82px' : '200px'};

  form{
    margin-bottom: -10px;
  }
  .content-form{
    height: calc(100vh - 200px);
    padding: 24px;
    overflow: auto;
  }

  .button-submit{
    width: 90% !important;
    margin-left: 15px;
  }

  &.ant-drawer-left .ant-drawer-content-wrapper,
  .ant-drawer-right .ant-drawer-content-wrapper {
    height: calc(100% - 70px);
    box-shadow: none !important;
  }

  .ant-drawer-body{
    padding: 0px;
    border-right: 1px solid #e3e3e3;
    border-bottom: 1px solid #e3e3e3;
  }

  .ant-drawer-wrapper-body::-webkit-scrollbar { 
    display: none;
  }

  .ant-drawer-content{
    position: initial;
    background: transparent;
  }

  .ant-drawer-body{
    background: white;
  }

  .ant-drawer-content-wrapper {
    width: ${props => (!props.close ? 'inherit' : '0 !important')};
    top: 71px;
    transition: all 0.2s ease-in-out;
    min-height: 400px;
  }

  .ant-drawer-left {
    height: auto;
  }

  .ant-drawer-header {
    padding: 14px 22px;
    position: sticky;
    top: 0px;
    z-index: 2;
  }

  .ant-drawer-title {
    font-size: 14px;
    line-height: 19px;
  }
  
  .ant-drawer-close-x {
    width: 44px;
    height: 39px;
    line-height: 43px;
  }

  @media (max-width: 992px) {
    left: 0;
    top: 95px;

    .ant-drawer-content-wrapper {
      height: auto;
      width: ${props => (props.close ? '0 !important' : 'calc(100% - 49px) !important')};
    }
    .ant-drawer-wrapper-body {
      max-height: 80vh;
    }
  }

  @media (max-width: 400px) {
    left: ${props => props.close && !props.collapsed ? '200px' : '0px'};
    left: ${props => props.close && props.collapsed && '200px'};

    .ant-drawer-content-wrapper {
      top: 48px;
    }

    .ant-btn-group{
      flex-direction: ${props => props.close && props.collapsed ? 'row-reverse' : 'column'} !important; 
      top: ${props => props.close && props.collapsed && '-22px'};
    }    
  }
`;

const StyledButtons = styled.div`
  position: absolute;
  top: 0px;
  right: -50px;

  .ant-btn-group {
    display: flex;
    flex-direction: column;
  }

  .ant-btn {
    overflow: hidden;
    width: 50px;
    border-radius: 0 !important;
    margin-left: 0 !important;
    margin-bottom: 1px;
  }

  .ant-btn > span {
    display: none;
  }

  @media (min-width: 992px) {
    top: 0px;
    right: -120px;
    width: 120px;

    .ant-btn:hover {
      width: 100px;
    }

    .ant-btn:hover > span {
      display: inline;
    }
  }

  .ant-btn-group > .ant-btn:first-child:not(:last-child) {
    border-bottom: 0;
  }
`;

/*
 ** VISIBLE = X
 ** CLOSE = << | >>
 */

class ComponentDrawer extends React.Component {
  render(){
    const { 
      title, 
      children, 
      onClose, 
      visible, 
      close, 
      width = 256, 
      buttons = [], 
      onVisible, 
      collapsed 
    } = this.props;

    const buttonsDropdownMenu = [
      {
        icon: 'close',
        label: 'Cadastrar',
        onClick: () => onVisible && onVisible(!visible),
      },
      {
        icon: 'arrows-alt',
        label: 'Expandir',
        onClick: () => onClose && onClose(!close),
      }
    ]

    return(
      <React.Fragment>
        {!visible ? null : (
          <React.Fragment>
            { isMobile && close && collapsed ? (
              <ButtonsDropDown buttons={buttonsDropdownMenu} />
            ) : (
             <StyledDrawer
                title={title}
                width={width}
                visible={visible}
                close={close ? 1 : 0}
                mask={false}
                destroyOnClose
                closable={false}
                placement="left"
                collapsed={collapsed ? 1 : 0}
              >
                {children}
                {/* RIGHT BUTTONS */}
                <StyledButtons>
                  <ButtonGroup>
                    <Button
                      icon="close"
                      type="primary"
                      onClick={() => onVisible && onVisible(!visible)}
                    >
                      {'Fechar'}
                    </Button>
                    <Button
                      type="primary"
                      icon={!close ? 'double-left' : 'double-right'}
                      onClick={() => onClose && onClose(!close)}
                    > 
                      {!close ? 'Ocultar' : 'Expandir'}
                    </Button>
                    {!close && buttons.map(
                      element => (
                        element.type === 'file' ? (
                          <Upload        
                            key={`btn${element.icon}1`}         
                            customRequest={() => (false)}
                            beforeUpload={element.onClick} 
                            showUploadList={false}
                          >
                            <Button 
                              type="primary"
                              key={`btn${element.icon}`}
                              icon={element.icon}
                              title={element.title}
                              disabled={element.disabled}
                              style={element.style}
                            > 
                              {element.label}
                            </Button>
                          </Upload>
                        ) : (
                          <Button
                            type="primary"
                            key={`btn${element.icon}`}
                            icon={element.icon}
                            title={element.title}
                            onClick={() => element.onClick()}
                            disabled={element.disabled}
                            style={element.style}
                          >
                            {element.label}
                          </Button>
                        )
                      )
                    )}
                  </ButtonGroup>
                </StyledButtons>
              </StyledDrawer>       
            )}
          </React.Fragment>   
        )}
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({ collapsed : state.menu.collapsed })

export default connect(mapStateToProps,null)(ComponentDrawer)