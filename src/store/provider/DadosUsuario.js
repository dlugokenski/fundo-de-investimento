import BaseProvider from './BaseProvide'

export const cadastrarUsuario = (params) => {
    return BaseProvider.POST(`usuario/cadastrar`, params);
};

export const cadastrarAvatarUsuario = (params) => {
    return BaseProvider.POST(`usuario/cadastrar/avatar`, params);
};

export const me = () => {
    return BaseProvider.GET(`usuario/me`);
} 
