const localeUserAcess = Intl.DateTimeFormat().resolvedOptions().timeZone;

const headersAuthenticed = () => ({
  Accept: 'application/json',
  'Content-Type': 'application/json',
  Authorization: `Bearer ${sessionStorage.getItem('tokenHash')}`,
  'X-FMP-TZ': localeUserAcess,
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': '*',
});

const headersPublic = () => ({
  Accept: 'application/json',
  'Content-Type': 'application/json',
  'X-FMP-TZ': localeUserAcess,
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': '*',
});

export default (path = 'defaultpath') => {
  if (path === 'login' || path.includes('public/')) {
    return {
      headers: headersPublic(),
    };
  } else {
    return {
      headers: headersAuthenticed(),
    };
  }
};
