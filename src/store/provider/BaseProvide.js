import store from 'Store';
import { Creators as actions } from 'Ducks/alert';
import { StatusCodes } from 'http-status-codes';

import config from './config';
import loggoutSystem from 'Shared/util';
import { VALID_REPONSE } from 'Util/Constantes';

const getUrlToFetch = (url) => {
  const urlDev = `/api/${url}`;
  const urlProd = `http://${process.env.REACT_APP_API_URL}/${url}`;
  return process.env.NODE_ENV === 'development' ? urlDev : urlProd;
};

const chegarErro = async (response, method, url) => {
  if (url !== 'login') {
    if (response) {
      if (
        response.status === StatusCodes.UNAUTHORIZED ||
        response.status === StatusCodes.FORBIDDEN ||
        response.error === VALID_REPONSE.INVALID_TOKEN ||
        response.error === VALID_REPONSE.AUTHENTICATION_REQUIRED
      ) {
        loggoutSystem();
        return false;
      } else if (
        ( response.status === StatusCodes.BAD_REQUEST || response.status === StatusCodes.INTERNAL_SERVER_ERROR ) &&
        response.json
      ) {
        response.json().then((data) => {
          if (data.message) {
            store.dispatch(
              actions.adicionarAlert({
                description: data.message,
              })
            );
          }
        });
        return false;
      } else if (response && response.status === StatusCodes.NOT_FOUND) {
        const res =
          response.status !== StatusCodes.NO_CONTENT && response.json
            ? await response.json()
            : undefined;
        throw res && res ? res : new Error(`Erro na chamada ${method}: ${url}`);
      }
    } else {
      return false;
    }
  } else {
    if (response && response.status === StatusCodes.UNAUTHORIZED) {
      return false;
    } else {
      return true;
    }
  }
  return true;
};

const GET = async (url) => {
  const api = getUrlToFetch(url);

  try {
    const response = await fetch(api, config());
    if (await chegarErro(response, 'GET', url)) {
      return await response.json().then((data) => data);
    }
  } catch (err) {
    throw err;
  }
};

const DELETE = async (url, body) => {
  const api = getUrlToFetch(url);

  try {
    const response = await fetch(api, {
      ...config(),
      method: 'DELETE',
      body: JSON.stringify(body),
    });
    if (await chegarErro(response, 'DELETE', url)) {
      return response.status !== StatusCodes.NO_CONTENT && response.json
        ? await response.json()
        : undefined;
    }
  } catch (err) {
    throw err;
  }
};

const POST = async (url, body) => {
  const api = getUrlToFetch(url);

  try {
    const response = await fetch(api, {
      ...config(url),
      method: 'POST',
      body: JSON.stringify(body),
    });
    if (await chegarErro(response, 'POST', url)) {
      return response.status !== StatusCodes.NO_CONTENT && response.json
        ? await response.json()
        : undefined;
    }
  } catch (err) {
    throw err;
  }
};

const UPLOAD = async (url, body) => {
  const api = getUrlToFetch(url);

  try {
    const response = await fetch(api, {
      headers: {
        Authorization: config().headers.Authorization,
      },
      method: 'POST',
      body,
    });
    if (await chegarErro(response, 'UPLOAD', url)) {
      return response.status !== StatusCodes.NO_CONTENT && response.json
        ? await response.json()
        : undefined;
    }
  } catch (err) {
    throw err;
  }
};

const BLOB = async (url) => {
  const api = getUrlToFetch(url);

  try {
    const response = await fetch(api, ...config());
    return await response.blob();
  } catch (err) {
    throw new Error(`Erro na chamada BLOB: ${url}`);
  }
};

export default {
  GET,
  POST,
  DELETE,
  UPLOAD,
  BLOB,
};
