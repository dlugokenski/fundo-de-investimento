import BaseProvider from './BaseProvide'

export const login = (parans) => {
    return BaseProvider.POST(`login`, parans);
} 

export const refreshToken = () => {
    return BaseProvider.GET('refreshtoken')
}
