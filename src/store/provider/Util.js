import BaseProvider from './BaseProvide'

export const efetuarProcessamentoDeDados = (params) => {
    return BaseProvider.UPLOAD(`carteira/processarDadosExcel`, params);
};

export const buscarConfiguracoesAplication = () => {
    return BaseProvider.GET(`config`);
};
