import BaseProvider from './BaseProvide'

export const consultarUsuariosAtivos = (search) => {
    return BaseProvider.POST("usuario/consultar/ativos", search);
};

export const cadastrar = (parans) => {
    return BaseProvider.POST(`public/usuario/cadastrar`, parans);
} 

export const buscarInformacoesUsuario = (idUsuario) => {
    return BaseProvider.GET(`usuario/consultar/${idUsuario}`);
};

export const cadastrarAvatarUsuario = (params) => {
    return BaseProvider.UPLOAD(`usuario/cadastrar/avatar`, params);
};

export const me = () => {
    return BaseProvider.GET(`usuario/me`);
};