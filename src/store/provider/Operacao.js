import BaseProvider from './BaseProvide'

export const apagarOperacao = (id) => {
    BaseProvider.DELETE(`operacao/delete/${id}`);
};

export const cadastrarOperacao = (operacao) => {
    return BaseProvider.POST("operacao/cadastrar", operacao);
};

export const lerPdfOperacao = (params) => {
    return BaseProvider.UPLOAD(`operacao/ler-pdf`, params);
};
