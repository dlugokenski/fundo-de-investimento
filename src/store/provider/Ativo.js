import BaseProvider from "./BaseProvide";

export const atualizarTodasAsAcoes = () => {
  BaseProvider.POST("cotacao/atualizar/acao/all");
};

export const atualizarCotacaoComGoogle = (status = false) => {
  BaseProvider.POST(`cotacao/atualizar/acao/google/all/${status}`);
};

export const atualizarDividendos = () => {
  BaseProvider.POST("cotacao/atualizar/all/dividendos");
};

export const atualizarDividendosAtivo = (ativo) => {
  BaseProvider.POST(`cotacao/atualizar/dividendos/${ativo}`);
};

export const atualizarCodigoCvm = () => {
  BaseProvider.POST(`ativo/atualizar_codigoscvm`);
};

export const validarCodigosCvm = () => {
  BaseProvider.POST(`ativo/validar/codigoscvm/all`);
};

export const atualizarNoticias = () => {
  BaseProvider.POST("noticia_ativo/atualizar/all");
};

export const atualizarEventos = () => {
  BaseProvider.POST("evento/atualizar/all");
};

export const buscarTiposAtivos = () => {
  return BaseProvider.GET(`tipo/consultar/all`);
};

export const buscarSegmentosAtivos = () => {
  return BaseProvider.GET(`segmento/consultar/all`);
};

export const cadastrarAtivoAcao = (ativo) => {
  return BaseProvider.POST("ativo/cadastrar/acao", ativo);
};

export const cadastrarAtivoFundoImobiliario = (fii) => {
  return BaseProvider.POST("ativo/cadastrar/fii", fii);
};

export const cadastrarTesouroDireto = (tesouro) => {
  return BaseProvider.POST("ativo/cadastrar/tesouro", tesouro);
};

export const seachAtivo = (value) => {
  return BaseProvider.POST("ativo/pesquisar", value);
};

export const apagarAtivo = (ativo) => {
  BaseProvider.DELETE("ativo/delete", ativo);
};

export const getAtivoApi = (tipo, value) => {
  return BaseProvider.POST(`ativo/pesquisar/api/${tipo}`, value);
};

export const consultarAtivo = (id) => {
  return BaseProvider.GET(`ativo/consultar/${id}`);
};

export const consultarAllCountryWithListing = () => {
  return BaseProvider.GET(`country-with-listing/consultar/all`);
};

export const findAllCompanies = () => {
  return BaseProvider.POST("ativo/find-all-companies");
};

export const findAllFII = () => {
  return BaseProvider.POST("ativo/find-all-fii");
};

export const consultarSeAplicacaoJaIniciou = () => {
  return BaseProvider.GET("ativo/find/init-aplication");
};