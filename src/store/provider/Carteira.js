import BaseProvider from "./BaseProvide";

export const cadastrarNovaCarteira = (parans) => {
  return BaseProvider.POST("carteira/cadastrar", parans);
};

export const buscarCarteira = (numeroCarteira) => {
  return BaseProvider.GET(`carteira/consultar/${numeroCarteira}`);
};

export const buscarCarteiraFavoritaOuUltimaCadastrada = () => {
  return BaseProvider.GET(`carteira/consultar/favorita_ou_ultima_cadastrada`);
};

export const buscarListaDeCarteiras = (idUsuario) => {
  return BaseProvider.GET(`carteira/consultar/carteiras/${idUsuario}`);
};

export const buscarImpostoDeRenda = (idCarteira) => {
  return BaseProvider.GET(`carteira/${idCarteira}/imposto-de-renda`);
};

export const buscarListaDeNoticias = () => {
  return BaseProvider.GET(`noticia_ativo/consultar`);
};

export const buscarCarteiraAtual = () => {
  return BaseProvider.GET(`carteira/consultar/atual`);
};

export const buscarAcoesCarteiraAtual = () => {
  return BaseProvider.GET(`carteira/consultar/composicao/acao`);
};

export const buscarTesouroCarteiraAtual = () => {
  return BaseProvider.GET(`carteira/consultar/composicao/tesouro`);
};

export const buscarFIICarteiraAtual = () => {
  return BaseProvider.GET(`carteira/consultar/composicao/fii`);
};

export const buscarComposicaoCarteiraAtual = () => {
  return BaseProvider.GET(`carteira/consultar/composicao`);
};

export const getListAporteMensal = () => {
  return BaseProvider.GET(`carteira/consultar/lista_aporte_mensal`);
};

export const buscarListTradeInCache = () => {
  return BaseProvider.GET(`carteira/consultar/lista_trade`);
};

export const buscarListaDeDividendosReceber = () => {
  return BaseProvider.GET(`carteira/consultar/dividendos/receber`);
};

export const buscarListaDeDividendosReceberSeManter = () => {
  return BaseProvider.GET(`carteira/consultar/dividendos/receber_se_manter`);
};

export const apagarCarteira = (carteira) => {
  BaseProvider.DELETE(`carteira/${carteira}`);
};

export const adicionarCarteiraFavorita = (idUsuario, idCarteira, value) => {
  return BaseProvider.POST(
    `carteira/adicionar/favorita?idUsuario=${idUsuario}&idCarteira=${idCarteira}&value=${value}`
  );
};
