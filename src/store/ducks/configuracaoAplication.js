import { createActions, createReducer } from "reduxsauce";

const INITIAL_STATE = {
  exponentialGraph: localStorage.getItem("exponentialGraph")
    ? localStorage.getItem("exponentialGraph")
    : "false",
};

export const { Types, Creators } = createActions({
  adicionarConfiguracaoAplication: ["json"],
});

const setConfiguracaoAplication = (state = INITIAL_STATE, action) => {
  if (action.json.exponentialGraph) {
    localStorage.setItem(
      "exponentialGraph",
      action.json.exponentialGraph.toString()
    );
  }
  return {
    ...state,
    ...action.json,
  };
};

export default createReducer(INITIAL_STATE, {
  [Types.ADICIONAR_CONFIGURACAO_APLICATION]: setConfiguracaoAplication,
});
