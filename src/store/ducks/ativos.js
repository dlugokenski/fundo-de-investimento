import { createActions, createReducer } from 'reduxsauce'

let timerId = null;

const INITIAL_STATE = { 
    coletaCotacaoEmAndamento: false,
    quantidadeTotalDeCotacaoColetar: 0,
    quantidadeDeCotacoesAindaNaoColetada: 0,
    totalColetadoEmPorcentagem: 0,
    listaDeCotacoesColetadas: []
};

export const { Types, Creators } = createActions({
    alterarStatusCotacao: ["json"],
    alterarQuantidadeTotalDeCotacaoColetar: ["json"],
    alterarListaDeCotacoesColetadas: ["json"],
    alterarQuantidadeDeCotacoesAindaNaoColetada: ["json"],
    alterarTotalColetadoEmPorcentagem: ["json"],
})

const alterarStatusCotacao = (state = INITIAL_STATE, action) => ({
    ...state,
    coletaCotacaoEmAndamento: action.json
});

const alterarQuantidadeTotalDeCotacaoColetar = (state = INITIAL_STATE, action) => ({
    ...state,
    quantidadeTotalDeCotacaoColetar: action.json
});

const alterarListaDeCotacoesColetadas = (state = INITIAL_STATE, action) => {
    // clearTimeout(timerId); // Limpa o temporizador existente
    // timerId = setTimeout(() => {
    //     debugger;
    //     // Define coletaCotacaoEmAndamento como false após 30 segundos
    //     Creators.alterarStatusCotacao(false);
    // }, 30000); // 30 segundos em milissegundos

    const tamanhoDaListaAtual = state.quantidadeTotalDeCotacaoColetar - state.quantidadeDeCotacoesAindaNaoColetadaAinda;
    const total = (tamanhoDaListaAtual * 100) / state.quantidadeTotalDeCotacaoColetar;      

    return {
        ...state,
        listaDeCotacoesColetadas: action.json,
        totalColetadoEmPorcentagem: Number(total.toFixed(0))
    };
};

const alterarQuantidadeDeCotacoesAindaNaoColetada = (state = INITIAL_STATE, action) => ({
    ...state,
    quantidadeDeCotacoesAindaNaoColetadaAinda: action.json
});

const alterarTotalColetadoEmPorcentagem = (state = INITIAL_STATE, action) => ({
    ...state,
    totalColetadoEmPorcentagem: action.json
});


export default createReducer(INITIAL_STATE, {
    [Types.ALTERAR_STATUS_COTACAO]: alterarStatusCotacao,
    [Types.ALTERAR_QUANTIDADE_TOTAL_DE_COTACAO_COLETAR]: alterarQuantidadeTotalDeCotacaoColetar,
    [Types.ALTERAR_QUANTIDADE_DE_COTACOES_AINDA_NAO_COLETADA]: alterarQuantidadeDeCotacoesAindaNaoColetada,
    [Types.ALTERAR_TOTAL_COLETADO_EM_PORCENTAGEM]: alterarTotalColetadoEmPorcentagem,
    [Types.ALTERAR_LISTA_DE_COTACOES_COLETADAS]: alterarListaDeCotacoesColetadas
})

  