import { createActions, createReducer } from 'reduxsauce'

const INITIAL_STATE = { collapsed: false };

export const { Types, Creators } = createActions({
    changeCollapsed: ["json"],
})

const changeCollapsed = ( state = INITIAL_STATE, action ) =>  ({collapsed: action.json});

export default createReducer(INITIAL_STATE, {
    [Types.CHANGE_COLLAPSED]: changeCollapsed,
})
