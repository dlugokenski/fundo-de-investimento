import { notification } from "antd";
import { createActions, createReducer } from "reduxsauce";

const INITIAL_STATE = {
  type: "",
  title: "",
  description: "",
};

export const { Types, Creators } = createActions({
  adicionarAlert: ["json"],
});

const adicionarAlert = (state = INITIAL_STATE, action) => {
  notification[action.json.type ? action.json.type : "error"]({
    message: action.json.title ? action.json.title : "Ocorreu um problema!",
    description: action.json.description,
  });
  return null;
};

export default createReducer(INITIAL_STATE, {
  [Types.ADICIONAR_ALERT]: adicionarAlert,
});
