import { createActions, createReducer } from 'reduxsauce'

const INITIAL_STATE = {
    coresAtivo: [],
    carteira: {},
    operacao: {
        ativo: {}
    }
};

export const { Types, Creators } = createActions({
    adicionarCarteira: ["json"],
    adicionarCorAtivo: ["json"],
    adicionarOperacao: ["json"],
    atualizarCarteira: ["json"],
})

const adicionarCarteira = ( state = INITIAL_STATE, action ) =>  ({
    ...state,
    carteira:{
         ...action.json
    }
});

const atualizarCarteira = ( state = INITIAL_STATE, action ) =>  ({
    ...state,
    carteira:{
        ...state.carteira,
        ...action.json
    }
});

const adicionarCorAtivo = ( state = INITIAL_STATE, action ) =>  ({
    ...state, 
    coresAtivo: action.json
});

const adicionarOperacao = ( state = INITIAL_STATE, action ) =>  ({
    ...state, 
    operacao: action.json
});


export default createReducer(INITIAL_STATE, { 
    [Types.ATUALIZAR_CARTEIRA]: atualizarCarteira,   
    [Types.ADICIONAR_CARTEIRA]: adicionarCarteira,
    [Types.ADICIONAR_COR_ATIVO]: adicionarCorAtivo, 
    [Types.ADICIONAR_OPERACAO]: adicionarOperacao   
})
