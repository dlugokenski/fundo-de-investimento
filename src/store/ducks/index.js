import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import configuracaoAplication from "./configuracaoAplication";
import configuracaoUsuario from "./configuracaoUsuario";
import websocket from "./websocketRedux";
import carteira from "./carteira";
import menu from "./menu";
import alert from "./alert";
import ativos from "./ativos";

export default (history) =>
  combineReducers({
    router: connectRouter(history),
    configuracaoAplication,
    configuracaoUsuario,
    websocket,
    carteira,
    alert,
    menu,
    ativos
  });
