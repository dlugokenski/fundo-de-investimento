import { createActions, createReducer } from 'reduxsauce'

const INITIAL_STATE = { socket: undefined };

export const { Types, Creators } = createActions({
    adicionarSocket: ["json"]
})

const setSocket = ( state = INITIAL_STATE, action ) =>  ({ socket: action.json });

export default createReducer(INITIAL_STATE, {
    [Types.ADICIONAR_SOCKET]: setSocket
})
