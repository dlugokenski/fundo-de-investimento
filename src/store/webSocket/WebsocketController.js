import * as SockJS from 'sockjs-client';
import Stomp from 'stompjs';

import { Creators as actions} from 'Ducks/websocketRedux'
import store from 'Store';

const websocketController = () => { 
  try{
    const sockJsProtocols = ["websocket"];
    const socket = new SockJS(`/ws/websocket`, null, {transports: sockJsProtocols}); 
    const stompClient = Stomp.over(socket);
    if(stompClient){
      stompClient.debug = null;
      stompClient.connect({"X-Authorization": `Bearer ${sessionStorage.getItem('tokenHash')}`}, () => onSuccess(stompClient), onError);
    }
    socket.onclose  = function() {
      store.dispatch(actions.adicionarSocket(null));
      onDisconect(socket);
    };

  } catch(err) {
    console.log('err', err)
  } 
};

function onError(e){
  console.log('error websocket', e)
}

function onSuccess(socket){
  store.dispatch(actions.adicionarSocket(socket));
}

function onDisconect(socket){
  console.log('onDisconect websocket', socket)
}

export default websocketController;

