import React from 'react';
import { Provider } from 'react-redux';

import store from './store';
import Routes from './router';

export default () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);
