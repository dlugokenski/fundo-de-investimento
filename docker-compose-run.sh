MOST_RECENT_VERSION=$(grep '"version":' package.json | cut -d\" -f4)

echo "*********************************************"
echo
echo "Building frontend-service-$MOST_RECENT_VERSION"
echo
echo "*********************************************"

VERSION_TAG=$MOST_RECENT_VERSION IP_APP_HOST="192.168.2.106:8080" docker-compose up -d