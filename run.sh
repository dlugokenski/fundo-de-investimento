#!/usr/bin/env bash
# NEED NODE VERSION 16.13.0

API_URL="localhost:8080"
API_MODE="DEV"
PROJECT_NAME="Meus investimentos"

cwd=$(pwd)

function getTypeSystemOperational(){
	if [ "$(uname)" == "Darwin" ]; then
		# Do something under Mac OS X platform  
		cwd=$(pwd)  		
	elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
		# Do something under GNU/Linux platform
		cwd=$(pwd)  
	elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW32_NT" ]; then
		# Do something under 32 bits Windows NT platform
		cwd=$(pwd -W)
	elif [ "$(expr substr $(uname -s) 1 10)" == "MINGW64_NT" ]; then
		# Do something under 64 bits Windows NT platform
		cwd=$(pwd -W)
	fi
}

function showHelp(){
  echo -e "\n"
  echo -e "\e[1mFrontend Running Script\e[0m"
  echo -e "\n"
  echo -e "This script will help you to run the frontend local application"
  echo -e "\n"
  echo -e "Commands:"
  echo -e "--help: show this help menu and leave the script"
  echo -e "-h or --host: the address for backend, default address: ${API_URL}"
  echo -e "-m or --mode: the application's run mode, default ${API_MODE}. Options [DEV, PROD] " 
  echo -e "-n or --name: the project name, default ${PROJECT_NAME}." 
  echo -e "Usages:"
  echo -e " - Showing this help menu: $./r.sh --help"
  echo -e " - Defining a custom backend address and application mode: $./r.sh -h 192.168.0.10:8080 -m PROD -n 'Meu projeto'" 
  echo -e "\n"
  echo -e "Thanks for using Frontend Running Script."
  echo -e "bye!"
}

function rumFront() {
  if [[ "${API_MODE}" == "PROD" ]]; then

    # deleting build folder 
    rm -r build

    echo -e "installing dependencies."
    yarn install

    echo -e "Building project."
    yarn build
       
  else	
    yarn start
	fi
}

while [ $# -gt 0 ]; do
  case "$1" in
    --help)
      showHelp
      exit 1
    ;;
    -h | --host)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        API_URL=$2
        shift 2
      else
        echo "Error: Argument for $1 is missing" >&2
        exit 1
      fi
    ;;
    -m | --mode)
      if [ -n "$2" ] && [ ${2:0:1} != "-" ]; then
        API_MODE=$2
        shift 2
      else
        echo "Error: Argument for $2 is missing" >&2
        exit 1
      fi
    ;;
    -n | --name)
      PROJECT_NAME=$2
      shift 2
    ;;
    -* | --*=)
      echo "Error: Option not found $1" >&2
      showHelp
      exit 1
    ;;
  esac
done

echo -e "\n"
echo -e "\e[1mFrontend Running Script\e[0m"
echo -e "\n"
echo -e "Backend address: $API_URL"
echo -e "Mode: $API_MODE"
echo -e "Porject name: $PROJECT_NAME"
echo -e "\n"

export REACT_APP_API_URL=${API_URL}
export REACT_APP_PROJECT_NAME=${PROJECT_NAME}
rumFront

