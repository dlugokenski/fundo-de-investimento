# build react stage
FROM node:16.3.0-alpine as builder-react
WORKDIR /app-builder-react
ADD . .
RUN chmod +x run.sh
ARG APP_HOST=localhost:8080
RUN sh ./run.sh -h ${APP_HOST} -m PROD 

FROM nginx:alpine
COPY ./nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/* 
COPY --from=builder-react /app-builder-react/build /usr/share/nginx/html
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]




