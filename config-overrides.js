const { injectBabelPlugin } = require('react-app-rewired');
const rewireLess = require('react-app-rewire-less');
const path = require('path');

module.exports = function override(config, env) {
  config = injectBabelPlugin(
    ['import', { libraryName: 'antd', libraryDirectory: 'es', style: true }], // change importing css to less
    config,
  );

  config.resolve = {
    alias: {
      Componente: path.resolve(__dirname, 'src/componente/'),
      Pages: path.resolve(__dirname, 'src/container/pages/'),
      Provider: path.resolve(__dirname, 'src/store/provider/'),
      Ducks: path.resolve(__dirname, 'src/store/ducks/'),
      RoutersPage: path.resolve(__dirname, 'src/router/'),
      Util: path.resolve(__dirname, 'src/util/'), 
      Imagens: path.resolve(__dirname, 'src/imagens/'),
      Store: path.resolve(__dirname, './src/store'),
      Shared: path.resolve(__dirname, 'src/shared')   
    }
  };

  config = rewireLess.withLoaderOptions({
    modifyVars: { "@primary-color": "#0258d1" },
    javascriptEnabled: true,
  })(config, env);
  return config;
};
